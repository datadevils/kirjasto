package fi.datadevils.kirjasto;

import fi.datadevils.kirjasto.fxui.KirjastoFXWindow;

/**
 *
 * @author Mika Pihlamo
 */
public class Constants {

	public static final String APP_NAME = "KirjastoFX";
	public static final String APP_ICON_NAME = "/fi/datadevils/kirjasto/resources/datadevils.png";

	public static final String DEFAULT_DATABASE_NAME = System
			.getProperty("user.dir") + "/database/kirjasto.sq3";
	public static final String DEFAULT_DATABASE_CREATION_FILE = System
			.getProperty("user.dir")
			+ "/src/fi/datadevils/kirjasto/resources/kirjastokanta.sql";
	public static final String DEFAULT_DATABASE_POPULATE_FILE = System
			.getProperty("user.dir")
			+ "/src/fi/datadevils/kirjasto/resources/populatekirjastokanta.sql";
	public static final String DEFAULT_DATABASE_NAME_CONSOLE = "/../database/kirjasto.sq3";

	public static final int LAINAUSAIKA = 30;

	public static final String FXML_DIR = "/fi/datadevils/kirjasto/fxml/";

	public static final String MAIN_WINDOW_NAME = "MainWindow";
	public static final String MAIN_WINDOW_FXML = MAIN_WINDOW_NAME + ".fxml";
	public static final KirjastoFXWindow MAIN_WINDOW = new KirjastoFXWindow(
			MAIN_WINDOW_NAME, FXML_DIR + MAIN_WINDOW_FXML);

	public static final String LOGIN_WINDOW_NAME = "LoginWindow";
	public static final String LOGIN_WINDOW_FXML = LOGIN_WINDOW_NAME + ".fxml";
	public static final KirjastoFXWindow LOGIN_WINDOW = new KirjastoFXWindow(
			LOGIN_WINDOW_NAME, FXML_DIR + LOGIN_WINDOW_FXML);

	public static final String REGISTRATION_WINDOW_NAME = "RegistrationWindow";
	public static final String REGISTRATION_WINDOW_FXML = REGISTRATION_WINDOW_NAME
			+ ".fxml";
	public static final KirjastoFXWindow REGISTRATION_WINDOW = new KirjastoFXWindow(
			REGISTRATION_WINDOW_NAME, FXML_DIR + REGISTRATION_WINDOW_FXML);

	public static final String ADMIN_MAIN_WINDOW_NAME = "AdminWindowMain";
	public static final String ADMIN_MAIN_WINDOW_FXML = ADMIN_MAIN_WINDOW_NAME
			+ ".fxml";
	public static final KirjastoFXWindow ADMIN_MAIN_WINDOW = new KirjastoFXWindow(
			ADMIN_MAIN_WINDOW_NAME, FXML_DIR + ADMIN_MAIN_WINDOW_FXML);

	public static final String ADMIN_SELAA_WINDOW_NAME = "AdminWindowSelaa";
	public static final String ADMIN_SELAA_WINDOW_FXML = ADMIN_SELAA_WINDOW_NAME
			+ ".fxml";
	public static final KirjastoFXWindow ADMIN_SELAA_WINDOW = new KirjastoFXWindow(
			ADMIN_SELAA_WINDOW_NAME, FXML_DIR + ADMIN_SELAA_WINDOW_FXML);

	public static final String ADMIN_POISTA_WINDOW_NAME = "AdminWindowPoista";
	public static final String ADMIN_POISTA_WINDOW_FXML = ADMIN_POISTA_WINDOW_NAME
			+ ".fxml";
	public static final KirjastoFXWindow ADMIN_POISTA_WINDOW = new KirjastoFXWindow(
			ADMIN_POISTA_WINDOW_NAME, FXML_DIR + ADMIN_POISTA_WINDOW_FXML);

	public static final String ADMIN_LISÄÄ_WINDOW_NAME = "AdminWindowLisää";
	public static final String ADMIN_LISÄÄ_WINDOW_FXML = ADMIN_LISÄÄ_WINDOW_NAME
			+ ".fxml";
	public static final KirjastoFXWindow ADMIN_LISÄÄ_WINDOW = new KirjastoFXWindow(
			ADMIN_LISÄÄ_WINDOW_NAME, FXML_DIR + ADMIN_LISÄÄ_WINDOW_FXML);

	public static final String USER_MAIN_WINDOW_NAME = "UserWindowMain";
	public static final String USER_MAIN_WINDOW_FXML = USER_MAIN_WINDOW_NAME
			+ ".fxml";
	public static final KirjastoFXWindow USER_MAIN_WINDOW = new KirjastoFXWindow(
			USER_MAIN_WINDOW_NAME, FXML_DIR + USER_MAIN_WINDOW_FXML);

	public static final String USER_MUOKKAA_WINDOW_NAME = "UserWindowMuokkaa";
	public static final String USER_MUOKKAA_WINDOW_FXML = USER_MUOKKAA_WINDOW_NAME
			+ ".fxml";
	public static final KirjastoFXWindow USER_MUOKKAA_WINDOW = new KirjastoFXWindow(
			USER_MUOKKAA_WINDOW_NAME, FXML_DIR + USER_MUOKKAA_WINDOW_FXML);
}
