package fi.datadevils.kirjasto;

import fi.datadevils.kirjasto.ui.MainWindow;
import fi.datadevils.kirjasto.varasto.Varasto;
import java.io.Console;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Kirjasto {

    public static void main(String[] args) {
        
        Console console = System.console();
        String baseName = null;
        if (console == null)
            baseName = Constants.DEFAULT_DATABASE_NAME;
        else
            baseName = Constants.DEFAULT_DATABASE_NAME_CONSOLE;
        System.err.println(baseName);
        switch (args.length) {
            case 1:
                baseName = args[1];
            case 0:
                try {
                    new MainWindow(new Varasto(baseName)).show();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Kirjasto.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(Kirjasto.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            default:
                System.out.println("Käyttö: Kirjasto <sqlite database file name>");
                System.exit(1);
        }
    }
}
