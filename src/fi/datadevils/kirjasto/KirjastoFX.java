/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto;

import fi.datadevils.kirjasto.fxui.KirjastoFXWindow;
import fi.datadevils.kirjasto.fxui.UiControl;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;

import java.util.ArrayList;

import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 *
 * @author mika
 */
public class KirjastoFX extends Application {

	private ArrayList<KirjastoFXWindow> myWindows = null;
        public static Scene scene;
        
	@Override
	public void start(Stage aStage) {
		aStage.setTitle(Constants.APP_NAME);
		aStage.getIcons().add(new Image((Constants.APP_ICON_NAME)));
		// tämä on ihan outo ratkaisu, mutta ei pystynyt just parempaan.
		// Älkää tehkö näin.
		if (!SingletonVarasto.getInstance().isReady()) {
			SingletonVarasto.getInstance().initDataBase(
					Constants.DEFAULT_DATABASE_CREATION_FILE);
			SingletonVarasto.getInstance().initDataBase(
					Constants.DEFAULT_DATABASE_POPULATE_FILE);
		}
		UiControl mainUi = new UiControl();
		mainUi.loadScreen(Constants.LOGIN_WINDOW);
		mainUi.loadScreen(Constants.REGISTRATION_WINDOW);
//		mainUi.loadScreen(Constants.MAIN_WINDOW);
		mainUi.loadScreen(Constants.ADMIN_MAIN_WINDOW);
//		mainUi.loadScreen(Constants.ADMIN_SELAA_WINDOW);
//		mainUi.loadScreen(Constants.ADMIN_POISTA_WINDOW);
//		mainUi.loadScreen(Constants.ADMIN_LISÄÄ_WINDOW);
		mainUi.loadScreen(Constants.USER_MAIN_WINDOW);
		// mainUi.loadScreen(Constants.USER_MUOKKAA_WINDOW);
		// mainUi.setScreen(Constants.ADMIN_MAIN_WINDOW.getNimi());
		// mainUi.setScreen(Constants.USER_MAIN_WINDOW.getNimi());
		mainUi.setScreen(Constants.LOGIN_WINDOW.getNimi());

		Group root = new Group();
		root.getChildren().addAll(mainUi);
		scene = new Scene(root);
               
                EventHandler event = new EventHandler(){
                    @Override
                    public void handle(Event event) {
                        scene.setCursor(Cursor.DEFAULT);
                    }
                };
                
                scene.setOnMouseReleased(event);
                scene.setOnMouseDragReleased(event);
                scene.setOnMouseDragExited(event);
                
                
		aStage.setScene(scene);
		aStage.show();
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
