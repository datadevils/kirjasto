/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.data;

import java.util.ArrayList;

/**
 *
 * @author Samuli
 */
public class Teos {

	protected ArrayList<String> tekijät = null;
	protected String nimi = null;
	protected String tyylilaji = null;
	protected String luokka = null;
	protected int julkaisuVuosi = 0;
	protected String kustantaja = null;
	protected String tyyppi = null;
	protected int kopioidenLkm = 0;

	public Teos() {
	}

	public Teos(String nimi, int kopioidenLkm, String tyylilaji,
			int julkaisuVuosi, String kustantaja, String luokka,
			ArrayList<String> tekijät, String tyyppi) {
		this.nimi = nimi;
		this.kopioidenLkm = kopioidenLkm;
		this.tyylilaji = tyylilaji;
		this.julkaisuVuosi = julkaisuVuosi;
		this.kustantaja = kustantaja;
		this.luokka = luokka;
		this.tekijät = tekijät;
		this.tyyppi = tyyppi;
	}

	public String getNimi() {
		return nimi;
	}

	public void setNimi(String nimi) {
		this.nimi = nimi;
	}

	public int getKopioidenLkm() {
		return kopioidenLkm;
	}

	public void setKopioidenLkm(int i) {
		this.kopioidenLkm = i;
	}

	public String getTyylilaji() {
		return tyylilaji;
	}

	public void setTyylilaji(String tyylilaji) {
		this.tyylilaji = tyylilaji;
	}

	public int getJulkaisuVuosi() {
		return julkaisuVuosi;
	}

	public void setJulkaisuVuosi(int julkaisuVuosi) {
		this.julkaisuVuosi = julkaisuVuosi;
	}

	public String getKustantaja() {
		return kustantaja;
	}

	public void setKustantaja(String kustantaja) {
		this.kustantaja = kustantaja;
	}

	public String getLuokka() {
		return luokka;
	}

	public void setLuokka(String luokka) {
		this.luokka = luokka;
	}

	public ArrayList<String> getTekijät() {
		return tekijät;
	}

	public void setTekijät(ArrayList<String> tekijät) {
		this.tekijät = tekijät;
	}

	@Override
	public String toString() {
		return "Teos\n----\n" + "  nimi=" + nimi + "\n  kopioidenLkm="
				+ kopioidenLkm + "\n  tyylilaji=" + tyylilaji
				+ "\n  julkaisuVuosi=" + julkaisuVuosi + "\n  kustantaja="
				+ kustantaja + "\n  luokka=" + luokka + "\n  tekijät="
				+ tekijät;
	}

	public String getTyyppi() {
		return tyyppi;
	}

	public void setTyyppi(String tyyppi) {
		this.tyyppi = tyyppi;
	}
}
