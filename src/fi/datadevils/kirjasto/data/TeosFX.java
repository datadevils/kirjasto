/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.data;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Verneri
 */
public class TeosFX {

    private final SimpleIntegerProperty id = new SimpleIntegerProperty();
    private final SimpleStringProperty nimi = new SimpleStringProperty();
    private final SimpleIntegerProperty julkaisuvuosi = new SimpleIntegerProperty();
    private final SimpleStringProperty tekijät = new SimpleStringProperty();
    private final SimpleStringProperty kustantaja = new SimpleStringProperty();
    private final SimpleIntegerProperty lukumäärä = new SimpleIntegerProperty();
    private final SimpleStringProperty tyyppi = new SimpleStringProperty();
    private final SimpleStringProperty palautus = new SimpleStringProperty();

    public TeosFX(int id, String nimi, int julkaisuvuosi, String tekijät, String kustantaja, String tyyppi, int lukumäärä, String palautus) {
        setId(id);
        setNimi(nimi);
        setJulkaisuvuosi(julkaisuvuosi);
        setTekijät(tekijät);
        setKustantaja(kustantaja);
        setLukumäärä(lukumäärä);
        setTyyppi(tyyppi);
        setPalautus(palautus);
    }

    public TeosFX(String id, String nimi, String julkaisuvuosi, String tekijät, String kustantaja, String tyyppi, String lukumäärä, String palautus) {
        this(Integer.parseInt(id), nimi, Integer.parseInt(julkaisuvuosi), tekijät, kustantaja, tyyppi, Integer.parseInt(lukumäärä), palautus);
    }

    public TeosFX(String... data) {
        this(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
    }

    public TeosFX() {
        this(-1, null, -1, null, null, null, -1, null);
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public Integer getId() {
        return id.get();
    }

    public void setNimi(String nimi) {
        this.nimi.set(nimi);
    }

    public String getNimi() {
        return nimi.get();
    }

    public void setJulkaisuvuosi(int julkaisuvuosi) {
        this.julkaisuvuosi.set(julkaisuvuosi);
    }

    public Integer getJulkaisuvuosi() {
        return julkaisuvuosi.get();
    }

    public void setTekijät(String tekijät) {
        this.tekijät.set(tekijät);
    }

    public String getTekijät() {
        return tekijät.get();
    }

    public void setKustantaja(String kustantaja) {
        this.kustantaja.set(kustantaja);
    }

    public String getKustantaja() {
        return kustantaja.get();
    }

    public void setLukumäärä(int lukumäärä) {
        this.lukumäärä.set(lukumäärä);
    }

    public Integer getLukumäärä() {
        return lukumäärä.get();
    }

    private void setTyyppi(String tyyppi) {
        this.tyyppi.set(tyyppi);
    }

    public String getTyyppi() {
        return this.tyyppi.get();
    }
    
    private void setPalautus(String palautus) {
        this.palautus.set(palautus);
    }
    
    public String getPalautus() {
        return this.palautus.get();
    }

}
