/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.data;

/**
 *
 * @author Samuli
 */
public class User {

	private int id;
	private String email;
	private String etunimi;
	private String sukunimi;
	private String salasana;
	private double sakkosaldo;
	private boolean admin;

	public User(int id, String email, String etunimi, String sukunimi,
			String salasana, double sakkosaldo, boolean admin) {
		this.id = id;
		this.email = email;
		this.etunimi = etunimi;
		this.sukunimi = sukunimi;
		this.salasana = salasana;
		this.sakkosaldo = sakkosaldo;
		this.admin = admin;
	}

	public User() {
		this(-1, "", "", "", "", -1.0, false);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEtunimi() {
		return etunimi;
	}

	public String getSalasana() {
		return salasana;
	}

	public void setSalasana(String salasana) {
		this.salasana = salasana;
	}

	public void setEtunimi(String etunimi) {
		this.etunimi = etunimi;
	}

	public String getSukunimi() {
		return sukunimi;
	}

	public void setSukunimi(String sukunimi) {
		this.sukunimi = sukunimi;
	}

	public double getSakkosaldo() {
		return sakkosaldo;
	}

	public void setSakkosaldo(double sakkosaldo) {
		this.sakkosaldo = sakkosaldo;
	}

	public boolean getAdmin() {
		return admin;
	}

	public int getAdminInt() {
		return (admin ? 1 : 0);
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	@Override
	public String toString() {
		return "User\n----\n" + "  email=" + email + "\n  etunimi=" + etunimi
				+ "\n  sukunimi=" + sukunimi + "\n  sakkosaldo=" + sakkosaldo
				+ "\n  admin=" + admin;
	}

}
