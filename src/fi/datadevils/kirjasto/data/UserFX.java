/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.data;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Samuli
 */
public class UserFX {

    private final SimpleIntegerProperty id = new SimpleIntegerProperty();
    private final SimpleStringProperty email = new SimpleStringProperty();
    private final SimpleStringProperty etunimi = new SimpleStringProperty();
    private final SimpleStringProperty sukunimi = new SimpleStringProperty();
    private final SimpleStringProperty salasana = new SimpleStringProperty();
    private final SimpleDoubleProperty sakkosaldo = new SimpleDoubleProperty();
    private final SimpleBooleanProperty admin = new SimpleBooleanProperty();

    public UserFX(int id, String etunimi, String sukunimi, String email, String salasana, Double sakkosaldo, boolean admin) {
        this.id.set(id);
        this.email.set(email);
        this.etunimi.set(etunimi);
        this.sukunimi.set(sukunimi);
        this.salasana.set(salasana);
        this.sakkosaldo.set(sakkosaldo);
        this.admin.set(admin);
    }

    public UserFX(String id, String etunimi, String sukunimi, String email, String salasana, String sakkosaldo, String admin) {
        this(Integer.parseInt(id), etunimi, sukunimi, email, salasana, Double.parseDouble(sakkosaldo), admin.equals("1") ? true : false);
    }
    
    public UserFX(String... data) {
        this(data[0], data[1], data[2], data[3], data[4], data[5], data[6]);
    }

    public UserFX() {
        this(-1, null, null, null, null, 0.0, false);
    }

    public int getId() {
        return this.id.get();
    }

    public String getEmail() {
        return this.email.get();
    }

    public String getEtunimi() {
        return this.etunimi.get();
    }

    public String getSukunimi() {
        return this.sukunimi.get();
    }

    public String getSalasana() {
        return this.salasana.get();
    }

    public double getSakkosaldo() {
        return this.sakkosaldo.get();
    }

    public boolean getAdmin() {
        return this.admin.get();
    }

    
    
}
