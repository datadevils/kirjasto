/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxcontrollers;

import fi.datadevils.kirjasto.Constants;
import fi.datadevils.kirjasto.data.Teos;
import fi.datadevils.kirjasto.operations.LisääKustantaja;
import fi.datadevils.kirjasto.fxui.UiControl;
import fi.datadevils.kirjasto.fxui.UiControlInterface;
import fi.datadevils.kirjasto.operations.LisääLuokka;
import fi.datadevils.kirjasto.operations.LisääTyylilaji;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Samuli
 */
public class AdminWindowLisääController implements Initializable, UiControlInterface {

    UiControl myParent = null;

    @FXML
    private BorderPane admin;
    @FXML
    private Label current_user;
    @FXML
    private Button lopeta;
    @FXML
    private TextField teosnimi;
    @FXML
    private TextField tekijät;
    @FXML
    private TextField kappalemäärä;
    @FXML
    private ComboBox<String> genre;
    @FXML
    private TextField julkaisu;
    @FXML
    private ComboBox<String> kustantaja;
    @FXML
    private ComboBox<String> luokka;
    @FXML
    private Button lisääkirja;
    @FXML
    private Text statustext;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ObservableList<String> obsKustantajat = FXCollections.observableArrayList(LisääKustantaja.populatePublishers());
        kustantaja.getItems().addAll(obsKustantajat);
        kustantaja.setEditable(true);

        ObservableList<String> obsGenret = FXCollections.observableArrayList(LisääTyylilaji.populateGenres());
        genre.getItems().addAll(obsGenret);
        genre.setEditable(true);

        ObservableList<String> obsLuokat = FXCollections.observableArrayList(LisääLuokka.populateLuokat());
        luokka.getItems().addAll(obsLuokat);
        luokka.setEditable(true);
    }

    @Override
    public void modifyButtonStates() {
        // 
    }

    @Override
    public void windowOpened() {
        current_user.setText(SingletonVarasto.getInstance().getMyUserName().getValue());
    }

    @FXML
    private void handleLopetaButtonAction(ActionEvent event) {
        myParent.setScreen(Constants.ADMIN_MAIN_WINDOW.getNimi());
    }

    @Override
    public void setParent(UiControl aParent) {
        myParent = aParent;
    }

    @FXML
    private void handleLisääKirjaButtonAction(ActionEvent event) {

        ArrayList<String> tekijät = new ArrayList(5);
        Teos myTeos = null;

        if (checkAllgiven()) {

            if (!this.tekijät.getText().isEmpty()) {
                String[] tempTable = null;
                tempTable = this.tekijät.getText().split(",");
                for (int i = 0; i < tempTable.length; i++) {
                    tekijät.add(tempTable[i].trim());
                }
            }

            myTeos = new Teos();

            myTeos.setNimi(teosnimi.getText());
            myTeos.setKopioidenLkm(Byte.parseByte(kappalemäärä.getText()));

            LisääTyylilaji.checkAndAddGenre(genre.getValue());
            myTeos.setTyylilaji(genre.getValue());
            myTeos.setJulkaisuVuosi(Integer.parseInt(julkaisu.getText()));

            LisääKustantaja.checkAndAddPublisher(kustantaja.getValue());
            myTeos.setKustantaja(kustantaja.getValue());

            LisääLuokka.checkAndAddLuokka(luokka.getValue());
            myTeos.setLuokka(luokka.getValue());

            myTeos.setTekijät(tekijät);

            SingletonVarasto.getInstance().setTeos(myTeos);

            myParent.setScreen(Constants.ADMIN_MAIN_WINDOW.getNimi());

        } else {
            statustext.setText("You have failed!");
        }
    }

    private boolean checkAllgiven() {
        if ((!teosnimi.getText().isEmpty())
                && (!kappalemäärä.getText().isEmpty())
                && (!genre.getValue().isEmpty())
                && (!julkaisu.getText().isEmpty())
                && (!kustantaja.getValue().isEmpty())
                && (!luokka.getValue().isEmpty())
                && (!tekijät.getText().isEmpty())) {

            return true;
        }
        return false;
    }

}
