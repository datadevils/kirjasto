/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxcontrollers;

import fi.datadevils.kirjasto.Constants;
import fi.datadevils.kirjasto.KirjastoFX;
import fi.datadevils.kirjasto.data.TeosFX;
import fi.datadevils.kirjasto.data.UserFX;
import fi.datadevils.kirjasto.fxui.UiControl;
import fi.datadevils.kirjasto.fxui.UiControlInterface;
import fi.datadevils.kirjasto.operations.PoistaKäyttäjä;
//import fi.datadevils.kirjasto.operations.PoistaKäyttäjä;
import fi.datadevils.kirjasto.operations.PoistaTeos;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import static javax.xml.bind.DatatypeConverter.parseString;

/**
 * FXML Controller class
 *
 * @author Samuli
 */
public class AdminWindowMainController implements Initializable,
		UiControlInterface {

	UiControl myParent = null;

	@FXML
	private BorderPane admin;
	@FXML
	private Button lopeta;
	@FXML
	private Button listUser;
	@FXML
	private Button addUser;
	@FXML
	private Button kirjastoButton;
	@FXML
	private Button listBook;
	@FXML
	private Button addBook;
	@FXML
	private Button toUser;
	@FXML
	private Label current_user;
	@FXML
	private Parent embeddedTablePanel;
	@FXML
	public TablePanelController embeddedTablePanelController;
	@FXML
	private GridPane keskusta;
	@FXML
	private Button roskis;

	private Integer id = 0;

	private TeosFX tfx = null;

	private UserFX ufx = null;

	private PoistaTeos poistaTeosOlio = new PoistaTeos(
			SingletonVarasto.getInstance());

        private PoistaKäyttäjä poistaKäyttäjäOlio = new PoistaKäyttäjä();
        
	private int object_index = 0;

	// TODO
	@Override
	public void initialize(URL location, ResourceBundle resources) {
            kirjastoButton.setVisible(false);
	}

	@Override
	public void modifyButtonStates() {

	}

	@FXML
	private void handleLopetaButtonAction(ActionEvent event) {
		if (lopeta.getId().matches("palaa")) {
			setButtonsDisabled(false);
			lopeta.setId("lopeta");
			GridPane parentController = ((AdminWindowMainController) myParent
					.getInterface(Constants.ADMIN_MAIN_WINDOW.getNimi()))
					.getKeskusta();
			parentController.getChildren().remove(0);
			try {
				URL tempURL = this.getClass().getResource(
						Constants.FXML_DIR + "TablePanel.fxml");
				FXMLLoader tempPanel = new FXMLLoader(tempURL);
				parentController.add(tempPanel.load(), 0, 0);
				embeddedTablePanelController = ((TablePanelController) tempPanel
						.getController());
				embeddedTablePanelController.w = embeddedTablePanelController.w.ADMIN;
				embeddedTablePanelController.teeTeosTaulu();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			myParent.setScreen(Constants.LOGIN_WINDOW.getNimi());
		}
	}

	@FXML
	private void handleAddUserButtonAction(ActionEvent event) {
		if (keskusta.getChildren().size() > 0) {
			keskusta.getChildren().remove(0);
		}
		setButtonsDisabled(true);
		try {
			URL temp = this.getClass().getResource(
					Constants.FXML_DIR + "NewUserPanel.fxml");
			FXMLLoader tempPanel = new FXMLLoader(temp);
			keskusta.add(tempPanel.load(), 0, 0);
			((NewUserPanelController) tempPanel.getController())
					.setParent(myParent);
			lopeta.setId("palaa");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	private void handleKirjastoButtonAction(ActionEvent event) {
		embeddedTablePanelController.teeTeosTaulu();
	}

    @FXML
    protected void handleDropToTrashAction(MouseDragEvent event) {
        System.out.println("Putting to trash  ADMIN side");

        try {
            if (!embeddedTablePanelController.usersDisplayed) {
                removeItem();
            } else {
                removeUser();
            }

        } catch (NullPointerException ex) {

            Logger.getLogger(AdminWindowMainController.class.getName()).log(
                    Level.SEVERE, null, ex);
            System.out.println("Dropissa null pointer exeption");
        }
    }

    private void removeItem() {

        if (!(embeddedTablePanelController.taulu.getItems().isEmpty())) {

            int lukumäärä = 0;
            try {
                object_index = embeddedTablePanelController.taulu
                        .getSelectionModel().getSelectedIndex();
                tfx = (TeosFX) embeddedTablePanelController.taulu
                        .getSelectionModel().getSelectedItem();

            } catch (NullPointerException ex) {
                Logger.getLogger(AdminWindowMainController.class.getName())
                        .log(Level.SEVERE, null, ex);
                System.out.println("Dropissa null pointer exeption tfx luonti");
            }
            if (tfx != null) {
                id = tfx.getId();
                lukumäärä = (int) tfx.getLukumäärä();
                
                embeddedTablePanelController.info.clear();

                ArrayList<String> vastaus_unique_item = new ArrayList<String>();

                // haetaan kaikki kappaleet unique_item taulusta, joiden item_id
                // on this.id
                vastaus_unique_item = poistaTeosOlio.haeKappaleetFx(id);

                String[] vastaus = new String[100];
                int id__unique_item = 0;
                boolean ei_lainassa = true;
                // Käydään läpi kaikki unique_item taulun alkiot, ja jos joku
                // ei ole lainassa, niin poistetaan se.
                for (int i = 0; i < vastaus_unique_item.size(); i++) {

                    // splitataan unique_item
                    vastaus = vastaus_unique_item.get(i).split(
                            Pattern.quote(";"));

                    // otetaan id
                    id__unique_item = parseInt(vastaus[0].trim());

                    // jos user on null, niin ei ole lainassa ja voidaan poistaa
                    if (parseString(vastaus[3]).equals("null")) {

                        ei_lainassa = true;
                        KirjastoFX.scene.setCursor(Cursor.DEFAULT);
                        roskis.setId("roskis_taysi");

                        if (lukumäärä > 1) {

                            // poistetaan unique_item taulusta vapaa alkio
                            poistaTeosOlio.poistaYksiFx(id__unique_item, id);
                            tfx.setLukumäärä(lukumäärä - 1);
                            embeddedTablePanelController.taulu.getItems().set(
                                    object_index, tfx);
                            break;

                        } else {

                            // jos kappaleita on 1 tai vähemmän poistetaan koko
                            // teos jos on vapaa
                            poistaTeosOlio.poistaYksiFx(id__unique_item, id);
                            embeddedTablePanelController.taulu.getItems()
                                    .remove(object_index);
                            break;
                        }
                    } else {
                        // merkataan, että teos oli lainassa
                        ei_lainassa = false;
                    }
                }
                // Jos kaikki teokset olivat lainassa, niin tulostetaan,
                // että ei voi poistaa teosta.
                if (ei_lainassa == false) {
                    embeddedTablePanelController.info
                            .appendText("Teosta ei voi poistaa, koska kappaleita on lainassa.");
                }
            } else {
                System.out.println("tfx luonti epäonnistui dropissa");
            }
        }
    }

    private void removeUser() {

        if (!(embeddedTablePanelController.taulu.getItems().isEmpty())) {

            try {
                object_index = embeddedTablePanelController.taulu
                        .getSelectionModel().getSelectedIndex();
                ufx = (UserFX) embeddedTablePanelController.taulu
                        .getSelectionModel().getSelectedItem();

            } catch (NullPointerException ex) {
                Logger.getLogger(AdminWindowMainController.class.getName())
                        .log(Level.SEVERE, null, ex);
                System.out.println("Dropissa null pointer exeption ufx luonti");
            }

            if (ufx != null) {
                id = ufx.getId();

                if (SingletonVarasto.getMyUser().getId() != (int) id) {

                    KirjastoFX.scene.setCursor(Cursor.DEFAULT);
                    roskis.setId("roskis_taysi");
                    PoistaKäyttäjä.poistaKäyttäjäFx(id);
                    embeddedTablePanelController.taulu.getItems().remove(
                            object_index);
                } else {
                    embeddedTablePanelController.info.clear();
                    embeddedTablePanelController.info.setText("Kirjautunutta käyttäjää ei voi poistaa!");
                }
            }

        }
    }

	@FXML
	protected void handleMouseClickOnTrash(ActionEvent event) {

		if (roskis.getId()=="roskis_taysi") {
			roskis.setId("roskis_tyhja");
		}
	}

	@FXML
	private void handleUserButtonAction(ActionEvent event) {
	}

	@FXML
	private void handleToUserButtonAction(ActionEvent event) {
		myParent.setScreen(Constants.USER_MAIN_WINDOW.getNimi());
	}

	@FXML
	private void handleListUserButtonAction(ActionEvent event) {
		embeddedTablePanelController.teeUserTaulu();
	}

	@FXML
	private void handleAddBookButtonAction(ActionEvent event) {
		if (keskusta.getChildren().size() > 0) {
			keskusta.getChildren().remove(0);
		}
		setButtonsDisabled(true);
		try {
			URL temp = this.getClass().getResource(
					Constants.FXML_DIR + "NewTeosPanel.fxml");
			FXMLLoader tempPanel = new FXMLLoader(temp);
			keskusta.add(tempPanel.load(), 0, 0);
			((NewTeosPanelController) tempPanel.getController())
					.setParent(myParent);
			lopeta.setId("palaa");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	protected void handleDropToBagAction(MouseDragEvent event) {
		System.out.println("Adding to bag  ADMIN side");
	}

	@FXML
	protected void handleDropToShelfAction(MouseDragEvent event) {
		System.out.println("Adding to shelf  ADMIN side");
	}

	@FXML
	protected void setOnDragOver(MouseDragEvent event) {
		System.out.println("Drag over  ADMIN side");
	}

	@FXML
	protected void setOnDragRelease(MouseDragEvent event) {
		System.out.println("Drag released outside trash  ADMIN side!");
		KirjastoFX.scene.setCursor(Cursor.DEFAULT);
	}

	@FXML
	protected void setOnEntered(MouseDragEvent event) {
		System.out.println("Drag entered  ADMIN side");
	}

	@Override
	public void setParent(UiControl aParent) {
		myParent = aParent;
	}

	@Override
	public void windowOpened() {
		setButtonsDisabled(false);
		current_user.setText(SingletonVarasto.getInstance().getMyUserName()
				.getValue());
		embeddedTablePanelController.w = embeddedTablePanelController.w.ADMIN;
		embeddedTablePanelController.teeTeosTaulu();

		roskis.setId("roskis_tyhja");

		embeddedTablePanelController.info.clear();
	}

	public void setButtonsDisabled(boolean aState) {
		listUser.setDisable(aState);
		addUser.setDisable(aState);
		kirjastoButton.setDisable(aState);
		listBook.setDisable(aState);
		addBook.setDisable(aState);
		toUser.setDisable(aState);
		roskis.setDisable(aState);
	}

	public GridPane getKeskusta() {
		return keskusta;
	}

	public void setLopetaButtonId(String aStatus) {
		lopeta.setId(aStatus);
	}
}
