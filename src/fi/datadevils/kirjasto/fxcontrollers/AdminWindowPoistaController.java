/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxcontrollers;

import fi.datadevils.kirjasto.Constants;
import static fi.datadevils.kirjasto.fxcontrollers.AdminWindowPoistaController.KPL_VALIKKO;
import fi.datadevils.kirjasto.fxui.UiControl;
import fi.datadevils.kirjasto.fxui.UiControlInterface;
import fi.datadevils.kirjasto.operations.HaeTeokset;
import fi.datadevils.kirjasto.operations.PoistaTeos;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import static java.lang.Integer.parseInt;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.layout.BorderPane;

/**
 * FXML Controller class
 *
 * @author Jarkko
 */
public class AdminWindowPoistaController implements Initializable,
		UiControlInterface {

	UiControl myParent = null;
	boolean isListDisplayed = false;

	@FXML
	private BorderPane admin;
	@FXML
	private Button lopeta;

	@FXML
	private Button teokset;

	@FXML
	private ListView<String> lista = new ListView<String>();

	int id = 0;
	int id__unique_item = 0;
	boolean valittu = false;

	@FXML
	private Label info_text;

	int lista_valikko = TEOS_VALIKKO;
	public static final int TEOS_VALIKKO = 0;
	public static final int KPL_VALIKKO = 1;

	private ArrayList<String> kappaleet = new ArrayList<String>();

	PoistaTeos poistaTeosOlio = new PoistaTeos(SingletonVarasto.getInstance());

	@FXML
	protected void handleListaClickAction() {
		// Tätä handleria kutsutaan jos listaa klikataan hiirellä normaalisti.
		// Toteuttaa teos valikon toiminnan.
		try {
			if (!(lista.getItems().isEmpty())) {

				String selected_teos = lista.selectionModelProperty().get()
						.getSelectedItem();
				String[] string_lista = new String[100];

				if (lista_valikko == TEOS_VALIKKO) {

					if (!lista.selectionModelProperty().get().isEmpty()) {

						string_lista = selected_teos.split(Pattern.quote("|"));

						string_lista = string_lista[0]
								.split(Pattern.quote(":"));

						this.id = parseInt(string_lista[1].trim());
						this.kappaleet = poistaTeosOlio.haeKappaleetFx(id);
						ObservableList<String> kappaleetlist = FXCollections
								.observableArrayList(kappaleet);
						lista.setItems(kappaleetlist);
						lista.selectionModelProperty().get().clearSelection();
						valittu = false;
						lista_valikko = KPL_VALIKKO;
					}
				}
			}
		} catch (NullPointerException ex) {
			System.out.println("valinnassa null pointer exeption");
		}

	}

	@FXML
	protected void handleListaDragAction() {
		// Tätä funktiota kutsutaan jos listassa aletaan raahata hiirellä
		// eli jos DragDetected event laukeaa. Toteuttaa kpl valikon toiminnan.
		try {
			if (!(lista.getItems().isEmpty())) {

				String selected_teos = lista.selectionModelProperty().get()
						.getSelectedItem();
				String[] string_lista = new String[100];

				if (lista_valikko == KPL_VALIKKO) {

					if (!lista.selectionModelProperty().get().isEmpty()) {

						string_lista = selected_teos.split(";");

						this.id__unique_item = parseInt(string_lista[0].trim());
						valittu = !(lista.selectionModelProperty().get()
								.isEmpty());

						if (valittu) {
							lista.startFullDrag();
						}

					} else {
						System.out.println("No item selected!");
					}
				}
			}
		} catch (NullPointerException ex) {
			System.out.println("valinnassa null pointer exeption");
		}
	}

	@FXML
	public void handlePoistaButtonAction(MouseDragEvent event) {
		// Tämä handleri poistaa tietokannasta teoksen tai sen kappaleen jos
		// niitä on useampia. Kutsutaan kun MouseDragReleased event laukeaa.
		try {

			if (!lista.getItems().isEmpty()) {
				if (valittu) {
					// poistaTeosOlio.poistaYksiFx(this.id__unique_item,
					// this.id, this.kappaleet);
				}

				this.kappaleet = poistaTeosOlio.haeKappaleetFx(this.id);

				this.id__unique_item = 0;

				if (!this.kappaleet.isEmpty()) {
					ObservableList<String> kappaleetlist = FXCollections
							.observableArrayList(this.kappaleet);
					lista.setItems(kappaleetlist);
					lista.selectionModelProperty().get().clearSelection();
				} else {
					ObservableList<String> kappaleetlist = FXCollections
							.emptyObservableList();
					lista.itemsProperty().set(kappaleetlist);
				}
			}
		} catch (NullPointerException ex) {
			System.out.println("Poistossa null pointer exeption");
		}

	}

	@FXML
	private void handleLopetaButtonAction(ActionEvent event) {
		myParent.setScreen(Constants.ADMIN_MAIN_WINDOW.getNimi());
	}

	@FXML
	private void handleTeoksetButtonAction(ActionEvent event) {
		// teokset napin toiminta. Palataan teos valikkoon eli
		// haetaan tiedot tietokannasta ja tulostetaan.
		try {
			lista_valikko = TEOS_VALIKKO;
			this.id = 0;
			this.id__unique_item = 0;
			valittu = false;
			lista.selectionModelProperty().get().clearSelection();
			lista.getItems().clear();
			ArrayList<String> teokset = new ArrayList<String>(); // =
																	// poistaTeosOlio.haeTeoksetFx();

			teokset = HaeTeokset.haeKaikki();

			ObservableList<String> teoksetlist = FXCollections
					.observableArrayList(teokset);
			lista.setItems(teoksetlist);

		} catch (NullPointerException ex) {
			System.out.println("teosten haussa null pointer exeption");
		}
	}

	@FXML
	private void setOnDragOver(MouseDragEvent event) {
		// System.out.println("Drag over");
	}

	@FXML
	private void setOnEntered(MouseDragEvent event) {
		// System.out.println("Drag entered");
	}

	@FXML
	private void setOnDragRelease(MouseDragEvent event) {
		// System.out.println("Drag released outside trash!");
	}

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// TODO
	}

	@Override
	public void setParent(UiControl aParent) {
		myParent = aParent;
	}

	@Override
	public void modifyButtonStates() {
		// throw new UnsupportedOperationException("Not supported yet."); //To
		// change body of generated methods, choose Tools | Templates.
	}

	/*
	 * @Override public void setUserName() { //
	 * current_user.setText(SingletonVarasto
	 * .getInstance().getMyUserName().getValue()); }
	 */

	@Override
	public void windowOpened() {
		// throw new UnsupportedOperationException("Not supported yet."); //To
		// change body of generated methods, choose Tools | Templates.
		// Haetaan ikkunan avautuessa tiedot tietokannasta ja tulostetaan
		try {

			lista_valikko = TEOS_VALIKKO;
			this.id = 0;
			this.id__unique_item = 0;
			valittu = false;
			lista.getItems().clear();
			lista.selectionModelProperty().get().clearSelection();
			ArrayList<String> teokset = new ArrayList<String>(); // =
																	// poistaTeosOlio.haeTeoksetFx();
			teokset = HaeTeokset.haeKaikki();

			ObservableList<String> teoksetlist = FXCollections
					.observableArrayList(teokset);
			lista.setItems(teoksetlist);

		} catch (NullPointerException ex) {
			System.out.println("ikkunan avauksessa null pointer exeption");
		}
	}

}
