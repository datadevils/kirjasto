/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxcontrollers;

import fi.datadevils.kirjasto.Constants;
import fi.datadevils.kirjasto.fxui.UiControl;
import fi.datadevils.kirjasto.fxui.UiControlInterface;
import fi.datadevils.kirjasto.operations.HaeTeokset;
import fi.datadevils.kirjasto.operations.LainaaTeos;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;

/**
 * FXML Controller class
 *
 * @author Samuli
 */
public class AdminWindowSelaaController implements Initializable, UiControlInterface {

    UiControl myParent = null;
    boolean isListDisplayed = false;

    @FXML
    private BorderPane admin;
    @FXML
    private Button lopeta;
    @FXML
    private Button kirjat;
    @FXML
    private Button cdt;
    @FXML
    private Button lehdet;
    @FXML
    private ListView<String> lista;
    @FXML
    private Button lainaa;
    @FXML
    private Button tiedot;
    @FXML
    private Label current_user;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void handleLopetaButtonAction(ActionEvent event) {
        myParent.setScreen(Constants.ADMIN_MAIN_WINDOW.getNimi());
    }

    @FXML
    private void handleKirjatButtonAction(ActionEvent event) {
        ArrayList<String> kirjat = HaeTeokset.haeKaikki("kirjat");
        ObservableList<String> kirjalist = FXCollections.observableArrayList(kirjat);
        lista.setItems(kirjalist);
        isListDisplayed = true;
    }

    @FXML
    private void handleLainaaButtonAction(ActionEvent event) {
        if (isListDisplayed) {
            int valittu = lista.getSelectionModel().getSelectedIndex();
            System.out.println(valittu);
            if (valittu > -1) {
                String valittuRivi = lista.getSelectionModel().getSelectedItem();
                String[] split = valittuRivi.split(" ");
                if (Integer.parseInt(split[1]) > -1) {
                    ArrayList<Integer> lainat = new ArrayList<>();

                    System.out.println(Integer.parseInt(split[1]));
                    lainat.add(Integer.parseInt(split[1]));
                    
                    Boolean tulos = LainaaTeos.lainaaTeos(lainat, SingletonVarasto.getInstance().getMyUser());
                    System.out.println(tulos);
                }
                ArrayList<String> tiedot = HaeTeokset.lisätiedotHaeJaTulosta(split[1]);
                ObservableList<String> obsTiedot = FXCollections.observableArrayList(tiedot);
                lista.setItems(obsTiedot);

                isListDisplayed = false;
            }
        }
    }

    @Override
    public void setParent(UiControl aParent) {
        myParent = aParent;
    }

    @Override
    public void modifyButtonStates() {
        // 
    }

    @FXML
    private void handleTiedotButtonAction(ActionEvent event) {
        if (isListDisplayed) {
            int valittu = lista.getSelectionModel().getSelectedIndex();
            System.out.println(valittu);
            if (valittu > -1) {
                String valittuRivi = lista.getSelectionModel().getSelectedItem();
                String[] split = valittuRivi.split(" ");
                int id = Integer.parseInt(split[1]);
                System.out.println(id);
                ArrayList<String> tiedot = HaeTeokset.lisätiedotHaeJaTulosta(split[1]);
                ObservableList<String> obsTiedot = FXCollections.observableArrayList(tiedot);
                lista.setItems(obsTiedot);
                isListDisplayed = false;
            }
        }
    }

    @Override
    public void windowOpened() {
        current_user.setText(SingletonVarasto.getInstance().getMyUserName().getValue());
    }
}
