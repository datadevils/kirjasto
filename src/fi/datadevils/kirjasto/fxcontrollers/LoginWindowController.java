package fi.datadevils.kirjasto.fxcontrollers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import fi.datadevils.kirjasto.Constants;
import fi.datadevils.kirjasto.fxui.UiControl;
import fi.datadevils.kirjasto.fxui.UiControlInterface;
import fi.datadevils.kirjasto.operations.Kirjaudu;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;

/**
 * FXML Controller class
 *
 * @author mika
 */
public class LoginWindowController implements Initializable, UiControlInterface {

	UiControl myParent = null;

	@FXML
	private Label current_user;
	@FXML
	private Button kirjasto;
	@FXML
	private TextField email;
	@FXML
	private PasswordField password;
	@FXML
	private Label statusText;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
            current_user.setVisible(false);
	}

	@Override
	public void modifyButtonStates() {
	}

	@FXML
	private void handleLopetaButtonAction(ActionEvent event)
			throws SQLException {
		SingletonVarasto.getInstance().close();
		Platform.exit();
	}

	@FXML
	private void handleRegisterButtonAction(ActionEvent event) {
		myParent.setScreen(Constants.REGISTRATION_WINDOW.getNimi());

	}

	@FXML
	private void handleOhitaButtonAction(ActionEvent event) {
		if (Kirjaudu.login("admin@kirjasto.datadevils.fi", "salasana")) {
			if (SingletonVarasto.getInstance().getMyUserIsAdmin().get()) {
				myParent.setScreen(Constants.ADMIN_MAIN_WINDOW.getNimi());
			} else {
				myParent.setScreen(Constants.USER_MAIN_WINDOW.getNimi());
			}
		} else {
			System.err.println("Fail");
			statusText.setText("You have failed!");
			this.email.requestFocus();
			this.password.setText("");
		}

	}

	@FXML
	private void handleEmailAction(ActionEvent aKeyEvent) {
		password.requestFocus();
	}

	@FXML
	private void handlePasswordAction(ActionEvent aKeyEvent) {
		String email = this.email.getText();
		String password = this.password.getText();
		if (email.isEmpty()) {
			email = "user@kirjasto.datadevils.fi";
		}
		if (password.isEmpty()) {
			password = "salasana";
		}
		System.err.println(email + ": " + password);
		if (Kirjaudu.login(email, password)) {
			if (SingletonVarasto.getInstance().getMyUserIsAdmin().get()) {
				myParent.setScreen(Constants.ADMIN_MAIN_WINDOW.getNimi());
			} else {
				myParent.setScreen(Constants.USER_MAIN_WINDOW.getNimi());
			}
		} else {
			System.err.println("Fail");
			statusText.setText("You have failed!");
			this.email.requestFocus();
			this.password.setText("");
		}
	}

	@Override
	public void setParent(UiControl aParent) {
		myParent = aParent;
	}

	@Override
	public void windowOpened() {
		current_user.setText(SingletonVarasto.getInstance().getMyUserName()
				.getValue());
	}
}
