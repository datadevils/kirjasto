/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxcontrollers;

import fi.datadevils.kirjasto.Constants;
import fi.datadevils.kirjasto.data.User;
import fi.datadevils.kirjasto.fxui.UiControl;
import fi.datadevils.kirjasto.fxui.UiControlInterface;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author mika
 */
public class MainWindowController implements Initializable, UiControlInterface {

    UiControl myParent = null;

    @FXML
    private Button lopeta;
    @FXML
    private Button Kirjaudu;
    @FXML
    private Button admin;
    @FXML
    private Button user;
    @FXML
    private Label current_user;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        admin.setDisable(true);
        user.setDisable(true);
    }

    @Override
    public void modifyButtonStates() {
        User user = SingletonVarasto.getInstance().getMyUser();
        if (user != null) {
            if (user.getAdmin()) {
                this.admin.setDisable(false);
            } else {
                this.admin.setDisable(true);
            }
            this.user.setDisable(false);
        } else {
            this.admin.setDisable(true);
            this.user.setDisable(true);
        }
    }

    @FXML
    private void handleLopetaButtonAction(ActionEvent event) throws SQLException {
        SingletonVarasto.getInstance().close();
        Platform.exit();
    }

    @FXML
    private void handleKirjauduButtonAction(ActionEvent event) {
        myParent.setScreen(Constants.LOGIN_WINDOW.getNimi());

    }

    @FXML
    private void handleAdminButtonAction(ActionEvent event) {
        myParent.setScreen(Constants.ADMIN_MAIN_WINDOW.getNimi());
    }

    @FXML
    private void handleUserButtonAction(ActionEvent event) {
        //System.out.println("You clicked me!");
        myParent.setScreen(Constants.USER_MAIN_WINDOW.getNimi());
    }

    @Override
    public void setParent(UiControl aParent) {
        myParent = aParent;
    }

    @Override
    public void windowOpened() {
        current_user.setText(SingletonVarasto.getInstance().getMyUserName().getValue());
    }
}
