/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxcontrollers;

import fi.datadevils.kirjasto.Constants;
import fi.datadevils.kirjasto.data.Teos;
import fi.datadevils.kirjasto.fxui.UiControl;
import fi.datadevils.kirjasto.fxui.UiControlInterface;
import fi.datadevils.kirjasto.operations.LisääKustantaja;
import fi.datadevils.kirjasto.operations.LisääLuokka;
import fi.datadevils.kirjasto.operations.LisääTyylilaji;
import fi.datadevils.kirjasto.operations.LisääTyyppi;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author Mika
 */
public class NewTeosPanelController implements Initializable,
		UiControlInterface {

	private static String STYLE_VIRHE = "-fx-background-color: peachpuff;";
	private static String STYLE_OK = "-fx-background-color: white;";

	UiControl myParent = null;

	@FXML
	private GridPane newTeos;
	@FXML
	private TextField tekijat;
	@FXML
	private TextField teosnimi;
	@FXML
	private ComboBox<String> genre;
	@FXML
	private ComboBox<String> luokka;
	@FXML
	private TextField julkaisu;
	@FXML
	private ComboBox<String> kustantaja;
	@FXML
	private ComboBox<String> tyyppi;
	@FXML
	private TextField kappalemaara;
	@FXML
	private Button addNew;
	@FXML
	private Label ilmoitus;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {

		ObservableList<String> obsKustantajat = FXCollections
				.observableArrayList(LisääKustantaja.populatePublishers());
		kustantaja.getItems().addAll(obsKustantajat);
		kustantaja.setEditable(true);

		ObservableList<String> obsGenret = FXCollections
				.observableArrayList(LisääTyylilaji.populateGenres());
		genre.getItems().addAll(obsGenret);
		genre.setEditable(true);

		ObservableList<String> obsLuokat = FXCollections
				.observableArrayList(LisääLuokka.populateLuokat());
		luokka.getItems().addAll(obsLuokat);
		luokka.setEditable(true);

		ObservableList<String> obsTyypit = FXCollections
				.observableArrayList(LisääTyyppi.populateTyypit());
		tyyppi.getItems().addAll(obsTyypit);
		tyyppi.setEditable(true);
	}

	@FXML
	private void handleAddNewAction(ActionEvent event) {
		if (checkEmpties()) {
			Teos temp = new Teos();
			String[] tempTekijat = tekijat.getText().split(",");
			ArrayList<String> tekijatList = new ArrayList<>();
			for (String tempT : tempTekijat) {
				tekijatList.add(tempT.trim());
			}
			LisääTyylilaji.checkAndAddGenre(genre.getValue());
			temp.setTyylilaji(genre.getValue());
			LisääKustantaja.checkAndAddPublisher(kustantaja.getValue());
			temp.setKustantaja(kustantaja.getValue());
			LisääLuokka.checkAndAddLuokka(luokka.getValue());
			temp.setLuokka(luokka.getValue());
			temp.setTekijät(tekijatList);
			temp.setNimi(teosnimi.getText());
			temp.setJulkaisuVuosi(Integer.parseInt(julkaisu.getText()));
			LisääTyyppi.checkAndAddLuokka(tyyppi.getValue());
			temp.setTyyppi(tyyppi.getValue());
			temp.setKopioidenLkm(Integer.parseInt(kappalemaara.getText()));

			if (SingletonVarasto.getInstance().setTeos(temp)) {
				ilmoitus.setText("Teoksen lisääminen onnistui");
				AdminWindowMainController parentController = ((AdminWindowMainController) myParent
						.getInterface(Constants.ADMIN_MAIN_WINDOW_NAME));
				parentController.getKeskusta().getChildren().remove(0);
				parentController.setButtonsDisabled(false);
				try {
					URL tempURL = this.getClass().getResource(
							Constants.FXML_DIR + "TablePanel.fxml");
					FXMLLoader tempPanel = new FXMLLoader(tempURL);
					parentController.getKeskusta().add(tempPanel.load(), 0, 0);
					AdminWindowMainController vara = ((AdminWindowMainController) myParent
							.getInterface(Constants.ADMIN_MAIN_WINDOW_NAME));
					vara.embeddedTablePanelController = (TablePanelController) tempPanel
							.getController();
					vara.embeddedTablePanelController.teeTeosTaulu();

					((AdminWindowMainController) myParent
							.getInterface(Constants.ADMIN_MAIN_WINDOW_NAME))
							.setLopetaButtonId("lopeta");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				ilmoitus.setText("Teoksen lisääminen epäonnistui");
			}
		}
	}

	private boolean checkEmpties() {
		boolean temp = true;
		for (Node tempNode : newTeos.getChildren()) {
			if (tempNode.getClass() == TextField.class
					|| tempNode.getClass() == PasswordField.class) {
				if (((TextField) tempNode).getText().isEmpty()) {
					tempNode.setStyle(STYLE_VIRHE);
					temp &= false;
				} else {
					tempNode.setStyle(STYLE_OK);
				}
			}
		}
		if (!temp) {
			ilmoitus.setText("Joidenkin kenttien tiedot ovat puutteelliset!");
		}
		return temp;
	}

	@FXML
	private void handleLeaveAction(InputMethodEvent event) {
		TextField temp = (TextField) event.getSource();
		System.err.println(temp.getText().isEmpty());
		if (temp.getText().isEmpty()) {
			temp.setId("virhe");
			temp.requestFocus();
		} else {
			temp.setId("ok");
		}
	}

	@Override
	public void setParent(UiControl screenPage) {
		myParent = screenPage;
	}

	@Override
	public void modifyButtonStates() {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowOpened() {
		// TODO Auto-generated method stub
	}
}
