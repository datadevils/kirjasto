/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxcontrollers;

import fi.datadevils.kirjasto.Constants;
import fi.datadevils.kirjasto.data.User;
import fi.datadevils.kirjasto.fxui.UiControl;
import fi.datadevils.kirjasto.fxui.UiControlInterface;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author Mika
 */
public class NewUserPanelController implements Initializable,
		UiControlInterface {

	private static String STYLE_VIRHE = "-fx-background-color: peachpuff;";
	private static String STYLE_OK = "-fx-background-color: white;";

	UiControl myParent = null;

	@FXML
	private GridPane newUser;
	@FXML
	private Button lopeta;
	@FXML
	private Button muuta;
	@FXML
	private TextField email;
	@FXML
	private TextField etunimi;
	@FXML
	private TextField sukunimi;
	@FXML
	private TextField sakkosaldo;
	@FXML
	private PasswordField salasana;
	@FXML
	private PasswordField salasanaVarmistus;
	@FXML
	private CheckBox admin;
	@FXML
	private Label vääräSalasana;
	@FXML
	private Label ilmoitus;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// TODO
	}

	@FXML
	private void handlePalaaButtonAction(ActionEvent event) {
		GridPane parentController = ((UserWindowMainController) myParent
				.getInterface(Constants.ADMIN_MAIN_WINDOW_NAME)).getKeskusta();
		parentController.getChildren().remove(0);
		try {
			URL tempURL = this.getClass().getResource(
					Constants.FXML_DIR + "TablePanel.fxml");
			FXMLLoader tempPanel = new FXMLLoader(tempURL);
			parentController.add(tempPanel.load(), 0, 0);
			((TablePanelController) tempPanel.getController()).teeTeosTaulu();
		} catch (IOException e) {
			e.printStackTrace();
		}
		myParent.setScreen(Constants.ADMIN_MAIN_WINDOW.getNimi());
	}

	@FXML
	private void handleAddNewAction(ActionEvent event) {
		if (checkEmpties()) {
			User temp = new User();
			temp.setEmail(email.getText());
			temp.setEtunimi(etunimi.getText());
			temp.setSukunimi(sukunimi.getText());
			temp.setSakkosaldo(Double.parseDouble(sakkosaldo.getText()));
			temp.setAdmin(admin.isSelected());
			if (!salasana.getText().equals(salasanaVarmistus.getText())) {
				ilmoitus.setText("Salasanat eivät täsmää!");
				salasana.setStyle(STYLE_VIRHE);
				salasanaVarmistus.setStyle(STYLE_VIRHE);
				return;
			} else {
				salasana.setStyle(STYLE_OK);
				salasanaVarmistus.setStyle(STYLE_OK);
				// lisäätään käyttäjä
				temp.setSalasana(salasana.getText());
				try {
					SingletonVarasto.getInstance().addUser(temp);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				ilmoitus.setText("Uuden käyttäjän\nlisääminen onnistui");

				AdminWindowMainController parentController = ((AdminWindowMainController) myParent
						.getInterface(Constants.ADMIN_MAIN_WINDOW_NAME));
				parentController.getKeskusta().getChildren().remove(0);
				parentController.setButtonsDisabled(false);
				try {
					URL tempURL = this.getClass().getResource(
							Constants.FXML_DIR + "TablePanel.fxml");
					FXMLLoader tempPanel = new FXMLLoader(tempURL);
					parentController.getKeskusta().add(tempPanel.load(), 0, 0);
					((TablePanelController) tempPanel.getController())
							.teeTeosTaulu();

					AdminWindowMainController vara = ((AdminWindowMainController) myParent
							.getInterface(Constants.ADMIN_MAIN_WINDOW_NAME));
					vara.embeddedTablePanelController = (TablePanelController) tempPanel
							.getController();
					vara.embeddedTablePanelController.teeTeosTaulu();

					((AdminWindowMainController) myParent
							.getInterface(Constants.ADMIN_MAIN_WINDOW_NAME))
							.setLopetaButtonId("lopeta");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private boolean checkEmpties() {
		boolean temp = true;
		for (Node tempNode : newUser.getChildren()) {
			if (tempNode.getClass() == TextField.class
					|| tempNode.getClass() == PasswordField.class) {
				if (((TextField) tempNode).getText().isEmpty()) {
					tempNode.setStyle(STYLE_VIRHE);
					temp &= false;
				} else {
					tempNode.setStyle(STYLE_OK);
				}
			}
		}
		if (!temp) {
			ilmoitus.setText("Joidenkin kenttien tiedot ovat puutteelliset!");
		}
		return temp;
	}

	@FXML
	private void handleLeaveAction(InputMethodEvent event) {
		TextField temp = (TextField) event.getSource();
		System.err.println(temp.getText().isEmpty());
		if (temp.getText().isEmpty()) {
			temp.setId("virhe");
			temp.requestFocus();
		} else {
			temp.setId("ok");
		}
	}

	@Override
	public void setParent(UiControl screenPage) {
		myParent = screenPage;
	}

	@Override
	public void modifyButtonStates() {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowOpened() {
		// TODO Auto-generated method stub
	}
}
