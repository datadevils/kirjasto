/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxcontrollers;

import fi.datadevils.kirjasto.Constants;
import fi.datadevils.kirjasto.fxui.UiControl;
import fi.datadevils.kirjasto.fxui.UiControlInterface;
import fi.datadevils.kirjasto.operations.LisääKäyttäjä;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Samuli
 */
public class RegistrationWindowController implements Initializable, UiControlInterface {

    UiControl myParent = null;

    @FXML
    private BorderPane main;
    @FXML
    private Button lopeta;
    @FXML
    private TextField etunimi;
    @FXML
    private TextField sukunimi;
    @FXML
    private TextField email;
    @FXML
    private PasswordField password;
    @FXML
    private Button kirjaudu;
    @FXML
    private Text statustext;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @Override
    public void modifyButtonStates() {
        // 
    }

    @FXML
    private void handleLopetaButtonAction(ActionEvent event) {
        myParent.setScreen(Constants.LOGIN_WINDOW.getNimi());
    }

    @FXML
    private void handleRekisteröidyButtonAction(ActionEvent event) {
        Boolean registerFail = true;
        String etunimi, sukunimi, email, password;

        etunimi = this.etunimi.getText();
        sukunimi = this.sukunimi.getText();
        email = this.email.getText();
        password = this.password.getText();

        if ((etunimi != null && sukunimi != null && email != null && password != null)
        && (!etunimi.isEmpty()) && (!sukunimi.isEmpty()) && (!email.isEmpty()) && (!password.isEmpty())) {
            System.out.println(etunimi + ": " + sukunimi + ": " + email + ": " + password);
            registerFail = LisääKäyttäjä.luo(etunimi, sukunimi, email, password);
        }

        if (!registerFail) {
            statustext.setText("Success");
            myParent.setScreen(Constants.LOGIN_WINDOW.getNimi());
        } else {
            System.out.println("Fail");
            statustext.setText("You have failed!");
        }

    }

    @Override
    public void setParent(UiControl aParent) {
        myParent = aParent;
    }

    @Override
    public void windowOpened() {
    }
}
