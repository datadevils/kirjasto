/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxcontrollers;

import fi.datadevils.kirjasto.KirjastoFX;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import fi.datadevils.kirjasto.data.TeosFX;
import fi.datadevils.kirjasto.data.UserFX;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static fi.datadevils.kirjasto.fxcontrollers.TablePanelController.window.USER;
import fi.datadevils.kirjasto.fxui.AutoComplete;
import fi.datadevils.kirjasto.operations.HaeTeokset;
import fi.datadevils.kirjasto.varasto.Constants;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.ImageCursor;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

/**
 *
 * @author Verneri
 */
public class TablePanelController implements Initializable {

    @FXML
    private GridPane tablePanel;
    @FXML
    public ComboBox<String> searchBox;
    @FXML
    public TextArea info;
    @FXML
    public TableView taulu = new TableView();

    private AutoComplete searchComplete;
    private int object_index;
    private TeosFX tfx = null;
    private UserFX ufx = null;
    private Integer id;
    public boolean usersDisplayed;

    public enum window {

        USER, ADMIN
    }

    public window w = USER;

    public void teeLainaTaulu() {

        taulu.getColumns().clear();
        taulu.getItems().clear();

        TableColumn nimiSarake = new TableColumn("Nimi");
        nimiSarake
        .setCellValueFactory(new PropertyValueFactory<TeosFX, String>(
        "nimi"));
        nimiSarake.setId("nimiSarake");

        TableColumn julkaisuvuosiSarake = new TableColumn("Julkaisuvuosi");
        julkaisuvuosiSarake
        .setCellValueFactory(new PropertyValueFactory<TeosFX, String>(
        "julkaisuvuosi"));
        julkaisuvuosiSarake.setId("julkaisuvuosiSarake");

        TableColumn tekijätSarake = new TableColumn("Tekijät");
        tekijätSarake
        .setCellValueFactory(new PropertyValueFactory<TeosFX, String>(
        "tekijät"));
        tekijätSarake.setId("tekijätSarake");

        TableColumn julkaisijaSarake = new TableColumn("Julkaisija");
        julkaisijaSarake
        .setCellValueFactory(new PropertyValueFactory<TeosFX, String>(
        "kustantaja"));
        julkaisijaSarake.setId("julkaisijaSarake");

        TableColumn palautusSarake = new TableColumn("Palautus pvm.");
        palautusSarake
        .setCellValueFactory(new PropertyValueFactory<TeosFX, String>(
        "palautus"));
        palautusSarake.setId("palautusSarake");

        taulu.getColumns().addAll(nimiSarake, julkaisuvuosiSarake,
        tekijätSarake, julkaisijaSarake, palautusSarake);

        taulu.setId("taulu");

        int userId = SingletonVarasto.getInstance().getMyUser().getId();

        try {
            ResultSet rs = SingletonVarasto
            .getInstance()
            .sendSql(
            ""
            + "select item.id, item.nimi, item.julkaisuvuosi, tekija.nimi, kustantaja.nimi, tyyppi.nimi, unique_item.palautus "
            + "from item, kustantaja, unique_item, tyyppi "
            + "inner join tekijat on item.id=tekijat.item_id "
            + "inner join tekija on tekijat.tekija_id=tekija.id "
            + "where item.kustantaja_id=kustantaja.id "
            + "and item.id=unique_item.item_id "
            + "and item.tyyppi_id=tyyppi.id "
            + "and unique_item.user_id=" + userId);

            displayLainaResults(rs);
        } catch (SQLException ex) {
            Logger.getLogger(TablePanelController.class.getName()).log(
            Level.SEVERE, null, ex);
        }

    }

    public void displayLainaResults(ResultSet rs) throws SQLException {

        ObservableList<TeosFX> data = FXCollections.observableArrayList();

        int count = rs.getMetaData().getColumnCount();
        for (int i = 1; i <= count; i++) {
            TableColumn temp = new TableColumn(rs.getMetaData()
            .getColumnName(i));
            temp.setCellValueFactory(new PropertyValueFactory(rs.getMetaData()
            .getColumnName(i)));
        }

        String[] temp2 = new String[count + 2];
        boolean start = true;
        boolean foundOne = false;
        while (rs.next()) {

            foundOne = true;
            String[] temp = new String[count + 2];
            for (int i = 1; i <= count; i++) {
                if (i == 7) {
                    temp[7] = (i != 10 ? String.valueOf(rs.getObject(i))
                    : String.valueOf(rs.getBoolean(i)));
                } else {
                    temp[i - 1] = (i != 7 ? String.valueOf(rs.getObject(i))
                    : String.valueOf(rs.getBoolean(i)));
                }
            }

            temp[6] = "1";
            Date palautuspvm;
            try {
                palautuspvm = Constants.DEFAULT_OUT_DATE_FORMATTER
                .parse(temp[7]);
                temp[7] = Constants.DEFAULT_OUT_DATE_FORMATTER
                .format(palautuspvm);
            } catch (ParseException ex) {
                Logger.getLogger(TablePanelController.class.getName()).log(
                Level.SEVERE, null, ex);
            }

            if (start == true) {
                temp2 = temp;
                start = false;
            }

            if (temp[0].equalsIgnoreCase(temp2[0])
            && !temp[3].equalsIgnoreCase(temp2[3])) {
                temp[3] += "\n" + temp2[3];
            }

            if (!temp[0].equalsIgnoreCase(temp2[0])) {
                data.add(new TeosFX(temp2));
            }

            temp2 = temp;
        }

        if (foundOne) {
            data.add(new TeosFX(temp2));
        }

        taulu.setItems(data);
        this.usersDisplayed = false;
    }

    public void teeUserTaulu() {

        taulu.getColumns().clear();
        taulu.getItems().clear();
        w = w.ADMIN;

        TableColumn idSarake = new TableColumn("ID");
        idSarake.setCellValueFactory(new PropertyValueFactory<UserFX, String>(
        "id"));
        idSarake.setVisible(false);
        idSarake.setId("idSarake");

        TableColumn emailSarake = new TableColumn("Email");
        emailSarake
        .setCellValueFactory(new PropertyValueFactory<UserFX, String>(
        "email"));
        emailSarake.setId("emailSarake");

        TableColumn etunimiSarake = new TableColumn("Etunimi");
        etunimiSarake
        .setCellValueFactory(new PropertyValueFactory<UserFX, String>(
        "etunimi"));
        etunimiSarake.setId("etunimiSarake");

        TableColumn sukunimiSarake = new TableColumn("Sukunimi");
        sukunimiSarake
        .setCellValueFactory(new PropertyValueFactory<UserFX, String>(
        "sukunimi"));
        sukunimiSarake.setId("sukunimiSarake");

        TableColumn salasanaSarake = new TableColumn("Salasana");
        salasanaSarake
        .setCellValueFactory(new PropertyValueFactory<UserFX, String>(
        "salasana"));
        salasanaSarake.setVisible(false);
        salasanaSarake.setId("salasanaSarake");

        TableColumn sakkosaldoSarake = new TableColumn("Sakkosaldo");
        sakkosaldoSarake
        .setCellValueFactory(new PropertyValueFactory<UserFX, String>(
        "sakkosaldo"));
        sakkosaldoSarake.setId("sakkosaldoSarake");

        TableColumn adminSarake = new TableColumn("Admin");
        adminSarake
        .setCellValueFactory(new PropertyValueFactory<UserFX, String>(
        "admin"));
        adminSarake.setId("adminSarake");

        taulu.getColumns().addAll(idSarake, emailSarake, etunimiSarake,
        sukunimiSarake, salasanaSarake, sakkosaldoSarake, adminSarake);

        taulu.setId("taulu");

        try {
            ResultSet rs = SingletonVarasto.getInstance().sendSql(
            "" + "select * from user");

            displayUserResults(rs);
        } catch (SQLException ex) {
            Logger.getLogger(TablePanelController.class.getName()).log(
            Level.SEVERE, null, ex);
        }
    }

    private void displayUserResults(ResultSet rs) throws SQLException {

        ObservableList<UserFX> data = FXCollections.observableArrayList();

        int count = rs.getMetaData().getColumnCount();

        for (int i = 1; i <= count; i++) {
            TableColumn temp = new TableColumn(rs.getMetaData()
            .getColumnName(i));
            temp.setCellValueFactory(new PropertyValueFactory(rs.getMetaData()
            .getColumnName(i)));
        }

        boolean start = true;
        boolean foundOne = false;

        while (rs.next()) {

            String[] temp = new String[count];

            for (int i = 1; i <= count; i++) {
                temp[i - 1] = String.valueOf(rs.getObject(i));
            }

            UserFX user = new UserFX(temp);

            data.add(user);

        }

        taulu.setItems(data);
        this.usersDisplayed = true;
    }

    public void displayResults(ResultSet rs) throws SQLException {

        ObservableList<TeosFX> data = FXCollections.observableArrayList();

        int count = rs.getMetaData().getColumnCount();
        for (int i = 1; i <= count; i++) {
            TableColumn temp = new TableColumn(rs.getMetaData()
            .getColumnName(i));
            temp.setCellValueFactory(new PropertyValueFactory(rs.getMetaData()
            .getColumnName(i)));
        }

        String[] temp2 = new String[count + 2];
        boolean start = true;
        boolean foundOne = false;
        while (rs.next()) {

            foundOne = true;
            String[] temp = new String[count + 2];
            for (int i = 1; i <= count; i++) {
                temp[i - 1] = (i != 10 ? String.valueOf(rs.getObject(i))
                : String.valueOf(rs.getBoolean(i)));
            }

            temp[6] = "1";
            temp[7] = "null";

            if (start == true) {
                temp2 = temp;
                start = false;
            }

            if (temp[0].equalsIgnoreCase(temp2[0])
            && !temp[3].equalsIgnoreCase(temp2[3])) {
                temp[3] += "\n" + temp2[3];
            }

            if (!temp[0].equalsIgnoreCase(temp2[0])) {
                ResultSet rs2 = SingletonVarasto
                .getInstance()
                .sendSql(
                ""
                + "select item.nimi, unique_item.id, unique_item.item_id "
                + "from item, unique_item "
                + "where item.id=unique_item.item_id "
                + "and item.id=" + '"' + temp2[0] + '"');
                int lukumääräCounter = 0;
                while (rs2.next()) {
                    lukumääräCounter++;
                }
                temp2[6] = Integer.toString(lukumääräCounter);

                data.add(new TeosFX(temp2));
            }

            temp2 = temp;
        }

        // Hätänen bugikorjaus viimesen rivin sijoittamiseen
        // Siistin tämän myöhemmin
        if (foundOne) {
            ResultSet rs2 = SingletonVarasto
            .getInstance()
            .sendSql(
            ""
            + "select item.nimi, unique_item.id, unique_item.item_id "
            + "from item, unique_item "
            + "where item.id=unique_item.item_id "
            + "and item.id=" + '"' + temp2[0] + '"');
            int lukumääräCounter = 0;
            while (rs2.next()) {
                lukumääräCounter++;
            }
            temp2[6] = Integer.toString(lukumääräCounter);

            data.add(new TeosFX(temp2));

        }

        taulu.setItems(data);
        this.usersDisplayed = false;
    }

    public void populateTable(String text) {

        String sqlLause = searchComplete.getDbSearch().parseSearchString(text);

        if (sqlLause == null) {
            return;
        }

        ResultSet rs = null;
        try {
            rs = SingletonVarasto.getInstance().sendSql(sqlLause);
            displayResults(rs);
        } catch (SQLException ex) {
            Logger.getLogger(TablePanelController.class.getName()).log(
            Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        searchComplete = new AutoComplete(searchBox, this);
    }

    @FXML
    protected void handleTableClickAction(MouseEvent event) {
        if (!usersDisplayed) {
            try {
                object_index = taulu.getSelectionModel().getSelectedIndex();
                tfx = (TeosFX) taulu.getSelectionModel().getSelectedItem();
            } catch (NullPointerException ex) {
                System.out.println("valinnassa null pointer exeption");
                Logger.getLogger(TablePanelController.class.getName()).log(
                Level.SEVERE, null, ex);
            }

            if (tfx != null) {
                try {
                    id = tfx.getId();
                    setInfoText(getTeosInfo(Integer.toString(id)));
                    if (w == window.ADMIN) {
                        System.out.println("Table row clicked ADMIN side!");
                    } else if (w == window.USER) {
                        System.out.println("Table row clicked USER side!");
                    }
                } catch (NullPointerException ex) {
                    System.out.println("valinnassa null pointer exeption");
                    Logger.getLogger(TablePanelController.class.getName()).log(
                    Level.SEVERE, null, ex);
                }
            }
        } else {
            try {
                object_index = taulu.getSelectionModel().getSelectedIndex();
                ufx = (UserFX) taulu.getSelectionModel().getSelectedItem();
            } catch (NullPointerException ex) {
                System.out.println("valinnassa null pointer exeption");
                Logger.getLogger(TablePanelController.class.getName()).log(
                Level.SEVERE, null, ex);
            }
            if (ufx != null) {
                try {
                    id = ufx.getId();
                    System.err.println("handleTableClickAction(MouseEvent event): " + id);
                    setInfoText(getUserInfo(Integer.toString(id)));
                    System.out.println("Table row clicked ADMIN side!");
                } catch (NullPointerException ex) {
                    System.out.println("valinnassa null pointer exeption");
                    Logger.getLogger(TablePanelController.class.getName()).log(
                    Level.SEVERE, null, ex);
                }
            }
        }
    }

    @FXML
    protected void handleTableDragAction(MouseEvent event
    ) {
        if (!usersDisplayed) {
            startTeosTableDrag();
        } else {
            startUserTableDrag();
        }
    }

    private void startTeosTableDrag() {
        try {
            tfx = (TeosFX) taulu.getSelectionModel().getSelectedItem();
        } catch (NullPointerException ex) {
            System.out.println("drag:ssa null pointer exeption");
            KirjastoFX.scene.setCursor(Cursor.DEFAULT);
        }

        if (!(tfx == null)) {
            try {

                taulu.startFullDrag();

                if (tfx.getTyyppi().equals("CD")) {
                    Image tempI = new Image(
                    "/fi/datadevils/kirjasto/resources/cursor_lehti.png");
                    KirjastoFX.scene.setCursor(new ImageCursor(new Image(
                    "/fi/datadevils/kirjasto/resources/"
                    + "cursor_cd.png"), 64, 64));
                } else if (tfx.getTyyppi().equals("Lehti")) {
                    KirjastoFX.scene.setCursor(new ImageCursor(new Image(
                    "/fi/datadevils/kirjasto/resources/"
                    + "cursor_lehti.png"), 64, 64));
                } else if (tfx.getTyyppi().equals("Kirja")) {
                    KirjastoFX.scene.setCursor(new ImageCursor(new Image(
                    "fi/datadevils/kirjasto/resources/"
                    + "cursor_kirja.png"), 64, 64));
                }

                if (w == window.ADMIN) {
                    System.out.println("Drag started  ADMIN side");

                } else if (w == window.USER) {
                    System.out.println("Drag started  USER side");
                }

            } catch (NullPointerException ex) {
                System.out.println("drag:ssa null pointer exeption");
                KirjastoFX.scene.setCursor(Cursor.DEFAULT);
            }
        } else {
            System.out.println("tfx luonnissa ongelma dragissä!");
        }
    }

    private void startUserTableDrag() {
        try {
            ufx = (UserFX) taulu.getSelectionModel().getSelectedItem();
        } catch (NullPointerException ex) {
            System.out.println("drag:ssa null pointer exeption");
            KirjastoFX.scene.setCursor(Cursor.DEFAULT);
        }

        if (!(ufx == null)) {
            try {

                taulu.startFullDrag();

                KirjastoFX.scene.setCursor(new ImageCursor(new Image(
                "/fi/datadevils/kirjasto/resources/" + "hemmo.png"),
                64, 64));

                if (w == window.ADMIN) {
                    System.out.println("Drag started  ADMIN side");

                } else if (w == window.USER) {
                    System.out.println("Drag started  USER side");
                }

            } catch (NullPointerException ex) {
                System.out.println("drag:ssa null pointer exeption");
                KirjastoFX.scene.setCursor(Cursor.DEFAULT);
            }
        } else {
            System.out.println("tfx luonnissa ongelma dragissä!");
        }
    }

    public void teeTeosTaulu() {
        // Alustetaan teoksille sopiva taulu
        // TableView taulu = new TableView();
        taulu.getColumns().clear();
        taulu.getItems().clear();

        w = w.ADMIN;

        TableColumn nimiSarake = new TableColumn("Nimi");
        nimiSarake
        .setCellValueFactory(new PropertyValueFactory<TeosFX, String>(
        "nimi"));
        nimiSarake.setId("nimiSarake");

        TableColumn julkaisuvuosiSarake = new TableColumn("Julkaisuvuosi");
        julkaisuvuosiSarake
        .setCellValueFactory(new PropertyValueFactory<TeosFX, String>(
        "julkaisuvuosi"));
        julkaisuvuosiSarake.setId("julkaisuvuosiSarake");

        TableColumn tekijätSarake = new TableColumn("Tekijät");
        tekijätSarake
        .setCellValueFactory(new PropertyValueFactory<TeosFX, String>(
        "tekijät"));
        tekijätSarake.setId("tekijätSarake");

        TableColumn julkaisijaSarake = new TableColumn("Julkaisija");
        julkaisijaSarake
        .setCellValueFactory(new PropertyValueFactory<TeosFX, String>(
        "kustantaja"));
        julkaisijaSarake.setId("julkaisijaSarake");

        TableColumn lukumääräSarake = new TableColumn("Lukumäärä");
        lukumääräSarake
        .setCellValueFactory(new PropertyValueFactory<TeosFX, String>(
        "lukumäärä"));
        lukumääräSarake.setId("lukumääräSarake");

        taulu.getColumns().addAll(nimiSarake, julkaisuvuosiSarake,
        tekijätSarake, julkaisijaSarake, lukumääräSarake);

        taulu.setId("taulu");
        // tablePanel.add(taulu, 0, 1);

        try {
            ResultSet rs = SingletonVarasto
            .getInstance()
            .sendSql(
            ""
            + "select item.id, item.nimi, item.julkaisuvuosi, tekija.nimi, kustantaja.nimi, tyyppi.nimi "
            + "from item, kustantaja, tyyppi "
            + "inner join tekijat on item.id=tekijat.item_id "
            + "inner join tekija on tekijat.tekija_id=tekija.id "
            + "where item.kustantaja_id=kustantaja.id and "
            + "item.tyyppi_id=tyyppi.id");
            displayResults(rs);

        } catch (SQLException ex) {
            Logger.getLogger(TablePanelController.class
            .getName()).log(
            Level.SEVERE, null, ex);
        }

    }

    private ArrayList<String> getTeosInfo(String id) {
        ArrayList<String> info = new ArrayList<>();
        String temp = "";

        String aSql1 = "select item.id, item.nimi, item.julkaisuvuosi "
        + "from item where item.id=" + id;
        String aSql2 = "select tekija.nimi " + "from item "
        + "inner join tekijat on item.id=tekijat.item_id "
        + "inner join tekija on tekijat.tekija_id=tekija.id "
        + "where item.id=" + id;
        String aSql3 = "select kustantaja.nimi, tyyppi.nimi, tyylilaji.nimi, luokka.nimi "
        + "from item, kustantaja, tyyppi, tyylilaji, luokka "
        + "where item.kustantaja_id=kustantaja.id "
        + "and item.tyyppi_id=tyyppi.id "
        + "and item.tyylilaji_id=tyylilaji.id "
        + "and item.luokka_id=luokka.id " + "and item.id=" + id;

        try {
            ResultSet rs = SingletonVarasto.getInstance().sendSql(aSql1);
            while (rs.next()) {
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    if (i == 1) {
                        temp = "Id: ";
                    } else if (i == 2) {
                        temp = "Nimi: ";
                    } else if (i == 3) {
                        temp = "Julkaisuvuosi: ";
                    }

                    temp += rs.getString(i);
                    info.add(temp);
                }
            }

            rs = SingletonVarasto.getInstance().sendSql(aSql2);
            int lenght = rs.getFetchSize();
            int currentRow = 0;
            temp = "Tekijät: ";
            while (rs.next()) {
                temp += rs.getString(1);
                if (currentRow <= lenght) {
                    temp += ", ";
                    currentRow++;
                }
            }
            info.add(temp);

            temp = "";
            rs = SingletonVarasto.getInstance().sendSql(aSql3);
            while (rs.next()) {
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    if (i == 1) {
                        temp = "Kustantaja: ";
                    } else if (i == 2) {
                        temp = "Tyyppi: ";
                    } else if (i == 3) {
                        temp = "Tyylilaji: ";
                    } else if (i == 4) {
                        temp = "Luokka: ";
                    }
                    temp += rs.getString(i);
                    info.add(temp);
                }
            }

            info.addAll(HaeTeokset.lisätiedotHaeJaTulosta(id));

        } catch (SQLException ex) {
            Logger.getLogger(TablePanelController.class
            .getName()).log(
            Level.SEVERE, null, ex);
        }

        return info;
    }

    private ArrayList<String> getUserInfo(String id) {
        ArrayList<String> tInfo = new ArrayList<>();
        String[] tTitles = {"Id: ", "Etunimi: ", "Sukunimi: ", "Email: ", "Sakkosaldo: ", "Admin: "};

        String tSql = "select id, etunimi, sukunimi, email, sakkosaldo, admin "
        + "from user where id=" + id;

        try {
            ResultSet rs = SingletonVarasto.getInstance().sendSql(tSql);
            while (rs.next()) {
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    if (i != 6) {
                        tInfo.add(tTitles[i - 1] + rs.getString(i));
                    } else {
                        tInfo.add(tTitles[i - 1] + rs.getBoolean(i));
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(TablePanelController.class.getName()).log(
            Level.SEVERE, null, ex);
        }
        return tInfo;
    }

    public void setInfoText(ArrayList<String> textList) {
        // vaihto kohdassa siirrytään suoraan seuraavalle riville
        // saadaa pitemmät listat toiseen sarakkeeseen.
        final int VAIHTO = 9;
        info.clear();
        int j = 0;
        for (int i = 0; i < textList.size(); i++) {
            j = (i < VAIHTO ? i : i + 1);
            if (j % 2 == 0) {
                info.appendText(textList.get(i));
                if (j == VAIHTO - 1) {
                    info.appendText("\n");
                }
            } else {
                info.appendText(setTab(info.getCaretPosition(), 35)
                + textList.get(i) + "\n");
            }
        }
    }

    private String setTab(int aCaret, int aColumn) {
        String tab = "";
        int rivinVaihto = info.getText().lastIndexOf('\n');
        while (aCaret++ < aColumn + rivinVaihto) {
            tab += " ";
        }
        return tab;
    }

    @FXML
    protected void onDragOver(MouseDragEvent event) {
        System.out.println("Drag over  ADMIN side");
    }

    @FXML
    protected void onDragReleased(MouseDragEvent event) {
        System.out.println("Drag released outside trash  ADMIN side!");
        KirjastoFX.scene.setCursor(Cursor.DEFAULT);
    }

}
