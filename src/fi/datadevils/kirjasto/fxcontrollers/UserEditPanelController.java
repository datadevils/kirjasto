/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxcontrollers;

import fi.datadevils.kirjasto.Constants;
import fi.datadevils.kirjasto.data.User;
import fi.datadevils.kirjasto.fxui.UiControl;
import fi.datadevils.kirjasto.fxui.UiControlInterface;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author Verneri
 */
public class UserEditPanelController implements Initializable,
		UiControlInterface {

	UiControl myParent = null;

	@FXML
	private GridPane userMuokkaa;
	@FXML
	private Button lopeta;
	@FXML
	private Button muuta;
	@FXML
	private TextField email;
	@FXML
	private TextField etunimi;
	@FXML
	private TextField sukunimi;
	@FXML
	private TextField sakkosaldo;
	@FXML
	private PasswordField salasana;
	@FXML
	private PasswordField salasanaVarmistus;
	@FXML
	private CheckBox admin;
	@FXML
	private Label vaaraSalasana;
	@FXML
	private Label ilmoitus;

	/**
	 * Initializes the controller class.
	 */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// TODO
	}

	@FXML
	private void handlePalaaButtonAction(ActionEvent event) {
		GridPane parentController = ((UserWindowMainController) myParent
				.getInterface(Constants.USER_MAIN_WINDOW_NAME)).getKeskusta();
		parentController.getChildren().remove(0);
		try {
			URL tempURL = this.getClass().getResource(
					Constants.FXML_DIR + "TablePanel.fxml");
			FXMLLoader tempPanel = new FXMLLoader(tempURL);
			parentController.add(tempPanel.load(), 0, 0);
			((TablePanelController) tempPanel.getController()).teeTeosTaulu();
		} catch (IOException e) {
			e.printStackTrace();
		}
		myParent.setScreen(Constants.USER_MAIN_WINDOW.getNimi());
	}

	@FXML
	private void handleMuutaTietojaAction(ActionEvent event) {
		User temp = SingletonVarasto.getInstance().getMyUser();
		temp.setEtunimi(etunimi.getText());
		temp.setSukunimi(sukunimi.getText());
		if (salasana.getText().equals(salasanaVarmistus.getText())) {
			temp.setSalasana(salasana.getText());
			try {
				SingletonVarasto.getInstance().modyfyUser(temp);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ilmoitus.setText("Tietojen muuttaminen onnistui");

			GridPane parentController = ((UserWindowMainController) myParent
					.getInterface(Constants.USER_MAIN_WINDOW_NAME))
					.getKeskusta();
			parentController.getChildren().remove(0);
			try {
				URL tempURL = this.getClass().getResource(
						Constants.FXML_DIR + "TablePanel.fxml");
				FXMLLoader tempPanel = new FXMLLoader(tempURL);
				parentController.add(tempPanel.load(), 0, 0);
				((TablePanelController) tempPanel.getController())
						.teeTeosTaulu();
				((UserWindowMainController) myParent
						.getInterface(Constants.USER_MAIN_WINDOW_NAME))
						.setLopetaButtonId("lopeta");
				((UserWindowMainController) myParent
						.getInterface(Constants.USER_MAIN_WINDOW_NAME)).setCurrentUser();
                                ((UserWindowMainController) myParent
						.getInterface(Constants.USER_MAIN_WINDOW_NAME)).setButtonsDisabled(false);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			ilmoitus.setText("SALASANA EI TÄSMÄÄ!");
		}
	}

	public void populateFields(User aUser) {
		ilmoitus.setText("");
		vaaraSalasana.setText("");
		email.setText(aUser.getEmail());
		etunimi.setText(aUser.getEtunimi());
		sukunimi.setText(aUser.getSukunimi());
		salasana.setText(aUser.getSalasana());
		salasanaVarmistus.setText(aUser.getSalasana());
		String sakkoString = String.valueOf(aUser.getSakkosaldo());
		sakkosaldo.setText(sakkoString + " " + "€");
		admin.setSelected(aUser.getAdmin());
	}

	@Override
	public void setParent(UiControl screenPage) {
		myParent = screenPage;
	}

	@Override
	public void modifyButtonStates() {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened() {
		// TODO Auto-generated method stub

	}
}
