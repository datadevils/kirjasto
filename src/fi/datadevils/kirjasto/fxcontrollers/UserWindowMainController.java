/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxcontrollers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import fi.datadevils.kirjasto.Constants;
import fi.datadevils.kirjasto.KirjastoFX;
import fi.datadevils.kirjasto.data.TeosFX;
import fi.datadevils.kirjasto.data.User;
import fi.datadevils.kirjasto.fxui.UiControl;
import fi.datadevils.kirjasto.fxui.UiControlInterface;
import fi.datadevils.kirjasto.operations.LainaaTeos;
import static fi.datadevils.kirjasto.operations.LainaaTeos.haeKäyttäjänKaikkiLainat;
import static fi.datadevils.kirjasto.operations.LainaaTeos.lainaaKappale;
import fi.datadevils.kirjasto.operations.PoistaTeos;
import fi.datadevils.kirjasto.operations.PoistaVaraus;
import fi.datadevils.kirjasto.operations.VaraaKirja;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Jarkko
 */
public class UserWindowMainController implements Initializable,
        UiControlInterface {

    UiControl myParent = null;

    @FXML
    private Label current_user;
    @FXML
    private Button kirjastoButton;

    @FXML
    private Button toAdmin;

    @FXML
    private Parent userEditPanel;

    @FXML
    private UserEditPanelController userEditPanelController;

    @FXML
    private GridPane keskusta;

    @FXML
    private Button editUser;
    @FXML
    private Button listBook;

    @FXML
    private Parent embeddedTablePanel;

    @FXML
    private TablePanelController embeddedTablePanelController;

    @FXML
    private Button lopeta;

    @FXML
    private Button userButton;

    private int object_index;
    private TeosFX tfx = null;
    private Integer id;

    private boolean varataanko = false;

    private PoistaTeos poistaTeosOlio = new PoistaTeos(
            SingletonVarasto.getInstance());

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        listBook.setVisible(false);
    }

    @Override
    public void setParent(UiControl aParent) {
        myParent = aParent;
    }

    @Override
    public void modifyButtonStates() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @FXML
    private void handleLopetaButtonAction(ActionEvent event) {
        if (lopeta.getId().matches("palaa")) {
            setButtonsDisabled(false);
            setLopetaButtonId("lopeta");
            GridPane parentController = ((UserWindowMainController) myParent
                    .getInterface(Constants.USER_MAIN_WINDOW_NAME))
                    .getKeskusta();
            parentController.getChildren().remove(0);
            try {
                URL tempURL = this.getClass().getResource(
                        Constants.FXML_DIR + "TablePanel.fxml");
                FXMLLoader tempPanel = new FXMLLoader(tempURL);
                parentController.add(tempPanel.load(), 0, 0);

                embeddedTablePanelController = ((TablePanelController) tempPanel
                        .getController());
                embeddedTablePanelController.w = embeddedTablePanelController.w.USER;
                embeddedTablePanelController.teeTeosTaulu();

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {
            myParent.setScreen(Constants.LOGIN_WINDOW.getNimi());
        }
    }

    @FXML
    private void handleToAdminButtonAction(ActionEvent event) {
        myParent.setScreen(Constants.ADMIN_MAIN_WINDOW.getNimi());
    }

    @FXML
    private void handleKirjastoButtonAction(ActionEvent event) {
        embeddedTablePanelController.teeTeosTaulu();
    }

    @FXML
    private void handleUserButtonAction(ActionEvent event) {
        embeddedTablePanelController.teeLainaTaulu();
    }

    @FXML
    public void handleDropToBagAction(MouseDragEvent event) {
        System.out.println("Adding to bag USER side");

        try {
            if (!(embeddedTablePanelController.taulu.getItems().isEmpty())) {

                object_index = embeddedTablePanelController.taulu.getSelectionModel().getSelectedIndex();
                tfx = (TeosFX) embeddedTablePanelController.taulu.getSelectionModel().getSelectedItem();

                if (tfx != null) {
                    id = tfx.getId();

                    embeddedTablePanelController.info.clear();
                    KirjastoFX.scene.setCursor(Cursor.DEFAULT);

                    ArrayList<String> vastaus_unique_item;
                    ArrayList<String> vastausVaraus;

                    // haetaan kaikki varaukset taulusta, joiden item_id
                    // on id. 
                    vastausVaraus = VaraaKirja.haeVaraukset(id);

                    String[] vastaus;
                    int id__unique_item = 0;
                    int varausId = -1;
                    int pieninVarausNro = 99999999;
                    boolean ei_lainassa = true;
                    boolean onkoVarauksia = vastausVaraus.size() > 0 ? true : false;
                    boolean onkoPieninVarausOma = false;
                    boolean onkoOmaVaraus = false;
                    boolean lainausOnnistui = false;

                    User user = SingletonVarasto.getMyUser();

                    // tutkitaan teoksen varaustilannetta
                    for (int i = 0; i < vastausVaraus.size(); i++) {
                        
                        // splitataan varaus
                        vastaus = vastausVaraus.get(i).split(Pattern.quote(";"));
                        varausId = parseInt(vastaus[0].trim());

                        if (parseInt(vastaus[3]) == user.getId()) {
                            onkoOmaVaraus = true;
                        }

                        if (pieninVarausNro > parseInt(vastaus[2])){
                            pieninVarausNro = parseInt(vastaus[2]);
                            if (parseInt(vastaus[3]) == user.getId()) {
                                onkoPieninVarausOma = true;
                                onkoOmaVaraus = true;
                            } else {
                                onkoPieninVarausOma = false;
                            }
                        } 
                    }

                    if (onkoVarauksia) {
                        if (!onkoPieninVarausOma) {
                            embeddedTablePanelController.info.clear();
                            if (onkoOmaVaraus) {
                                embeddedTablePanelController.info.setText("Teosta ei voi lainata, koska siitä on varauksia. Sinulla on varaus mutta teos on varattu sinua ennen. ");
                            } else {
                                embeddedTablePanelController.info.setText("Teosta ei voi lainata, koska siitä on varauksia.");
                                // Avataan ikkuna ja jäädään odottamaan
                                // nappien eventtejä
                                openPopup();
                                if (varataanko) {
                                    if (!varaaTeos(id, user)) {
                                        embeddedTablePanelController.info
                                                .appendText(" Varaus epäonnistui!");
                                    }
                                }
                            }
                            return;
                        }
                    }

                    // jatketaan tästä eteenpäin mikäli varauksia ei ole, tai jos pienin varaus on oma
                    
                    // haetaan kaikki kappaleet unique_item taulusta, joiden item_id
                    // on id. Älä hämäänny poistaTeosOliosta
                    vastaus_unique_item = poistaTeosOlio.haeKappaleetFx(id);
                    // Käydään läpi kaikki unique_item taulun alkiot, ja jos joku
                    // on vapaa, niin lainataan se.
                    for (int i = 0; i < vastaus_unique_item.size(); i++) {

                        // splitataan unique_item
                        vastaus = vastaus_unique_item.get(i).split(
                                Pattern.quote(";"));

                        // otetaan id
                        id__unique_item = parseInt(vastaus[0].trim());

                        // jos user_id on null ja käyttäjällä ei ole muita
                        // kappaleita lainassa, niin voidaan lainata.
                        if (vastaus[3].equals("<NULL>") || vastaus[3].equals("null")) {
                            if (!(onkoMuitaLainassa(user, id__unique_item, id))) {
                                ei_lainassa = true;
                                userButton.setId("userButton_full");

                                if (!lainaaKappale(id__unique_item, user)) {
                                    System.out.println("lainaus epäonnistui!");
                                    continue;
                                } else lainausOnnistui=true;
                                embeddedTablePanelController.info.clear();
                                embeddedTablePanelController.info.setText(
                                        "Teos lainattu! ");
                                break;
                            } else {
                                ei_lainassa = false;
                                break;
                            }
                            // ei ole vapaa
                        } else {
                            ei_lainassa = false;

                            // Kuitenkin jos kappaleen user_id on sama kuin nykyinen
                            // käyttäjä, niin uusitaan laina, ellei teoksen toinen kappale
                            // ole jo lainattu kyseiselle käyttäjälle.
                            if (parseInt(vastaus[3]) == user.getId()) {
                                if (!onkoMuitaLainassa(user, id__unique_item, id)) {
                                    ei_lainassa = true;

                                    if (!lainaaKappale(id__unique_item, user)) {
                                        System.out.println("lainaus epäonnistui!");
                                        continue;
                                    } else lainausOnnistui=true;
                                    String palautuspäivä = vastaus[2];
                                    embeddedTablePanelController.info.clear();
                                    embeddedTablePanelController.info.setText(
                                            "Lainasi on uusittu! Uusi eräpäivä on "
                                            + palautuspäivä + "!");
                                    break;
                                } else {
                                    ei_lainassa = false;
                                    break;
                                }
                            }
                        }
                    }

                    // Jos kaikki teokset olivat lainassa, niin tulostetaan,
                    // että ei voi lainata teosta ja kysytään varataanko teos.
                    if (ei_lainassa == false) {
                        embeddedTablePanelController.info
                                .appendText("Teosta ei voi lainata, koska kaikki kappaleet ovat lainassa!");

                        // Avataan ikkuna ja jäädään odottamaan
                        // nappien eventtejä
                        openPopup();

                        if (varataanko) {
                            if (!varaaTeos(id, user)) {
                                embeddedTablePanelController.info
                                        .appendText(" Varaus epäonnistui!");
                            }
                        }
                    }
                    
                    
                    if((lainausOnnistui)&&(onkoPieninVarausOma)){
                        PoistaVaraus.poistaVarausFx(SingletonVarasto.getInstance(), id, user.getId());
                        System.out.println("Teos lainattu, varaus poistettu");
                    }
                    

                } else {
                    System.out.println("tfx luonti epäonnistui lainauksessa USER puoli");
                }
            }
        } catch (NullPointerException ex) {

            Logger.getLogger(AdminWindowMainController.class.getName()).log(
                    Level.SEVERE, null, ex);
            System.out.println("Dropissa null pointer exeption");
        }
    }

    private boolean onkoMuitaLainassa(User user, int id__unique_item, int item_id__unique_item) {

        ArrayList<String> käyttäjän_lainat = new ArrayList<>();

        käyttäjän_lainat = LainaaTeos.haeKäyttäjänLainat(user, id__unique_item, item_id__unique_item);

        return käyttäjän_lainat.size() > 0;
    }

    private void openPopup() {

        Stage newStage = new Stage(StageStyle.DECORATED);
        BorderPane root = new BorderPane();
        HBox box = new HBox();
        HBox box2 = new HBox();
        Label text = new Label();
        Button no_button = new Button();
        Button yes_button = new Button();

        yes_button.setText("Kyllä");
        no_button.setText("Ei");

        text.setText("Haluatko varata teoksen?");

        root.resize(200, 100);
        box.getChildren().add(text);
        box2.getChildren().add(yes_button);
        box2.getChildren().add(no_button);
        box.alignmentProperty().setValue(Pos.CENTER);
        box2.alignmentProperty().setValue(Pos.CENTER);
        box.setPadding(new Insets(10, 10, 10, 10));
        box2.setPadding(new Insets(10, 10, 10, 10));
        box2.setSpacing(10);

        BackgroundFill bf = new BackgroundFill(Color.IVORY, CornerRadii.EMPTY, Insets.EMPTY);
        BackgroundFill bf2 = new BackgroundFill(Color.SANDYBROWN, CornerRadii.EMPTY, Insets.EMPTY);
        Background bg = new Background(bf);
        Background bg2 = new Background(bf2);
        root.setBackground(bg);
        box.backgroundProperty().set(bg);
        box2.backgroundProperty().set(bg);
        yes_button.setBackground(bg2);
        no_button.setBackground(bg2);
        newStage.getIcons().add(new Image("fi/datadevils/kirjasto/resources/datadevils.png"));

        root.setTop(box);
        root.setCenter(box2);

        no_button.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("no button pressed!");
                varataanko = false;
                newStage.close();
            }
        });

        yes_button.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("yes button pressed!");
                varataanko = true;
                newStage.close();
            }
        });

        Scene stageScene = new Scene(root, 200, 100);
        newStage.setScene(stageScene);
        newStage.setTitle("Varaus!");
        newStage.setAlwaysOnTop(true);
        newStage.showAndWait();
    }

    private boolean varaaTeos(int id, User user) {
        System.out.println("Varausta kutsuttu");
        int user_id = (int) user.getId();
        if (VaraaKirja.varaaKirja(SingletonVarasto.getInstance(), id, user_id) != -1) {
            embeddedTablePanelController.info.clear();
            embeddedTablePanelController.info.setText("Teos varattu!");
            return true;
        } else {
            embeddedTablePanelController.info.clear();
            embeddedTablePanelController.info.setText(
                    "Teos on jo varattu tai lainattu sinulle!");
            return false;
        }
    }

    @FXML
    private void handleEditUserButtonAction(ActionEvent event) {
        if (keskusta.getChildren().size() > 0) {
            keskusta.getChildren().remove(0);
        }
        setButtonsDisabled(true);
        try {
            URL temp = this.getClass().getResource(
                    Constants.FXML_DIR + "UserEditPanel.fxml");
            FXMLLoader tempPanel = new FXMLLoader(temp);
            keskusta.add(tempPanel.load(), 0, 0);
            ((UserEditPanelController) tempPanel.getController())
                    .setParent(myParent);
            ((UserEditPanelController) tempPanel.getController())
                    .populateFields(SingletonVarasto.getInstance().getMyUser());
            setLopetaButtonId("palaa");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @FXML
    public void handleDropToShelfAction(MouseDragEvent event) {

        System.out.println("Adding to shelf USER side");

        KirjastoFX.scene.setCursor(Cursor.DEFAULT);

        try {
            if (!(embeddedTablePanelController.taulu.getItems().isEmpty())) {

                object_index = embeddedTablePanelController.taulu.getSelectionModel().getSelectedIndex();
                tfx = (TeosFX) embeddedTablePanelController.taulu.getSelectionModel().getSelectedItem();

                ArrayList<String> lainat = null;
                int uId = SingletonVarasto.getMyUser().getId();
                int lainatLkm = 0;
                if (tfx != null) {
                    id = tfx.getId();

                    embeddedTablePanelController.info.clear();

                    try {
                        lainat = SingletonVarasto.getInstance().getFull("select unique_item.id from unique_item where unique_item.user_id = " + uId + ";");
                        lainatLkm = lainat.size();
                    } catch (SQLException ex) {
                        Logger.getLogger(UserWindowMainController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    if (!lainat.isEmpty()) {
                        try {
                            SingletonVarasto.getInstance().sendSqlNRS("update unique_item set user_id=? where item_id = " + id + ";");
                        } catch (SQLException ex) {
                            Logger.getLogger(UserWindowMainController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        System.out.println("Lainoja ei löytynyt");
                    }

                    try {
                        lainat = SingletonVarasto.getInstance().getFull("select unique_item.id from unique_item where unique_item.user_id = " + uId + ";");
                    } catch (SQLException ex) {
                        Logger.getLogger(UserWindowMainController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    if (lainat.size() == (lainatLkm - 1)) {
                        System.out.println("Palautus onnistui");
                        embeddedTablePanelController.taulu.getItems().remove(tfx);
                        if (embeddedTablePanelController.taulu.getItems().size() == 0) {
                            userButton.setId("userButton");
                        }
                    }
                }
            }
        } catch (NullPointerException ex) {

            Logger.getLogger(AdminWindowMainController.class.getName()).log(
                    Level.SEVERE, null, ex);
            System.out.println("Dropissa null pointer exeption");
        }

    }

    @FXML
    private void setOnDragOver(MouseDragEvent event) {
        System.out.println("Drag over USER side");
    }

    @FXML
    private void setOnEntered(MouseDragEvent event) {
        System.out.println("Drag entered  USER side");
    }

    @FXML
    private void setOnDragRelease(MouseDragEvent event) {
        System.out.println("Drag released USER side!");
        KirjastoFX.scene.setCursor(Cursor.DEFAULT);
    }

    @Override
    public void windowOpened() {
        setButtonsDisabled(false);
        setCurrentUser();
        toAdmin.setDisable(!SingletonVarasto.getInstance().getMyUserIsAdmin()
                .getValue());
        embeddedTablePanelController.teeTeosTaulu();
        // embeddedTablePanelController.setInfoText(SingletonVarasto.getInstance()
        // .getMyUser());
        embeddedTablePanelController.w = embeddedTablePanelController.w.USER;
        embeddedTablePanelController.info.clear();
    }

    public void setCurrentUser() {
        current_user.setText(SingletonVarasto.getInstance().getMyUserName()
                .getValue());
        User user = SingletonVarasto.getInstance().getMyUser();
        ArrayList<String> lainat = haeKäyttäjänKaikkiLainat(user);
        if((lainat!=null)&&(lainat.size()>0)){
            userButton.setId("userButton_full");
        }
    }

    public GridPane getKeskusta() {
        return keskusta;
    }

    public void setLopetaButtonId(String aStatus) {
        lopeta.setId(aStatus);
    }

    public void setButtonsDisabled(boolean aState) {
        listBook.setDisable(aState);
        editUser.setDisable(aState);
        kirjastoButton.setDisable(aState);
        toAdmin.setDisable(aState);
        userButton.setDisable(aState);
    }
}
