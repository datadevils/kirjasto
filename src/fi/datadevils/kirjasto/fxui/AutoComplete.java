/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxui;

import fi.datadevils.kirjasto.fxcontrollers.TablePanelController;
import fi.datadevils.kirjasto.operations.HakuToiminto;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author Samuli
 */
public class AutoComplete implements EventHandler<KeyEvent> {

    public ComboBox<String> comboBox;
    private TablePanelController myParent;
    private StringBuilder sb;
    private ObservableList<String> oList;
    private boolean moveCaretToPos = false;
    private int caretPos;
    public HakuToiminto dbSearch;

    public AutoComplete(ComboBox comboBox, TablePanelController myParent) {
        this.comboBox = comboBox;
        this.myParent = myParent;
        sb = new StringBuilder();
        oList = comboBox.getItems();

        oList.addListener(new ListChangeListener() {
            @Override
            public void onChanged(ListChangeListener.Change change) {
                comboBox.setItems(oList);
                if (!oList.isEmpty()) {
                    comboBox.show();
                    caretPos = -1;
                    moveCaret(comboBox.getEditor().getText().length());
                }
            }
        });

        this.comboBox.addEventFilter(KeyEvent.KEY_PRESSED, this);

        this.comboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

            public void changed(ObservableValue ov, Object old_val, Object new_val) {
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                myParent.populateTable((String)comboBox.getValue());
            }

        });

        dbSearch = new HakuToiminto(AutoComplete.this);
    }

    @Override
    public void handle(KeyEvent event) {
        if (event.getCode() == KeyCode.UP) {
            caretPos = -1;
            moveCaret(comboBox.getEditor().getText().length());
            return;
        } else if (event.getCode() == KeyCode.DOWN) {
            if (!comboBox.isShowing()) {
                comboBox.show();
            }
            caretPos = -1;
            moveCaret(comboBox.getEditor().getText().length());
            return;
        } else if (event.getCode() == KeyCode.BACK_SPACE) {
            moveCaretToPos = true;
            caretPos = comboBox.getEditor().getCaretPosition();
            return;
        } else if (event.getCode() == KeyCode.DELETE) {
            moveCaretToPos = true;
            caretPos = comboBox.getEditor().getCaretPosition();
            return;
        } else if (event.getCode() == KeyCode.RIGHT
                || event.getCode() == KeyCode.LEFT
                || event.getCode() == KeyCode.HOME
                || event.getCode() == KeyCode.END
                || event.getCode() == KeyCode.TAB) {
            return;
        } else if (event.getCode() == KeyCode.ENTER) {
            caretPos = -1;
            moveCaret(comboBox.getEditor().getText().length());
            myParent.populateTable(comboBox.getEditor().getText());
            if (comboBox.isShowing()) {
                comboBox.hide();
            }
            return;
        }

        dbSearch.start(this.comboBox.getEditor().getText().toLowerCase(), oList);

        String t = (String) comboBox.getEditor().getText();
        comboBox.getEditor().setText(t);

        if (!moveCaretToPos) {
            caretPos = -1;
        }

        moveCaret(t.length());
    }

    private void moveCaret(int textLength) {
        if (caretPos == -1) {
            comboBox.getEditor().positionCaret(textLength);
        } else {
            comboBox.getEditor().positionCaret(caretPos);
        }
        moveCaretToPos = false;
    }

    public HakuToiminto getDbSearch() {
        return dbSearch;
    }
 
}
