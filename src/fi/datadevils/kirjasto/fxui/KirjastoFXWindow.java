/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxui;

/**
 *
 * @author mika
 */
public class KirjastoFXWindow {

    private String myNimi = null;
    private String myFXML = null;

    public KirjastoFXWindow(String aNimi, String aFXML) {
        myNimi = aNimi;
        myFXML = aFXML;
    }

    public String getNimi() {
        return myNimi;
    }

    public String getFXML() {
        return myFXML;
    }
}
