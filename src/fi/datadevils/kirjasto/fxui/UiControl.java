/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxui;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;

/**
 *
 * @author mika
 */
public class UiControl extends StackPane {

    private HashMap<String, Node> myScreens = new HashMap<>();
    private HashMap<String, UiControlInterface> myInterfaces = new HashMap<>();

    public UiControl() {
        super();
    }

    public void addScreen(String aNimi, Node aNode) {
        myScreens.put(aNimi, aNode);
    }

    public void addInterface(String aNimi, UiControlInterface aNode) {
        myInterfaces.put(aNimi, aNode);
    }

    public Node getScreen(String aNimi) {
        return myScreens.get(aNimi);
    }

    public UiControlInterface getInterface(String aNimi) {
        return myInterfaces.get(aNimi);
    }

    public boolean loadScreen(KirjastoFXWindow aWindow) {
        try {
            FXMLLoader myLoader = new FXMLLoader(getClass().getResource(aWindow.getFXML()));
            Parent loadScreen = (Parent) myLoader.load();
            UiControlInterface screenControler = ((UiControlInterface) myLoader.getController());
            screenControler.setParent(this);
            addScreen(aWindow.getNimi(), loadScreen);
            addInterface(aWindow.getNimi(), screenControler);
            return true;
        } catch (IOException ex) {
            Logger.getLogger(UiControl.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean setScreen(final String aNimi) {
        if (myScreens.get(aNimi) != null) {
            //Is there is more than one screen
            if (!getChildren().isEmpty()) {
                getChildren().remove(0);
                getChildren().add(0, myScreens.get(aNimi));
            } else {
                //no one else been displayed, then just show
                getChildren().add(myScreens.get(aNimi));
            }
            myInterfaces.get(aNimi).windowOpened();
            return true;
        } else {
            System.out.println("screen hasn't been loaded!\n");
            return false;
        }
    }

    public boolean unloadScreen(String name) {
        if (myScreens.remove(name) == null) {
            System.out.println("Screen didn't exist");
            return false;
        } else {
            return true;
        }
    }
}
