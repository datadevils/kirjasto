/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.fxui;

/**
 *
 * @author mika
 */
public interface UiControlInterface {

    public void setParent(UiControl screenPage);

    public void modifyButtonStates();

    public void windowOpened();
}
