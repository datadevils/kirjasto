package fi.datadevils.kirjasto.objektivarasto;


import fi.datadevils.kirjasto.data.Teos;
import fi.datadevils.kirjasto.data.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mika Pihlamo
 */
public class Varasto {

    SKuuLite myDatabase = null;

    public Varasto(SKuuLite aSql) {
        myDatabase = aSql;
    }

    public Varasto(String aDatabase) throws ClassNotFoundException, SQLException {
        this(new SKuuLite(aDatabase));
    }

    public ResultSet sendSql(String aSql) throws SQLException {
        return myDatabase.execSql(aSql);
    }

    public int sendSqlNRS(String aSql) throws SQLException {
        return myDatabase.execSqlNRS(aSql);
    }

    public String getOneString(String aSql) throws SQLException {
        return myDatabase.execSql(aSql).getString(Constants.FIRST_DATA_INDEX);
    }

    public float getOneFloat(String aSql) throws SQLException {
        return myDatabase.execSql(aSql).getFloat(Constants.FIRST_DATA_INDEX);
    }

    public int getOneInt(String aSql) throws SQLException {
        return myDatabase.execSql(aSql).getInt(Constants.FIRST_DATA_INDEX);
    }

    public int getNextId(String aTable) throws SQLException {
        String temp = "select max(id) from " + aTable + ";";
        return getOneInt(temp) + 1;
    }

    public Date getOneDate(String aSql) throws SQLException, ParseException {
        return Constants.DEFAULT_SQLITE_DATE_FORMATTER.parse(this.getOneString(aSql));
    }

    public ArrayList<String> getStringList(String aSql) throws SQLException {
        ArrayList<String> tempSL = new ArrayList<>();
        ResultSet rs = myDatabase.execSql(aSql);
        while (rs.next()) {
            tempSL.add(rs.getString(1));
        }
        return tempSL;
    }

    public void close() throws SQLException {
        myDatabase.close();
    }

    // erikoisesti Kirjasto sovelluksen käyttöön lähinnä testaukseen
    public ArrayList<String> getFull(String aSql) throws SQLException {
        ArrayList<String> tempSL = new ArrayList<>();
        ResultSet rs = myDatabase.execSql(aSql);
        while (rs.next()) {
            String tempS = "";
            for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                tempS += rs.getString(i);
                if (i < rs.getMetaData().getColumnCount()) {
                    tempS += Constants.DATA_SEPARATOR;
                }
            }
            tempSL.add(tempS);
        }
        return tempSL;
    }

    public User getUser(String email) throws SQLException {
        User user = null;
//        String aSql = "select * from user where email = '" + email + "'";
//        ResultSet rs = myDatabase.execSql(aSql);
//        if (rs.next()) {
//            user = new User(rs.getString("email"),
//                rs.getString("etunimi"),
//                rs.getString("sukunimi"),
//                rs.getString("salasana"),
//                rs.getDouble("sakkosaldo"),
//                rs.getBoolean("admin"));
//        }
        return user;
    }

    public void setTyylilaji(String[] genre) throws SQLException {
        String tempS = "insert into tyylilaji values(";
        tempS += getNextId("tyylilaji") + ", \"";
        tempS += genre[0] + "\", \"";
        tempS += genre[1] + "\");";
        System.err.println(tempS);
        sendSqlNRS(tempS);
    }

    public void setKustantaja(String[] publisher) throws SQLException {
        String tempS = "insert into kustantaja values(";
        tempS += getNextId("kustantaja") + ", \"";
        tempS += publisher[0] + "\", \"";
        tempS += publisher[1] + "\");";
        System.err.println(tempS);
        sendSqlNRS(tempS);
    }

    public void setLuokka(String[] luokka) throws SQLException {
        String tempS = "insert into luokka values(";
        tempS += getNextId("luokka") + ", \"";
        tempS += luokka[0] + "\", \"";
        tempS += luokka[1] + "\");";
        System.err.println(tempS);
        sendSqlNRS(tempS);
    }

    public void setUser(User aUser) throws SQLException, ParseException {
        String tempS = "insert into user values(";
        tempS += getNextId("user") + ", \"";
        tempS += aUser.getEmail() + "\", \"";
        tempS += aUser.getEtunimi() + "\", \"";
        tempS += aUser.getSukunimi() + "\", \"";
        tempS += aUser.getSalasana() + "\", ";
        tempS += String.valueOf(aUser.getSakkosaldo()) + ", ";
        tempS += aUser.getAdminInt() + ");";
        System.err.println(tempS);
        sendSqlNRS(tempS);
    }

    public boolean setTeos(Teos aTeos) {
        boolean tulos = false;
        String tempS = null;
        if (aTeos != null) {
            try {
//            keräilee tietoja talteen
                int id = getNextId("item");
                int tekijatId = getNextId("tekijat");
                int tyyliId = getTyyliLuokkaKustantajaTekijäId("tyylilaji", aTeos.getTyylilaji());
                int luokkaId = getTyyliLuokkaKustantajaTekijäId("luokka", aTeos.getLuokka());
                int kustantajaId = getTyyliLuokkaKustantajaTekijäId("kustantaja", aTeos.getKustantaja());
//            lisää tekijät tauluihin tekija ja tekijat
                for (String tekija : aTeos.getTekijät()) {
                    int tId = setTyyliLuokkaKustantajaTekijä("tekija", tekija);
                    tempS = "insert into tekijat values(" + getNextId("tekijat") + ", ";
                    tempS += id + ", " + tId + ");";
                    System.err.println(tempS);
                    sendSqlNRS(tempS);
                }
              
//              lisätään tyylkilaji tauluun
                if (tyyliId < 1) {
                    tyyliId = setTyyliLuokkaKustantajaTekijä("tyylilaji", aTeos.getTyylilaji());
                }
                
//              lisää luokan tauluun
                if (luokkaId < 1) {
                    luokkaId = setTyyliLuokkaKustantajaTekijä("luokka", aTeos.getLuokka());
                }
//            lisää kustantajan tauluu
                if (kustantajaId < 1) {
                    kustantajaId = setTyyliLuokkaKustantajaTekijä("kustantaja", aTeos.getKustantaja());
                }
//            lisää teoksen tauluun
                tempS = "insert into item values(";
                tempS += id + ", ";
                tempS += tekijatId + ", \"";
                tempS += aTeos.getNimi() + "\", ";
                tempS += tyyliId + ", ";
                tempS += luokkaId + ", ";
                tempS += aTeos.getJulkaisuVuosi() + ", ";
                tempS += kustantajaId + ");";
                System.err.println(tempS);
                sendSqlNRS(tempS);
//            lisää unique_item tauluun
                for (int i = 0; i < aTeos.getKopioidenLkm(); i++) {
                    tempS = "insert into unique_item (id, item_id) values(";
                    tempS += getNextId("unique_item") + ", " + id + ");";
                    System.err.println(tempS);
                    sendSqlNRS(tempS);
                }
                tulos = true;
            
            } catch (SQLException ex) {
                Logger.getLogger(Varasto.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return tulos;
    }
//     älä hämmenny käytössä on varargs,
//     (käyttö on taulunnimi, nimi/etunimi, selitys/kaupunki/sukunimi) 

    private int setTyyliLuokkaKustantajaTekijä(String... aDatat) throws SQLException {
        String tempS = "insert into " + aDatat[0] + " ";
        int id = getNextId(aDatat[0]);

        if (aDatat.length == 2) {
            tempS += "(id,nimi) ";
        }
        tempS += "values(" + id;
        for (int i = 1; i < aDatat.length; i++) {
            if (aDatat[i] != null) {
                tempS += ", \"" + aDatat[i] + "\"";
            }
        }
        tempS += ");";
        System.err.println(tempS);
        sendSqlNRS(tempS);
        return id;
    }

//     älä hämmenny käytössä on varargs,
//    (käyttö on taulunnimi, nimi/sukunimi, etunimi jos tarvitaan) 
    private int getTyyliLuokkaKustantajaTekijäId(String... aDatat) throws SQLException {
        String testi = "select id from " + aDatat[0] + " where ";
        int id = -1;
        if (aDatat.length == 3) {
            testi += "etunimi=\"" + aDatat[1] + "\" and sukunimi=\"" + aDatat[2] + "\";";
        } else {
            testi += "nimi=\"" + aDatat[1] + "\";";
        }
        try {
            id = getOneInt(testi);
        } catch (SQLException ex) {
            id = -1;
        }
        return id;
    }
}
