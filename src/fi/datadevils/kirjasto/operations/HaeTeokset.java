/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import fi.datadevils.kirjasto.varasto.Varasto;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Verkk_000
 */
public class HaeTeokset {

	private static Scanner myScanner = null;

	public HaeTeokset() {
		myScanner = new Scanner(System.in);
	}

	public static ArrayList<String> haeKaikki() {
		ArrayList<String> teokset = new ArrayList<>();
		try {
			teokset = SingletonVarasto
					.getInstance()
					.getFull(
							"select item.id, item.nimi, "
									+ "item.julkaisuvuosi, kustantaja.nimi, luokka.nimi, "
									+ "tyylilaji.nimi, tekija.nimi "
									+ "from item, kustantaja, luokka, tyylilaji "
									+ "inner join tekijat on item.id=tekijat.item_id "
									+ "inner join tekija on tekijat.tekija_id=tekija.id "
									+ "where "
									+ "(item.kustantaja_id=kustantaja.id "
									+ "and item.luokka_id=luokka.id "
									+ "and item.tyylilaji_id=tyylilaji.id)");
		} catch (SQLException ex) {
			Logger.getLogger(HaeTeokset.class.getName()).log(Level.SEVERE,
					null, ex);
		}

		teokset = tulostaTeokset("kirjat", teokset);

		return teokset;
	}

	public static ArrayList<String> haeKaikki(String tyyppi) {
		ArrayList<String> teokset = new ArrayList<>();
		/*
		 * String[] split = null; int id1 = 0; int id2 = 0;
		 */

		try {
			teokset = SingletonVarasto
					.getInstance()
					.getFull(
							"select item.id, item.nimi, "
									+ "item.julkaisuvuosi, kustantaja.nimi, luokka.nimi, "
									+ "tyylilaji.nimi, tekija.nimi "
									+ "from item, kustantaja, luokka, tyylilaji "
									+ "inner join tekijat on item.id=tekijat.item_id "
									+ "inner join tekija on tekijat.tekija_id=tekija.id "
									+ "where item.kustantaja_id=kustantaja.id "
									+ "and item.luokka_id=luokka.id "
									+ "and item.tyylilaji_id=tyylilaji.id");

		} catch (SQLException ex) {
			Logger.getLogger(HaeTeokset.class.getName()).log(Level.SEVERE,
					null, ex);
		}

		teokset = tulostaTeokset(tyyppi, teokset);

		return teokset;
	}

	public void haeYksi(String tyyppi) {
		ArrayList<String> yksiTeos = new ArrayList<>();

		System.out
				.print("\nKatso teoksen tarkat tiedot (syötä ID) - enter lopettaa): ");
		String itemID = myScanner.nextLine();

		if (itemID.matches("[0-9]+") == true) {
			try {
				yksiTeos = SingletonVarasto
						.getInstance()
						.getFull(
								"select item.id, item.nimi, "
										+ "item.julkaisuvuosi, kustantaja.nimi, luokka.nimi, "
										+ "tyylilaji.nimi, tekija.nimi "
										+ "from item, kustantaja, luokka, tyylilaji "
										+ "inner join tekijat on item.id=tekijat.item_id "
										+ "inner join tekija on tekijat.tekija_id=tekija.id "
										+ "where item.kustantaja_id=kustantaja.id "
										+ "and item.luokka_id=luokka.id "
										+ "and item.tyylilaji_id=tyylilaji.id "
										+ "and item.id=" + itemID);

			} catch (SQLException ex) {
				Logger.getLogger(HaeTeokset.class.getName()).log(Level.SEVERE,
						null, ex);
			}

			tulostaTeokset(tyyppi, yksiTeos);
			lisätiedotHaeJaTulosta(itemID);
		}

	}

	private static ArrayList<String> tulostaTeokset(String tyyppi,
			ArrayList<String> teokset) {
		ArrayList<String> palautus = new ArrayList<>();
		String[] split = null;
		int id1 = 0;
		int id2 = -1;

		String rivi = "";
		String lisäys = "";

		for (String temp : teokset) {

			split = temp.split(";");
			id1 = Integer.parseInt(split[0]);

			if (id1 != id2) {

				if ((rivi != null) && (!rivi.isEmpty())) {
					palautus.add(rivi);
				}

				if (split[6].equalsIgnoreCase("null")) {
					split[6] = "";
				}

				rivi = "\nID: " + split[0] + " | "
						+ split[1] // nimi
						+ ", "
						+ split[2] // vuosiluku
						+ " , "
						+ split[3] // kustantaja
						+ " | Luokka: " + split[4] + " | Tyylilaji: "
						+ split[5] + " | Tekijat: " + split[6];

				System.out.print(rivi);
			} else {
				lisäys = ", " + split[6];
				System.out.print(lisäys);
				rivi = rivi + lisäys;
			}
			id2 = id1;
		}
		;

		// Lisäsi ennen tyhjän rivin taulukkooon
		if ((rivi != null) && (!rivi.isEmpty())) {
			palautus.add(rivi);
		}

		return palautus;
	}

	public static ArrayList<String> lisätiedotHaeJaTulosta(String itemID) {
		String[] split = null;
		ArrayList<String> lainaajat = new ArrayList<>();
		ArrayList<String> varaajat = new ArrayList<>();
		ArrayList<String> tiedot = new ArrayList<>();
		ArrayList<String> palautus = new ArrayList<>();
		String rivi = "";
		int kpl = 0;
		int kplLainassa = 0;
		int kplVarattu = 0;
		int i = 0;
		int id1 = 0;
		int id2 = 0;

		try {
			tiedot = SingletonVarasto.getInstance().getFull(
					"select unique_item.id, "
							+ "unique_item.item_id, unique_item.palautus, "
							+ "unique_item.user_id "
							+ "from unique_item, item "
							+ "where item.id=unique_item.item_id "
							+ "and item.id=" + itemID);
		} catch (SQLException ex) {
			Logger.getLogger(HaeTeokset.class.getName()).log(Level.SEVERE,
					null, ex);
		}

		for (String temp : tiedot) {
			split = temp.split(";");
			id1 = Integer.parseInt(split[0]);

			if (id1 != id2) {
				kpl++;
				if (split[3].equalsIgnoreCase("null")) {/* älä tee mitään */

				} else {
					kplLainassa++;
					try {
						lainaajat.add(SingletonVarasto.getInstance()
								.getOneString(
										"select user.etunimi from user where user.id ="
												+ split[3])
								+ " "
								+ SingletonVarasto.getInstance().getOneString(
										"select user.sukunimi from user where user.id = "
												+ split[3])
								+ " "
								+ "("
								+ split[2] + ")");
					} catch (SQLException ex) {
						Logger.getLogger(HaeTeokset.class.getName()).log(
								Level.SEVERE, null, ex);
					}
				}
			}
			id2 = id1;
		}

		rivi = "Kopioita: " + kpl + " kpl";
		System.out.println(rivi);
		palautus.add(rivi);

		rivi = "Lainassa: " + kplLainassa + " kpl";
		System.out.println(rivi);
		palautus.add(rivi);

		if (SingletonVarasto.getInstance().getMyUserIsAdmin().getValue() == true) {
			rivi = "Lainaajat: ";
			// palautus.add(rivi);

			if (kplLainassa > 0) {
				// rivi = "";
				for (String temp : lainaajat) {
					split = temp.split(",");
					rivi = rivi + split[0];
					if (kplLainassa > 1 && i < kplLainassa - 1) {
						System.out.print(", ");
						rivi = rivi + ", ";
						i++;
					}
				}
				System.out.println(rivi);
				palautus.add(rivi);
			}
		}

		try {
			tiedot = SingletonVarasto
					.getInstance()
					.getFull(
							"select varaukset.user_id, varaukset.vuoronumero from varaukset where varaukset.item_id ="
									+ itemID);
		} catch (SQLException ex) {
			Logger.getLogger(HaeTeokset.class.getName()).log(Level.SEVERE,
					null, ex);
		}

		for (String temp : tiedot) {
			split = temp.split(";");
			id1 = Integer.parseInt(split[0]);

			if (id1 != id2) {
				kplVarattu++;
				try {
					varaajat.add(SingletonVarasto.getInstance().getOneString(
							"select user.etunimi from user where user.id ="
									+ split[0])
							+ " "
							+ SingletonVarasto.getInstance().getOneString(
									"select user.sukunimi from user where user.id = "
											+ split[0])
							+ " "
							+ "("
							+ split[1]
							+ ")");
				} catch (SQLException ex) {
					Logger.getLogger(HaeTeokset.class.getName()).log(
							Level.SEVERE, null, ex);
				}

			}
			id2 = id1;
		}

		rivi = "Varattu: " + kplVarattu + " kpl";
		System.out.println(rivi);
		palautus.add(rivi);

		if (SingletonVarasto.getInstance().getMyUserIsAdmin().getValue() == true) {
			rivi = "Varaajat: ";
//			System.out.print(rivi);
//			palautus.add(rivi);

			if (kplVarattu > 0) {
//				rivi = "";
				for (String temp : varaajat) {

					split = temp.split(",");
					System.out.print(split[0]);
					rivi = rivi + split[0];
					if (kplVarattu > 1 && i < kplVarattu - 1) {
						System.out.print(", ");
						rivi = rivi + ", ";
						i++;
					}
				}
				palautus.add(rivi);
			}
		}

		return (palautus);
	}
}
