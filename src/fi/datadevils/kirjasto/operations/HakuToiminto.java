/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.fxui.AutoComplete;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.ObservableList;

/**
 *
 * @author Samuli
 */
public class HakuToiminto implements Runnable {

    Thread renki = null;
    AutoComplete myParent = null;
    String haku = null;
    ObservableList<String> tulokset = null;
    ArrayList<String> results = null;

    public HakuToiminto(AutoComplete myParent) {
        this.myParent = myParent;
        results = new ArrayList<String>();
    }

    @Override
    public void run() {

        try {
            Thread.sleep(400);
            this.haku = this.myParent.comboBox.getEditor().getText();
            results.clear();
            if ((haku != null)
                    && (haku.isEmpty() == false)
                    && (haku.equals(" ") == false)) {
                
                results.addAll(addTypes(SingletonVarasto.getInstance().getFull("select item.nimi "
                        + "from item "
                        + "where (item.nimi like \"%" + haku + "%\")"),"(teos)")); 
                results.addAll(addTypes(SingletonVarasto.getInstance().getFull("select tekija.nimi "
                        + "from tekija "
                        + "where(tekija.nimi like \"%" + haku + "%\")"),"(tekijä)"));
                results.addAll(addTypes(SingletonVarasto.getInstance().getFull("select kustantaja.nimi "
                        + "from kustantaja "
                        + "where(kustantaja.nimi like \"%" + haku + "%\")"),"(kustantaja)"));
                results.addAll(addTypes(SingletonVarasto.getInstance().getFull("select luokka.nimi "
                        + "from luokka "
                        + "where(luokka.nimi like \"%" + haku + "%\")"),"(luokka)"));
                results.addAll(addTypes(SingletonVarasto.getInstance().getFull("select tyylilaji.nimi "
                        + "from tyylilaji "
                        + "where(tyylilaji.nimi like \"%" + haku + "%\")"),"(tyylilaji)"));        
                results.addAll(addTypes(SingletonVarasto.getInstance().getFull("select item.julkaisuvuosi "
                        + "from item "
                        + "where(item.julkaisuvuosi like \"%" + haku + "%\")"),"(julkaisuvuosi)"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(HakuToiminto.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(HakuToiminto.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tulokset.clear();
                        if(results.isEmpty() == false)
                            tulokset.addAll(results);
                    }
                });
        }
    }

    public void start(String haku, ObservableList<String> tulokset) {
        this.tulokset = tulokset;
        this.haku = haku;
        if ((renki == null) || (renki.isAlive() == false)) {
            renki = new Thread(this);
            renki.start();
        }
    }

    private ArrayList<String> addTypes(ArrayList<String> full, String type) {
        ArrayList<String> uusi = new ArrayList<String>();
        for(String g: full) {
            g = g + " "  + type;
            uusi.add(g);
        }
        return uusi;
    }

    public String parseSearchString(String originalText) {
        if (originalText == null || originalText.equals("")) {
            return null;
        }
        String[] jono = originalText.split(" ");
        String tyyppi = jono[jono.length - 1];
        String sqlLause;
        String text;
        sqlLause = "select item.id, item.nimi, item.julkaisuvuosi, tekija.nimi, kustantaja.nimi, tyyppi.nimi " 
                 + "from item, kustantaja, luokka, tyylilaji, tyyppi " 
                 + "inner join tekijat on item.id=tekijat.item_id " 
                 + "inner join tekija on tekijat.tekija_id=tekija.id " 
                 + "where item.kustantaja_id=kustantaja.id and " 
                 + " item.tyylilaji_id=tyylilaji.id and " 
                 + " item.luokka_id=luokka.id and "
                 + " item.tyyppi_id=tyyppi.id and ";
        text = originalText.replace(jono[jono.length - 1], "").trim().toLowerCase();
        originalText = originalText.trim().toLowerCase();
        System.out.println(jono[jono.length - 1]);
        System.out.println(text);
        if (tyyppi.equals("(tekij\u00e4)")) {
            sqlLause = sqlLause + "(tekija.nimi like \"" + text + "\")";
        } else if (tyyppi.equals("(teos)")) {
            sqlLause = sqlLause + "(item.nimi like \"" + text + "\")";
        } else if (tyyppi.equals("(julkaisuvuosi)")) {
            sqlLause = sqlLause + "(item.julkaisuvuosi like " + text + ")";
        } else if (tyyppi.equals("(luokka)")) {
            sqlLause = sqlLause + "(luokka.nimi like \"" + text + "\")";
        } else if (tyyppi.equals("(tyylilaji)")) {
            sqlLause = sqlLause + "(tyylilaji.nimi like \"" + text + "\")";
        } else if (tyyppi.equals("(kustantaja)")) {
            sqlLause = sqlLause + "(kustantaja.nimi like \"" + text + "\")";
        } else {
            sqlLause = sqlLause + "(item.nimi like \"%" + originalText + "%\")";
        }
        System.out.println(sqlLause);
        return sqlLause;
    }
}
