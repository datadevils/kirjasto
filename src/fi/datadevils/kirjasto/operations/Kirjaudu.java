/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.data.User;
import fi.datadevils.kirjasto.ui.KirjastoUiWindow;
import fi.datadevils.kirjasto.ui.MainWindow;
import fi.datadevils.kirjasto.ui.RegistrationWindow;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import java.io.Console;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuli
 */
public class Kirjaudu {

    public static boolean login(String email, String password) {

        Boolean result = false;
        int attempts = 0;

        do {
            
            result = verify(email, password);
            if (result == false) {
                System.out.println("\nväärä sähköposti tai salasana!\n");
            }
            attempts++;
        } while (result == false && attempts < 3);

        return result;
    }

    private static boolean verify(String email, String password) {
        try {
            User user = SingletonVarasto.getInstance().getUser(email);
            SingletonVarasto.setMyUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(RegistrationWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (SingletonVarasto.getInstance().getMyUser() != null && SingletonVarasto.getInstance().getMyUser().getSalasana() != null) {
            Boolean tosi = SingletonVarasto.getInstance().getMyUser().getSalasana().equals(password);
            return tosi;
        } else {
            return false;
        }
    }
}
