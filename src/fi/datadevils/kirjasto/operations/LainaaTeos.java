/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.data.User;
import fi.datadevils.kirjasto.Constants;
import static fi.datadevils.kirjasto.operations.LainaaTeos.addDays;
import static fi.datadevils.kirjasto.varasto.Constants.DEFAULT_OUT_DATE_FORMATTER;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuli
 */
public class LainaaTeos {

    // Hakee käyttäjän lainat pois lukien sen jonka id on id__unique_item.
    public static ArrayList<String> haeKäyttäjänLainat(User user,
            int id__unique_item, int item_id__unique_item) {
        ArrayList<String> lainat = new ArrayList<>();

        try {
            lainat = SingletonVarasto.getInstance().getFull(
                    "SELECT * FROM unique_item WHERE user_id = '" + user.getId()
                    + "' AND id != '" + id__unique_item
                    + "' AND item_id = '" + item_id__unique_item + "'");
        } catch (SQLException ex) {
            Logger.getLogger(LainaaTeos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lainat;
    }
 
        // Hakee käyttäjän lainat pois lukien sen jonka id on id__unique_item.
    public static ArrayList<String> haeKäyttäjänKaikkiLainat(User user) {
        ArrayList<String> lainat = new ArrayList<>();

        try {
            lainat = SingletonVarasto.getInstance().getFull(
                    "SELECT * FROM unique_item WHERE user_id = " + user.getId() + ";");
        } catch (SQLException ex) {
            Logger.getLogger(LainaaTeos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lainat;
    }
    
    public static ArrayList<Integer> valitseLainat(Scanner myScanner) {

        System.out.print("\nKirjoita lainattavien teosten numerot (erota pilkulla(,) - enter lopettaa): ");
        String rimpsu = myScanner.nextLine();
        if (rimpsu.equals("")) {
            return null;
        }

        String[] vara = rimpsu.split(",");

        ArrayList<Integer> lainat = new ArrayList();
        for (String x : vara) {
            lainat.add(Integer.parseInt(x));
        }

        return lainat;
    }

    public static Date addDays() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, Constants.LAINAUSAIKA);
        return cal.getTime();
    }
    

    public static boolean lainaaTeos(ArrayList<Integer> haeLainat, User user) {

        Date palautus = addDays();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        if (haeLainat != null) {
            for (Integer i : haeLainat) {

                String tempS = "select id, user_id from unique_item where item_id=" + i + ";";
                ArrayList<String> uniqueIdt = null;
                try {
                    uniqueIdt = SingletonVarasto.getInstance().getFull(tempS);
                } catch (SQLException ex) {
                    Logger.getLogger(PalautaTeos.class.getName()).log(Level.SEVERE, null, ex);
                }
                int id = 0;
                boolean foundFree = false;
                if (!uniqueIdt.isEmpty()) {
                    for (String testi : uniqueIdt) {
                        String[] vara = testi.split(";");
                        if (vara[1].equals("null")) {
                            id = Integer.parseInt(vara[0]);
                            foundFree = true;
                            break;
                        }
                    }
                }
                if (foundFree) {
                    tempS = "update unique_item set user_id=" + user.getId() + ", palautus=\"" + DEFAULT_OUT_DATE_FORMATTER.format(palautus) + "\" where id = " + id + ";";
                    System.err.println(tempS);

                    try {
                        SingletonVarasto.getInstance().sendSqlNRS(tempS);
                    } catch (SQLException ex) {
                        Logger.getLogger(PalautaTeos.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    // TODO
                }
            }
            return true;
        }
        return false;
    }
    
    public static boolean lainaaKappale(int id__unique_item, User user) {

        Date palautus = addDays();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        
        String tempS = null;
        tempS = "update unique_item set user_id=" + user.getId() + ", palautus=\"" + DEFAULT_OUT_DATE_FORMATTER.format(palautus) + "\" where id = " + id__unique_item + ";";
        System.err.println(tempS);

        try {
            SingletonVarasto.getInstance().sendSqlNRS(tempS);
        } catch (SQLException ex) {
            Logger.getLogger(PalautaTeos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
        
}