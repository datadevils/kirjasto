/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.Kirjasto;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import fi.datadevils.kirjasto.varasto.Varasto;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuli
 */
public class LisääKustantaja {

    public static ArrayList<String> populatePublishers() {
        try {
            ArrayList<String> lista = SingletonVarasto.getInstance().getFull("select nimi from kustantaja");
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String choosePublisher(Varasto myVarasto, Scanner myScanner) {

        String selected = null;

        try {
            ArrayList<String> lista = myVarasto.getFull("select nimi from kustantaja");

            int i = 1;
            for (String vara : lista) {
                System.out.println("  " + i + ". " + vara);
                i++;
            }
            System.out.print("  " + i + ". Lisää uusi kustantaja\n\n: ");
            Byte x = myScanner.nextByte();

            if (x == i) {
                selected = LisääKustantaja.addPublisher(myVarasto, myScanner);
            } else if (x > i) {
                return null;
            } else {
                if (myScanner.hasNextLine()) {
                    myScanner.nextLine();
                }
                selected = lista.get(x - 1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return selected;
    }

    public static boolean checkAndAddPublisher(String publisher) {
        try {
            ArrayList<String> lista = SingletonVarasto.getInstance().getFull("select nimi from kustantaja");
            boolean found = false;
            for(String nimi: lista){
                if(nimi.equals(publisher)){
                    found=true;
                    break;
                }
            }
            if(!found){
                String [] julkaisija = new String[2];
                julkaisija[0] = publisher;
                julkaisija[1] = "";        
                SingletonVarasto.getInstance().setKustantaja(julkaisija);
                return true;
            }   
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private static String addPublisher(Varasto myVarasto, Scanner myScanner) {

        String created = null;

        if (myScanner.hasNextLine()) {
            myScanner.nextLine();
        }

        System.out.print("\n  Anna kustantaja : ");
        String[] publisher = new String[2];
        publisher[0] = myScanner.nextLine();
        System.out.print("  Kuvaus : ");
        publisher[1] = myScanner.nextLine();

        try {
            myVarasto.setKustantaja(publisher);

            return publisher[0];
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);

            return null;

        }
    }

}
