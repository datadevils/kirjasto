/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.data.User;
import fi.datadevils.kirjasto.Kirjasto;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import fi.datadevils.kirjasto.varasto.Varasto;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Verneri
 */
public class LisääKäyttäjä {

    private static Scanner myScanner = null;

    public LisääKäyttäjä(Varasto aVarasto) {
        myScanner = new Scanner(System.in);
    }

    public static boolean luo(String etunimi, String sukunimi, String email, String salasana) {

        boolean result = false;

        User uusiKäyttäjä = new User(-1, "", "", "", "", 0.0, false);

        if (etunimi == null) {
            System.out.println("    Etunimi: ");
            uusiKäyttäjä.setEtunimi(myScanner.nextLine());
        } else {
            uusiKäyttäjä.setEtunimi(etunimi);
        }

        if (sukunimi == null) {
            System.out.println("    Sukunimi: ");
            uusiKäyttäjä.setSukunimi(myScanner.nextLine());
        } else {
            uusiKäyttäjä.setSukunimi(sukunimi);
        }

        if (email == null) {
            System.out.println("    Sähköposti: ");
            uusiKäyttäjä.setEmail(myScanner.nextLine());
        } else {
            uusiKäyttäjä.setEmail(email);
        }

        if (salasana == null) {
            System.out.println("    Salasana: ");
            uusiKäyttäjä.setSalasana(myScanner.nextLine());
        } else {
            uusiKäyttäjä.setSalasana(salasana);
        }

        // Sähköpostin tarkistus tänne vielä!!!!!!!!
        try {
            SingletonVarasto.getInstance().setUser(uusiKäyttäjä);
        } catch (ParseException | SQLException ex) {
            Logger.getLogger(LisääKäyttäjä.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
