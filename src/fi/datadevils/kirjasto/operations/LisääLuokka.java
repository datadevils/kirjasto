/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.Kirjasto;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import fi.datadevils.kirjasto.varasto.Varasto;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuli
 */
public class LisääLuokka {

    public static ArrayList<String> populateLuokat() {
        try {
            ArrayList<String> lista = SingletonVarasto.getInstance().getFull("select nimi from luokka");
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static boolean checkAndAddLuokka(String luokka) {
        try {
            ArrayList<String> lista = SingletonVarasto.getInstance().getFull("select nimi from luokka");
            boolean found = false;
            for(String nimi: lista){
                if(nimi.equals(luokka)){
                    found=true;
                    break;
                }
            }
            if(!found){
                String [] luokat = new String[2];
                luokat[0] = luokka;
                luokat[1] = "default sepustus";        
                SingletonVarasto.getInstance().setLuokka(luokat);
                return true;
            }   
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }  
    
    public static String chooseClass(Varasto myVarasto, Scanner myScanner) {

        String selected = null;

        try {
            ArrayList<String> lista = myVarasto.getFull("select nimi from luokka");

            int i = 1;
            for (String vara : lista) {
                System.out.println("  " + i + ". " + vara);
                i++;
            }
            System.out.print("  " + i + ". Lisää uusi luokka\n\n: ");
            Byte x = myScanner.nextByte();

            if (x == i) {
                selected = LisääLuokka.addClass(myVarasto, myScanner);
            } else if (x > i) {
                return null;
            } else {
                if (myScanner.hasNextLine()) {
                    myScanner.nextLine();
                }
                selected = lista.get(x - 1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return selected;
    }

    private static String addClass(Varasto myVarasto, Scanner myScanner) {

        String created = null;

        if (myScanner.hasNextLine()) {
            myScanner.nextLine();
        }

        System.out.print("\n  Anna luokka : ");
        String[] luokka = new String[2];
        luokka[0] = myScanner.nextLine();
        System.out.print("  Kuvaus : ");
        luokka[1] = myScanner.nextLine();

        try {
            myVarasto.setLuokka(luokka);

            return luokka[0];
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);

            return null;

        }
    }
}
