/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.data.Teos;
import fi.datadevils.kirjasto.Kirjasto;
import fi.datadevils.kirjasto.varasto.Varasto;
import static java.lang.Byte.parseByte;
import static java.lang.Integer.parseInt;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jarkko
 */
public class LisääTeos {

    private static final String ITEM_TYPE_PROMPT = "Valitse teoksen tyyppi!\n  1. Kirja\n  2. CD\n  3. Lehti\n\n: ";
    private static final String CHOOSE_CLASS_PROMPT = "Valitse luokka!\n";
    private static final String CHOOSE_PUBLISHER_PROMPT = "Valitse kustantaja!\n";
    private static final String CHOOSE_GENRE_PROMPT = "Valitse genre!\n";
    private static final byte ITEM_TYPE_BOOK = 1;
    private static final byte ITEM_TYPE_CD = 2;
    private static final byte ITEM_TYPE_MAGAZINE = 3;
    
    private static final byte INDEX_NAME  = 0;
    private static final byte INDEX_COUNT = 1;
    private static final byte INDEX_GENRE = 2;
    private static final byte INDEX_YEAR  = 3;
    private static final byte INDEX_PUBLISHER = 4;
    private static final byte INDEX_CLASS = 5;
    private static final byte INDEX_AUTHORS = 6;
    private static final byte INDEX_ITEM_TYPE = 7;
    private static final byte MAX_INDEX = 8;
    
    private static Varasto myVarasto = null;
    private static Scanner myScanner = null;
    private static Teos myTeos = null;

    public LisääTeos(Varasto aVarasto) {
        myVarasto = aVarasto;
        myScanner = new Scanner(System.in);
    }

    public static void lisääTeos(String aMark) {
    
        String row_mark = aMark;
        ArrayList<String> tulosteet = new ArrayList(MAX_INDEX);
        ArrayList<String> tiedot = new ArrayList(MAX_INDEX);
        ArrayList<String> tekijät = new ArrayList(5);
        String syöte = " ";
        
        System.out.print(row_mark);
        
        System.out.print("\n\nLisää teos\n----------\n\n");
        
        tulosteet.add("Anna teoksen nimi : ");
        tulosteet.add("Kuinka monta kopioita : ");
        tulosteet.add(CHOOSE_GENRE_PROMPT);
        tulosteet.add("Julkaisuvuosi : ");
        tulosteet.add(CHOOSE_PUBLISHER_PROMPT);
        tulosteet.add(CHOOSE_CLASS_PROMPT);
        tulosteet.add("Tekijät (erottele nimet pilkulla(,)) : ");
        tulosteet.add(ITEM_TYPE_PROMPT);
                
        int i = 0;
        byte item_type = 0;
        while (!(syöte.matches("loppu")) && (i < tulosteet.size())) {

            String prompt = tulosteet.get(i);
            System.out.print(prompt);
            if (prompt.equals(CHOOSE_GENRE_PROMPT)) {
                syöte = LisääTyylilaji.chooseGenre(myVarasto, myScanner);
            } else if (prompt.equals(CHOOSE_PUBLISHER_PROMPT)) {
                syöte = LisääKustantaja.choosePublisher(myVarasto, myScanner);
            } else if (prompt.equals(CHOOSE_CLASS_PROMPT)) {
                syöte = LisääLuokka.chooseClass(myVarasto, myScanner);
            } else if (prompt.equals(ITEM_TYPE_PROMPT)) {
                syöte = "";
                item_type = myScanner.nextByte();
                myScanner.nextLine();
            } else {
                syöte = myScanner.nextLine();
            }

            tiedot.add(syöte);
            i++;
        }
        
        String[] tempTable = null;
        tempTable = tiedot.get(INDEX_AUTHORS).split(",");
        for (i = 0; i < tempTable.length; i++) {
            tekijät.add(tempTable[i].trim());
        }
        
        switch(item_type){
            case ITEM_TYPE_BOOK:
                myTeos = new Teos();
                break;                
        }
        
        myTeos.setNimi(tiedot.get(INDEX_NAME));
        myTeos.setKopioidenLkm(parseByte(tiedot.get(INDEX_COUNT)));
        myTeos.setTyylilaji(tiedot.get(INDEX_GENRE));
        myTeos.setJulkaisuVuosi(parseInt(tiedot.get(INDEX_YEAR)));
        myTeos.setKustantaja(tiedot.get(INDEX_PUBLISHER));
        myTeos.setLuokka(tiedot.get(INDEX_CLASS));
        myTeos.setTekijät(tekijät);

        myVarasto.setTeos(myTeos);
    }
}
