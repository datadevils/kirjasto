/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.Kirjasto;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import fi.datadevils.kirjasto.varasto.Varasto;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuli
 */
public class LisääTyylilaji {
    
    public static ArrayList<String> populateGenres() {
        try {
            ArrayList<String> lista = SingletonVarasto.getInstance().getFull("select nimi from tyylilaji");
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static boolean checkAndAddGenre(String genre) {
        try {
            ArrayList<String> lista = SingletonVarasto.getInstance().getFull("select nimi from tyylilaji");
            boolean found = false;
            for(String nimi: lista){
                if(nimi.equals(genre)){
                    found=true;
                    break;
                }
            }
            if(!found){
                String [] genret = new String[2];
                genret[0] = genre;
                genret[1] = "default sepustus";        
                SingletonVarasto.getInstance().setTyylilaji(genret);
                return true;
            }   
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }    
    
    
    public static String chooseGenre(Varasto myVarasto, Scanner myScanner) {

        String selected = null;

        try {
            ArrayList<String> lista = myVarasto.getFull("select nimi from tyylilaji");

            int i = 1;
            for (String vara : lista) {
                System.out.println("  " + i + ". " + vara);
                i++;
            }
            System.out.print("  " + i + ". Lisää uusi genre\n\n: ");
            Byte x = myScanner.nextByte();
            
            if (x == i) {
                selected = LisääTyylilaji.addGenre(myVarasto, myScanner);
            } else if (x > i) {
                return null;
            } else {
                if(myScanner.hasNextLine())
                    myScanner.nextLine();
                selected = lista.get(x-1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return selected;
    }

    private static String addGenre(Varasto myVarasto, Scanner myScanner) {
        
        String created = null;

        if (myScanner.hasNextLine()) {
            myScanner.nextLine();
        }
        
        System.out.print("\n  Anna genre : ");
        String [] genre = new String[2];
        genre[0] = myScanner.nextLine();
        System.out.print("  Kuvaus : ");
        genre[1] = myScanner.nextLine();

        try {
            myVarasto.setTyylilaji(genre);
            
            return genre[0];
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        
            return null;
        
        }
    }
}
