/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.Kirjasto;
import fi.datadevils.kirjasto.varasto.Varasto;
import fi.datadevils.kirjasto.data.User;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jarkko
 */
public class MuokkaaTietoja {

    private Varasto myVarasto = null;
    private Scanner myScanner = null;

    public MuokkaaTietoja(Varasto aVarasto) {

        myVarasto = aVarasto;
        myScanner = new Scanner(System.in);
    }

    public void muokkaaTietoja(User user) {
        
        // Otetaan kirjautuneen käyttäjän tiedot talteen
        String syöte = " ";
        String old_email = user.getEmail();
        String old_etunimi = user.getEtunimi();
        String old_sukunimi = user.getSukunimi();
        String old_salasana = user.getSalasana();
        int old_user_id = user.getId();

        tulostaTiedot(user);
        
        // Kysytään uusia tietoja kunnes tulee loppu-komento
        while (!syöte.equalsIgnoreCase("loppu")) {

            System.out.println("Mitä haluat muokata?");
            syöte = myScanner.next().toLowerCase();
            switch (syöte) {
                case "email":
                    System.out.println("Syötä uusi Email: ");
                    syöte = myScanner.next();
                    user.setEmail(syöte);
                    tulostaTiedot(user);
                    break;
                case "etunimi":
                    System.out.println("Syötä uusi Etunimi: ");
                    syöte = myScanner.next();
                    user.setEtunimi(syöte);
                    tulostaTiedot(user);
                    break;
                case "sukunimi":
                    System.out.println("Syötä uusi Sukunimi: ");
                    syöte = myScanner.next();
                    user.setSukunimi(syöte);
                    tulostaTiedot(user);
                    break;
                case "salasana":
                    System.out.println("Syötä uusi Salasana:  ");
                    syöte = myScanner.next();
                    user.setSalasana(syöte);
                    tulostaTiedot(user);
                    break;
                case "loppu":
                    System.out.println("poistutaan..");
                    break;
                default:
                    System.out.println("Vaihtoehdot(Email, Sukunimi, Etunimi, Salasana, Loppu): ");
                    break;
            }

            try {
                // kirjoitetaan tiedot heti kantaan
                myVarasto.sendSqlNRS("UPDATE user set email = '" + user.getEmail() + "' where id = '" + old_user_id + "'");
                myVarasto.sendSqlNRS("UPDATE user set etunimi = '" + user.getEtunimi() + "' where id = '" + old_user_id + "'");
                myVarasto.sendSqlNRS("UPDATE user set sukunimi = '" + user.getSukunimi() + "' where id = '" + old_user_id + "'");
                myVarasto.sendSqlNRS("UPDATE user set salasana = '" + user.getSalasana() + "' where id = '" + old_user_id + "'");

            } catch (SQLException ex) {
                Logger.getLogger(Kirjasto.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    // tulostetaan käyttäjän tiedot
    private void tulostaTiedot(User user) {
        System.out.println("Tiedot: ");
        System.out.println("Email: " + user.getEmail());
        System.out.println("Etunimi: " + user.getEtunimi());
        System.out.println("Sukunimi: " + user.getSukunimi());
        System.out.println("Salasana: " + user.getSalasana());
        System.out.println("Sakkosaldo: " + user.getSakkosaldo());
    }
    
    public void muokkaaTietojaFX(User käyttäjä) {
        try {
            myVarasto.sendSqlNRS("UPDATE user set etunimi = '" + käyttäjä.getEtunimi() + "' where id = '" + käyttäjä.getId() + "'");
            myVarasto.sendSqlNRS("UPDATE user set sukunimi = '" + käyttäjä.getSukunimi() + "' where id = '" + käyttäjä.getId() + "'");
            myVarasto.sendSqlNRS("UPDATE user set salasana = '" + käyttäjä.getSalasana() + "' where id = '" + käyttäjä.getId() + "'");
        }
        catch (SQLException ex) {
                Logger.getLogger(MuokkaaTietoja.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
    }


}
