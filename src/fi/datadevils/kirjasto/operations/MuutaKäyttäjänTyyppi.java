/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.Kirjasto;
import fi.datadevils.kirjasto.varasto.Varasto;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jarkko
 */
public class MuutaKäyttäjänTyyppi {
    
    
    private Varasto myVarasto = null;
    private Scanner myScanner = null;
    private static final int ADMIN_TRUE = 1;
    private static final int ADMIN_FALSE = 0;

    public MuutaKäyttäjänTyyppi(Varasto aVarasto) {

        myVarasto = aVarasto;
        myScanner = new Scanner(System.in);
    }
    
    public void muutaKäyttäjänTyyppi() {
        String syöte = " ";
        ArrayList<String> vastaus = null;
        int admin = 0;
        int id = 0;
        
        while (!syöte.equalsIgnoreCase("loppu")) {
            
            // Tulostetaan ensin kaikki käyttäjät
            System.out.println("Käyttäjät: ");
            try {

                vastaus = myVarasto.getFull("select * from user");

            
            for (int i = 0; i < vastaus.size(); i++) {
                System.out.println(vastaus.get(i));
            }

            // Kysytään id, jotta voidaan muuttaa admin-oikeus
            System.out.println("Anna käyttäjän id tai loppu");
            syöte = myScanner.next();
                if (!syöte.equalsIgnoreCase("loppu")) {
                    id = parseInt(syöte);

                    admin = myVarasto.getOneInt("select admin from user where id = '" + id + "'");

                    // Päivitetään admin-oikeus, jos sitä ei aikaisemmin ollut
                    if (admin == ADMIN_FALSE) {
                        myVarasto.sendSqlNRS("UPDATE user set admin = '" + ADMIN_TRUE + "' where id = '" + id + "'");
                    }
                }
                else{
                    break;
                }

        }catch (SQLException ex) {
                Logger.getLogger(Kirjasto.class
                        .getName()).log(Level.SEVERE, null, ex);

        }
    }
}
    
}
