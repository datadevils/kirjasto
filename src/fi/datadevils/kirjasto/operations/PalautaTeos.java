/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.data.User;
import fi.datadevils.kirjasto.Kirjasto;
import fi.datadevils.kirjasto.varasto.Varasto;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuli
 */
public class PalautaTeos {

    private static ArrayList<Integer> haeLainat(User user, Varasto myVarasto, Scanner myScanner) {
        ArrayList<String> lainat = null;
        try {
            lainat = myVarasto.getFull("select item.nimi, unique_item.id, unique_item.palautus"
                    + " from item join unique_item on item.id = unique_item.item_id"
                    + " join user on unique_item.user_id = user.id where user.email = \""
                    + user.getEmail()
                    + "\";");
            int x = 1;
            System.out.print("\n\nLainat\n------\n");
            for (String laina : lainat) {
                String[] vara = laina.split(";");
                System.out.println(vara[1] + ": " + vara[0]);
                x++;
            }

        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        System.out.print("\nKirjoita palautettavien teosten numerot (erota pilkulla(,) - enter lopettaa): ");
        String rimpsu = myScanner.nextLine();
        if (rimpsu.equals("")) {
            return null;
        }
        
        String[] vara = rimpsu.split(",");

        ArrayList<Integer> palautukset = new ArrayList();
        for (String x : vara) {
            palautukset.add(Integer.parseInt(x));
        }

        return palautukset;
    }

    public static void show(String aMark, User user, Varasto varasto, Scanner scanner) {
        String row_mark = aMark;
        while(palautaLainat(haeLainat(user, varasto, scanner), varasto));
    }

    private static boolean palautaLainat(ArrayList<Integer> haeLainat, Varasto myVarasto) {
        if (haeLainat != null) {
            for (Integer i : haeLainat) {
                try {
                    myVarasto.sendSqlNRS("update unique_item set user_id=? where id = " + i + ";");
                } catch (SQLException ex) {
                    Logger.getLogger(PalautaTeos.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return true;
        }
        return false;
    }

}
