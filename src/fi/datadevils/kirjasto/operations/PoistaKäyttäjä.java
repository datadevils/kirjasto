/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.Kirjasto;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import fi.datadevils.kirjasto.varasto.Varasto;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jarkko
 */
public class PoistaKäyttäjä {

    private static Varasto myVarasto = null;

    public PoistaKäyttäjä() {
        myVarasto = SingletonVarasto.getInstance();
    }
    
    public static void poistaKäyttäjäFx(int id) {
         System.out.println("Käyttäjän poistoa kutsuttu!");
        try {

            ArrayList<String> vastaus = new ArrayList<String>();

            vastaus = myVarasto.getStringList("SELECT * from unique_item WHERE user_id = '" + id + "'");

            if (!vastaus.isEmpty()) {
                // Nollataan ensin lainat
                // nollataan kappaleen palautuspäivä
                myVarasto.sendSqlNRS("UPDATE unique_item SET palautus = '<NULL>' WHERE user_id = '" + id + "'");

                // nollataan kappaleen käyttäjä tieto
                myVarasto.sendSqlNRS("UPDATE unique_item SET user_id = '<NULL>' WHERE user_id = '" + id + "'");
            }
            
            vastaus.clear();
            
            vastaus = myVarasto.getStringList("select * from varaukset");

            if (!vastaus.isEmpty()) {
                // poistetaan varaus kannasta
                myVarasto.sendSqlNRS("DELETE FROM varaukset WHERE user_id = '" + id + "'");
            }
            // Poistetaan käyttäjä kannasta
            myVarasto.sendSqlNRS("DELETE FROM user WHERE id = '" + id + "'");

        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
}
