/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.Kirjasto;
import fi.datadevils.kirjasto.varasto.Varasto;
import static java.lang.Integer.parseInt;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author Jarkko
 */
public class PoistaTeos {

    private Varasto myVarasto = null;
    private Scanner myScanner = null;

    public PoistaTeos(Varasto aVarasto) {
        myVarasto = aVarasto;
        myScanner = new Scanner(System.in);
    }


    public void poistaTeos(int id) {
        
        System.out.println("id == " +id);
        //String row_mark = aMark;
        ArrayList<String> vastaus_item = null;
        ArrayList<String> vastaus_unique_item = null;
        ArrayList<String> vastaus_cd = null;
        ArrayList<String> vastaus_lehti = null;
        ArrayList<String> vastaus_kirjat = null;
        ArrayList<String> vastaus_varaukset = null;
        ArrayList<String> vastaus_tekijat = null;
        int item_id = 0;
        int id__unique_item = 0;
        //System.out.print(row_mark);

        System.out.println("Järjestelmässä olevat teokset: ");
        // haetaan teokset
            
          if (id == 0) {
            try {

                vastaus_item = myVarasto.getFull("select * from item");

            } catch (SQLException ex) {
                Logger.getLogger(Kirjasto.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
            for (int i = 0; i < vastaus_item.size(); i++) {
                System.out.println(vastaus_item.get(i));
            }

            System.out.println("Syötä teoksen id!");
            id = parseInt(myScanner.nextLine());
        }
        int id_neg = (0 - id);

        paivitaTaulu(vastaus_unique_item, "unique_item", id);
        paivitaTaulu(vastaus_cd, "cd", id);
        paivitaTaulu(vastaus_lehti, "lehti", id);
        paivitaTaulu(vastaus_tekijat, "tekijat", id);
        paivitaTaulu(vastaus_varaukset, "varaukset", id);

    }

    private void paivitaTaulu(ArrayList<String> vastaukset, String taulu, int id) {
        // Tässä haetaan tekijat taulun id:t
        try {
            vastaukset = myVarasto.getFull("select id from " + taulu + " where item_id = " + id);
            int id_neg = (0 - id);
            id_neg = (id > 0) ? (0 - id) : (id);

            if (vastaukset.size() > 0) {
                // System.out.println("vvastaukset.size  ==" + vastaus_varaukset.size());

                int temp = 0;
                int temp_id_neg = 0;
                for (int i = 0; i < vastaukset.size(); i++) {

                    temp = parseInt(vastaukset.get(i));
                    temp_id_neg = (temp > 0) ? (0 - temp) : (temp);

                    //   System.out.println("vastaukset.get(i):  ==" + vastaus_tekijat.get(i));
                    // Tässä muokataan tekijat talun id:t
                    myVarasto.sendSqlNRS("UPDATE " + taulu + " set id = "
                            + temp_id_neg + " where id = " + parseInt(vastaukset.get(i)));
                    
                      //  System.out.println("vastaus_tekijat.size  ==" + vastaus_tekijat.size());
                    
                }
                
                myVarasto.sendSqlNRS("UPDATE item set id = " + id_neg + " where id = " + id);
                
            } else {


                //  System.out.println("vastaus_tekijat.size  ==" + vastaus_tekijat.size());
                myVarasto.sendSqlNRS("UPDATE item set id = " + id_neg + " where id = " + id);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

  
    public void poistaYksiTeos()
    {
        //String row_mark = aMark;
        int id = 0;
        int id_neg = 0;
        ArrayList<String> vastaus_item = null;
        ArrayList<String> vastaus_unique_item = null;
       
        int item_id = 0;
        int id__unique_item = 0;
        int id__unique_item_neg = 0;
        //System.out.print(row_mark);
        ResultSet rs = null;
       // String syÃ¶te = " ";
            
            try {

            System.out.println("Järjestelmässä olevat teokset: ");
            
            // haetaan teokset
            vastaus_item = myVarasto.getFull("select * from item");

            for (int i = 0; i < vastaus_item.size(); i++) {
                System.out.println(vastaus_item.get(i));
            }

            System.out.println("Anna teos id!");

            id = parseInt(myScanner.nextLine());
            id_neg = (0 - id);

            // haetaan ensin unique_item rivit
            vastaus_unique_item = myVarasto.getFull("select * from unique_item where (item_id = " + id + " AND id > '0')");

            System.out.println("Teoksen kappaleet: ");

            for (int i = 0; i < vastaus_unique_item.size(); i++) {
                System.out.println(vastaus_unique_item.get(i));
            }

            System.out.println("Anna poistettavan id!");

            id__unique_item = parseInt(myScanner.nextLine());
            id__unique_item_neg = (0 - id__unique_item);

          
            if (vastaus_unique_item.size() > 1) {
                //System.out.println("vastaus_unique_item.size  ==" + vastaus_unique_item.size());

                // Tämä muokkaa unique_item taulua
                myVarasto.sendSqlNRS("UPDATE unique_item set id = "
                        + id__unique_item_neg + " where id = " + id__unique_item);
                
                } else {
                   // System.out.println("vastaus_unique_item.size  ==" + vastaus_unique_item.size());
                    
                    this.poistaTeos(id);

                }

        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<String> haeTeoksetFx() {
        ArrayList<String> vastaus_item = new ArrayList<String>();

        try {
            // System.out.println("Järjestelmässä olevat teokset: ");

            // haetaan teokset
            vastaus_item = myVarasto.getFull("select * from item");
            

        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return vastaus_item;
    }
    
    public ArrayList<String> haeKappaleetFx(int id) {

        ArrayList<String> vastaus_unique_item = new ArrayList<String>();

        try {
            // haetaan ensin unique_item rivit
            vastaus_unique_item = myVarasto.getFull("select * from unique_item where item_id = '" + id + "'");

            
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return vastaus_unique_item;
    }
    
     public void poistaYksiFx(int id__unique_item, int id) {
        // System.out.println("Poistoa kutsuttu!");
        try {
            ArrayList<String> vastaus_unique_item = new ArrayList<String>();

            vastaus_unique_item = haeKappaleetFx(id);

            //System.out.println("id__unique_item = " + id__unique_item);
            if (vastaus_unique_item.size() > 1) {
                //System.out.println("vastaus_unique_item.size  ==" + vastaus_unique_item.size());

                // Tämä poistaa rivin unique_item taulusta
                //System.out.println("Poistetaan unique_item kpl!");
                myVarasto.sendSqlNRS("DELETE FROM unique_item WHERE id = '" + id__unique_item + "'");

            } else {
                  // System.out.println("vastaus_unique_item.size  ==" + vastaus_unique_item.size());

                //System.out.println("Poistetaan koko teos!");
                // Tämä poistaa kaiken
                this.poistaTeosFx(id);

            }

        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void poistaTeosFx(int id) {
        
        ArrayList<String> vastaus_unique_item = null;
      //  ArrayList<String> vastaus_cd = null;
      //  ArrayList<String> vastaus_lehti = null;
        ArrayList<String> vastaus_tekijat = null;
        ArrayList<String> vastaus_varaukset = null;
       
        try {
            myVarasto.sendSqlNRS("DELETE FROM item WHERE id = '" + id + "'");
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        
        
        deleteRiviFromTaulu(vastaus_unique_item, "unique_item", id);
       // deleteRiviFromTaulu(vastaus_cd, "cd", id);
       // deleteRiviFromTaulu(vastaus_lehti, "lehti", id);
        deleteRiviFromTaulu(vastaus_tekijat, "tekijat", id);
        deleteRiviFromTaulu(vastaus_varaukset, "varaukset", id);
    }
    
    private void deleteRiviFromTaulu(ArrayList<String> vastaukset, String taulu, int id) {
        // Tässä haetaan tekijat taulun id:t
        try {
            vastaukset = myVarasto.getFull("select id from " + taulu + " where item_id = " + id);

            if (vastaukset.size() > 0) {
                // System.out.println("vvastaukset.size  ==" + vastaus_varaukset.size());

                int temp_id = 0;
                for (int i = 0; i < vastaukset.size(); i++) {
                    temp_id = parseInt(vastaukset.get(i));
                    //System.out.println("Poistetaan rivi taulusta: "+ taulu);
                    myVarasto.sendSqlNRS("DELETE FROM " + taulu + " WHERE id = '" + temp_id + "'");
                      //  System.out.println("vastaus_tekijat.size  ==" + vastaus_tekijat.size());

                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

