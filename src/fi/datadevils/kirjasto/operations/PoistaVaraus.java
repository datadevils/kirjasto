/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.varasto.Varasto;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mika
 */
public class PoistaVaraus {

    public static int poistaVaraus(Varasto aVarasto, int aTeoksenId, int aUserId) {
        int id = 0;
        int muutoksia = 0;
        String kysely = null;
        try {
            id = getId(aVarasto, aTeoksenId, aUserId);
            kysely = "update varaukset set vuoronumero=vuoronumero-1  where item_id=" + aTeoksenId;
            kysely += " and vuoronumero>=(select vuoronumero from varaukset where id=" + id + ");";
            System.err.println(kysely);
            muutoksia = aVarasto.sendSqlNRS(kysely);
        } catch (SQLException ex) {
            Logger.getLogger(PoistaVaraus.class.getName()).log(Level.SEVERE, null, ex);
            muutoksia = 0;
        }
        return muutoksia;
    }
    
        public static int poistaVarausFx(Varasto aVarasto, int aTeoksenId, int aUserId) {
        int id = 0;
        int muutoksia = 0;
        String kysely = null;
        try {
        //    id = getId(aVarasto, aTeoksenId, aUserId);
            
            kysely = "delete from varaukset where item_id=" + aTeoksenId + " and user_id=" + aUserId + ";";
            muutoksia = aVarasto.sendSqlNRS(kysely);
            System.err.println(kysely);
            
        //    kysely = "update varaukset set vuoronumero=vuoronumero-1  where item_id=" + aTeoksenId;
        //    kysely += " and vuoronumero>=(select vuoronumero from varaukset where id=" + id + ");";
            System.err.println(kysely);
            muutoksia = aVarasto.sendSqlNRS(kysely);
        } catch (SQLException ex) {
            Logger.getLogger(PoistaVaraus.class.getName()).log(Level.SEVERE, null, ex);
            muutoksia = 0;
        }
        return muutoksia;
    }
    

    public static int poistaKaikkiVaraukset(Varasto aVarasto, int aTeoksenId) {
        int poistoja = 0;
        String kysely = "update varaukset set id=-1 where item_id=" + aTeoksenId + ";";
        try {
            poistoja = aVarasto.sendSqlNRS(kysely);
        } catch (SQLException ex) {
            Logger.getLogger(PoistaVaraus.class.getName()).log(Level.SEVERE, null, ex);
            poistoja = 0;
        }
        return poistoja;
    }

    private static int getId(Varasto aVarasto, int aTeoksenId, int aUserId) throws SQLException {
        String kysely = "select id from varaukset where item_id=" + aTeoksenId;
        kysely += " and user_id =" + aUserId + ";";
        return aVarasto.getOneInt(kysely);
    }
}
