/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.operations;

import fi.datadevils.kirjasto.Kirjasto;
import fi.datadevils.kirjasto.varasto.SingletonVarasto;
import fi.datadevils.kirjasto.varasto.Varasto;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mika
 */
public class VaraaKirja {

    /**
     *
     * @param aVarasto
     * @param aTeoksenId
     * @param aUserId
     * @return Varauksen jonotusnumeron, if -1 varaus ei onnistunut.
     */
    public static int varaaKirja(Varasto aVarasto, int aTeoksenId, int aUserId) {
        String kysely = "insert into varaukset values(";
        int sinunNumero = -1;
        try {
            //sinunNumero = getVuoronumero(aVarasto, aTeoksenId, aUserId);
            sinunNumero = newVuoronumero(aVarasto, aTeoksenId, aUserId);
            if (sinunNumero != -1) {
                kysely += aVarasto.getNextId("varaukset") + ", ";
                kysely += aTeoksenId + ", ";
                kysely += sinunNumero + ", " + aUserId + ");";
                System.err.println(kysely);
                aVarasto.sendSqlNRS(kysely);
            }
        } catch (SQLException ex) {
            Logger.getLogger(VaraaKirja.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Teoksen varaus ei onnistunut!");
        }
        if (sinunNumero == -1) {
            System.out.println("Teos oli jo varattu tai lainattu käyttäjälle!");
        }
        return sinunNumero;
    }

    private static int getVuoronumero(Varasto aVarasto, int aTeoksenId, int aUserId) throws SQLException {
        int sinunNumero = -1;
        String kysely = "select vuoronumero from varaukset where item_id=" + aTeoksenId;
        kysely += " and user_id =" + aUserId + ";";

        if (aVarasto.getOneInt(kysely) == 0) {
            kysely = "select max(vuoronumero) from varaukset where user_id=" + aTeoksenId;
            kysely += " and user_id =" + aUserId + ";";
            sinunNumero = aVarasto.getOneInt(kysely) + 1;
        }
        return sinunNumero;
    }

    private static int newVuoronumero(Varasto aVarasto, int aTeoksenId, int aUserId) throws SQLException {
        int sinunNumero = -1;

        ArrayList<String> vastaus = new ArrayList<>();

        // Jos käyttäjällä oli jo varaus kyseiseen teokseen, 
        // niin palautetaan virhe arvo.
        vastaus = aVarasto.getStringList("select * from varaukset where item_id ='"
                + aTeoksenId + "'and user_id = '" + aUserId + "'");

        if (vastaus.size() > 0) {
            return -1;
        }

         // Tarkistetaan myös, ettei käyttäjällä ole lainassa
        // samaa teosta.
        vastaus = aVarasto.getStringList("select * from unique_item where item_id ='"
                + aTeoksenId + "'and user_id = '" + aUserId + "'");

        if (vastaus.size() > 0) {
            return -1;
        }

        // Haetaan kaikki varaukset kyseessä olevaan teokseen 
        vastaus = aVarasto.getStringList("select * from varaukset where item_id ='"
                + aTeoksenId + "'");  //"'and user_id = '" + aUserId + "'" );

        // Jos varauksia teokseen löytyy, niin haetaan suurin vuoronumero 
        //teokselle, kasvatetaan sitä yhdellä ja palautetaan se.
        if (vastaus.size() > 0) {
            vastaus = aVarasto.getStringList("select max(vuoronumero) from varaukset where item_id ='"
                    + aTeoksenId + "'"); // +"'and user_id = '" + aUserId + "'" );
            sinunNumero = parseInt(vastaus.get(0)) + 1;
        } else {
            sinunNumero = 0;
        }
        return sinunNumero;
    }

    public static ArrayList<String> haeVaraukset(Integer id) {
        ArrayList<String> vastaus_varaus = new ArrayList<String>();

        try {
            // haetaan ensin unique_item rivit
            vastaus_varaus = SingletonVarasto.getInstance().getFull("select * from varaukset where item_id = " + id + ";");
        } catch (SQLException ex) {
            Logger.getLogger(Kirjasto.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return vastaus_varaus;
    }
}
