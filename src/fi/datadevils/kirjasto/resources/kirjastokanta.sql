create table item(id INTEGER PRIMARY KEY, tekijat_id INTEGER NOT NULL UNIQUE, nimi TEXT NOT NULL, tyylilaji_id INTEGER NOT NULL, luokka_id INTEGER NOT NULL, julkaisuvuosi INTEGER, kustantaja_id INTEGER NOT NULL, tyyppi_id INTEGER NOT NULL);
create table unique_item(id INTEGER PRIMARY KEY, item_id INTEGER NOT NULL, palautus DATE, user_id INTEGER);
create table varaukset(id INTEGER PRIMARY KEY, item_id INTEGER NOT NULL, vuoronumero INTEGER NOT NULL, user_id INTEGER NOT NULL);
create table user(id INTEGER PRIMARY KEY, etunimi TEXT NOT NULL, sukunimi TEXT NOT NULL, email TEXT NOT NULL UNIQUE, salasana TEXT NOT NULL, sakkosaldo REAL NOT NULL, admin INTEGER);
create table tekijat(id INTEGER PRIMARY KEY, item_id INTEGER NOT NULL, tekija_id INTEGER NOT NULL);
create table tekija(id INTEGER PRIMARY KEY, nimi TEXT NOT NULL);
create table tyylilaji(id INTEGER PRIMARY KEY, nimi TEXT NOT NULL UNIQUE, selitys TEXT);
create table luokka(id INTEGER PRIMARY KEY, nimi TEXT NOT NULL UNIQUE, selitys TEXT);
create table kustantaja(id INTEGER PRIMARY KEY, nimi TEXT NOT NULL UNIQUE, kaupunki TEXT);
create table tyyppi(id INTEGER PRIMARY KEY, nimi TEXT NOT NULL UNIQUE, selitys TEXT);
