/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.ui;

import fi.datadevils.kirjasto.data.Teos;
import fi.datadevils.kirjasto.operations.LisääTeos;
import fi.datadevils.kirjasto.operations.PalautaTeos;
import fi.datadevils.kirjasto.operations.PoistaTeos;
import fi.datadevils.kirjasto.operations.MuutaKäyttäjänTyyppi;
import java.util.ArrayList;

/**
 *
 * @author Mika Pihlamo
 */
public class AdminWindow extends KirjastoUiWindow {

    public AdminWindow(KirjastoUiWindow aParent, String... aParametrit) {
        super(aParent, "AdminWindow", aParametrit);
        setValinnat("loppu", "lisaa", "poista", "poista yksi", "selaa", "palautus", "tee admin", "testi");
    }

    @Override
    public void show() {
        String input = null;
        for (;;) {
            System.out.print(getPrompt());
            input = getScanner().nextLine();
            switch (getValintaInt(input)) {
                case 0:
                    return;
                case 1:
                    new LisääTeos(getVarasto()).lisääTeos(" ");
                    break;
                case 2:
                    new PoistaTeos(getVarasto()).poistaTeos(0);
                    break;
                case 3:
                    new PoistaTeos(getVarasto()).poistaYksiTeos();
                    break;
                case 4:
                    (new SelaaWindow(this)).show();
                    break;
                case 5:
                    PalautaTeos.show("", getUser(), getVarasto(), getScanner());
                    break;
                case 6:
                    new MuutaKäyttäjänTyyppi(getVarasto()).muutaKäyttäjänTyyppi();
                    break;
                case 7:
                    testi();
                    break;
            }
        }
    }

    private void testi() {
        ArrayList<String> tekijät = new ArrayList<>();
        tekijät.add("Ilkka Haikala");
        tekijät.add("Jukka Märijärvi");
        Teos temp = new Teos("Ohjelmistotuotanto", 2, "ATK", 2000, "Satku", "Tietokirja", tekijät, "Kirja");
        System.err.println(getVarasto().setTeos(temp));
    }
}
