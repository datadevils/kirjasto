/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.ui;

import fi.datadevils.kirjasto.data.User;
import fi.datadevils.kirjasto.varasto.Varasto;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Mika Pihlamo
 */
public abstract class KirjastoUiWindow {

    protected KirjastoUiWindow myParent = null;
    private String myName = null;
    private Varasto myVarasto = null;
    private Scanner myScanner = null;
    private ArrayList<String> myValinnat = null;
    private ArrayList<String> myParametrit = null;
    private User myUser = null;

    public KirjastoUiWindow(KirjastoUiWindow aParent, String aNimi, Varasto aVarasto, Scanner aScanner, User aUser ,ArrayList<String> aParametrit) {
        myParent = aParent;
        myName = aNimi;
        myVarasto = aVarasto;
        myScanner = aScanner;
        myParametrit = aParametrit;
        myUser = aUser;
    }

    public KirjastoUiWindow(KirjastoUiWindow aParent, String aNimi, Varasto aVarasto, Scanner aScanner, User aUser, String... aParametrit) {
        this(aParent, aNimi, aVarasto, aScanner, aUser ,(ArrayList<String>) null);
        setParametrit(aParametrit);
    }

    public KirjastoUiWindow(KirjastoUiWindow aParent, String aNimi, String... aParametrit) {
        this(aParent, aNimi, aParent.getVarasto(), aParent.getScanner(), aParent.getUser(), aParametrit);
    }

    public KirjastoUiWindow(KirjastoUiWindow aParent, String aNimi, ArrayList<String> aParametrit) {
        this(aParent, aNimi, aParent.getVarasto(), aParent.getScanner(), aParent.getUser(), aParametrit);
    }

    public KirjastoUiWindow(KirjastoUiWindow aParent, String... aParametrit) {
        this(aParent, null, aParent.getVarasto(), aParent.getScanner(), aParent.getUser(), aParametrit);
    }

    public final KirjastoUiWindow getParent() {
        return myParent;
    }

    public User getUser() {
        return myUser;
    }

    public void setUser(User myUser) {
        this.myUser = myUser;
    }
    
    public final void setValinnat(String... aValinnat) {
        if (myValinnat == null) {
            myValinnat = new ArrayList<>();
        }
        for (String temp : aValinnat) {
            myValinnat.add(temp);
        }
    }

    public final ArrayList<String> getValinnat() {
        return myValinnat;
    }

    public final int getValintaInt(String aValinta) {
        int i = 0;
        for (String temp : myValinnat) {
            if (aValinta.equalsIgnoreCase(temp)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public final void setParametrit(String... aParametrit) {
        if (myParametrit == null) {
            myParametrit = new ArrayList<>();
        }
        for (String temp : aParametrit) {
            myParametrit.add(temp);
        }
    }

    public final void setParametrit(ArrayList<String> aParametrit) {
        if (myParametrit == null) {
            myParametrit = new ArrayList<>();
        }
        for (String temp : aParametrit) {
            myParametrit.add(temp);
        }
    }

    public final ArrayList<String> getParametrit() {
        return myParametrit;
    }

    public final Varasto getVarasto() {
        return myVarasto;
    }

    public final Scanner getScanner() {
        return myScanner;
    }

    public final String getPrompt() {
        String prompt = "(";
        int i = 0;
        for (String temp : myValinnat) {
            prompt += temp;
            if (i < myValinnat.size() - 1) {
                prompt += ", ";
            }
            i++;
        }
        prompt += ")\n" + myName + "> ";
        return prompt;
    }

    public abstract void show();
}
