/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.ui;

import fi.datadevils.kirjasto.data.User;
import fi.datadevils.kirjasto.varasto.Varasto;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mika Pihlamo
 */
public class MainWindow extends KirjastoUiWindow {

    // -1=ei kirjautunut, 0=user, 1=admin
    private int myUserType = -1;

    public MainWindow(Varasto aVarasto) {
        super(null, "MainWindow", aVarasto, new Scanner(System.in), null);
        setValinnat("loppu", "kirjaudu", "admin", "user");
    }

    public void setUserType(int aType) {
        switch (aType) {
            case 0:
            case 1:
                myUserType = aType;
                break;
            default:
                myUserType = -1;
        }
    }

    @Override
    public void show() {
        String input = null;
        for (;;) {
            System.out.print(getPrompt());
            input = getScanner().nextLine();
            switch (getValintaInt(input)) {
                case 0:
                    try {
                        getVarasto().close();
                        return;
                    } catch (SQLException ex) {
                        Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return;
                case 1:
                    new RegistrationWindow(this).show();
                    break;
                case 2:
                    if (myUserType < 0) {
                        System.out.println("Et ole vielä kirjautunut järjestelmään.");
                        break;
                    }
                    if (myUserType == 1) {
                        new AdminWindow(this).show();
                    } else {
                        System.out.println("Sinulla ei ole ADMIN oikeuksia.");
                    }
                    break;
                case 3:
                    if (myUserType < 0) {
                        System.out.println("Et ole vielä kirjautunut järjestelmään.");
                        break;
                    }
                    if (myUserType == 0 || myUserType == 1) {
                        new UserWindow(this).show();
                    } else {
                        System.out.println("Sinulla ei ole USER oikeuksia.");
                    }
                    break;
            }
        }
    }
}
