/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.ui;

import fi.datadevils.kirjasto.data.User;
import fi.datadevils.kirjasto.operations.LisääKäyttäjä;
import java.io.Console;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Samuli
 */
public class RegistrationWindow extends KirjastoUiWindow {

    
    public RegistrationWindow(KirjastoUiWindow aParent, String... aParametrit) {
        super(aParent, "RegistrationWindow", aParametrit);
        setValinnat("loppu", "kirjaudu", "rekisteroidy", "ohita");
    }

    @Override
    public void show() {
        String input = null;
        for (;;) {
            System.out.print(getPrompt());
            input = getScanner().nextLine();
            switch (getValintaInt(input)) {
                case 0:
                    return;
                case 1:
                    if (login() && myParent.getUser() != null) {
                        ((MainWindow) getParent()).setUserType(myParent.getUser().getAdmin() ? 1 : 0);
                        return;
                    }
                    break;
                case 2:
                    register();
                    break;
                case 3:
                    ((MainWindow) getParent()).setUserType(1);
                    return;
            }
        }
    }
//       public void show() {
//        byte valinta = 0;
//        boolean regResult = false;
//        while (valinta != 3) {
//            ps.println(REGISTRATION_MAIN);
//            if (regResult == true) {
//                System.out.println("Uusi käyttäjä luotu onnistuneesti!\n");
//                regResult = false;
//            }
//            ps.printf(PROMPT);
//
//            valinta = sc.nextByte();
//            switch (valinta) {
//                case 1:
//                    if (this.login() && this.user != null) {
//                        (new MainWindow(varasto)).show(user);
//                    }
//                    break;
//                case 2:
//                    regResult = this.register();
//                    break;
//                case 3:
//                    break;
//            }
//        }
//    }

    public boolean login() {
        PrintStream ps = new PrintStream(System.out);
        final String REGISTRATION_MAIN = "\n\nKIRJASTO\n--------\n\n  1.  Kirjaudu\n  2.  Rekisteröidy\n  3.  Poistu\n\n";
        final String REGISTRATION_LOGIN = "\nKirjaudu\n--------\n";
        Console console = System.console();
        Scanner sc = getScanner();
        String email, password;

        ps.println(REGISTRATION_LOGIN);

        Boolean result = false;
        int attempts = 0;

        do {
            ps.printf("  sähköposti    : ");
            email = sc.nextLine();

            // nopeuttaa testaamista -> muista poistaa
            if (email.equals("")) {
                email = "sahonen@kolumbus.fi";
            }

            ps.printf("  salasana      : ");
            if (console == null) {
                password = sc.nextLine();
            } else {
                password = new String(console.readPassword());
            }

            System.out.println(password);
            // nopeuttaa testaamista -> muista poistaa
            if (password.equals("")) {
                password = "salasana";
            }

            result = verify(email, password);
            if (result == false) {
                ps.println("\nväärä sähköposti tai salasana!\n");
            }
            attempts++;
        } while (result == false && attempts < 3);

        return result;
    }

    public void register() {
        boolean registerFail = false;
        System.out.println("\nUusi käyttäjä\n-------------");
        LisääKäyttäjä lisääKäyttäjä = new LisääKäyttäjä(getVarasto());
        do {
            registerFail = lisääKäyttäjä.luo(null,null,null,null);
        } while(registerFail == true);
    }

    private boolean verify(String email, String password) {
        try {
            myParent.setUser(getVarasto().getUser(email));
        } catch (SQLException ex) {
            Logger.getLogger(RegistrationWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (myParent.getUser() != null && myParent.getUser().getSalasana() != null) {
            Boolean tosi = myParent.getUser().getSalasana().equals(password);
            return tosi;
        } else {
            return false;
        }
    }
}
