/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.ui;

import fi.datadevils.kirjasto.operations.LainaaTeos;
import fi.datadevils.kirjasto.operations.HaeTeokset;

/**
 *
 * @author Verneri
 */
public class SelaaWindow extends KirjastoUiWindow {
    
    
    public SelaaWindow(KirjastoUiWindow aParent, String... aParametrit) {
        super(aParent, "SelaaWindow", aParametrit);
        setValinnat("loppu", "kirjat", "cdt", "lehdet");
    }

    @Override
    public void show() {
        String input = null;
        
        for (;;) {
            System.out.print(getPrompt());
            input = getScanner().nextLine();
            switch (getValintaInt(input)) {
                case 0:
                    return;
                case 1:
                    new HaeTeokset().haeKaikki("kirjat");
                    if(getUser().getAdmin() == true)
                        new HaeTeokset().haeYksi("kirja");
                    else
                        LainaaTeos.lainaaTeos(LainaaTeos.valitseLainat(getScanner()), getUser());
                    break;
                case 2:
                    new HaeTeokset().haeKaikki("cdt");
                    if(getUser().getAdmin() == true)
                        new HaeTeokset().haeYksi("cd");
                    else
                        LainaaTeos.lainaaTeos(LainaaTeos.valitseLainat(getScanner()), getUser());
                    break;
                case 3:
                    new HaeTeokset().haeKaikki("lehdet");
                    if(getUser().getAdmin() == true)
                        new HaeTeokset().haeYksi("lehti");
                    else
                        LainaaTeos.lainaaTeos(LainaaTeos.valitseLainat(getScanner()), getUser());
                    break;
                case 4:
                    //new HaeTeokset(getVarasto()).haeYksi();
                    break;
            }
          
            System.out.println("\n");
        }
    }

}
