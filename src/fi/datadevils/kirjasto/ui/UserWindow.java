/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.ui;

import fi.datadevils.kirjasto.Kirjasto;
import fi.datadevils.kirjasto.operations.MuokkaaTietoja;
import fi.datadevils.kirjasto.operations.VaraaKirja;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mika Pihlamo
 */
public class UserWindow extends KirjastoUiWindow {

    public UserWindow(KirjastoUiWindow aParent, String... aParameters) {
        super(aParent, "UserWindow", aParameters);
        setValinnat("loppu", "kysely", "selaa", "palautus", "varaa", "muokkaa tietoja");
    }

    /**
     *
     */
    @Override
    public void show() {
        String input = null;
        for (;;) {
            System.out.print(getPrompt());
            input = getScanner().nextLine();
            switch (getValintaInt(input)) {
                case 0:
                    return;
                case 1:
                    kysely();
                    break;
                case 2:
//                    (new SelaaWindow(this)).show();
                    break;
                case 3:
//                    new PalautaWindow(this, getParametrit());
                    break;
                case 4:
//                    VaraaKirja.varaaKirja(getVarasto(), aTeoksenId, user.getId());
                    VaraaKirja.varaaKirja(getVarasto(), 3, 1);
                    break;
                case 5:
                    //getUser();
                    new MuokkaaTietoja(getVarasto()).muokkaaTietoja(getUser());
                    break;
            }
        }
    }

    private void kysely() {
        String input = null;
        ArrayList<String> vastaus = null;
        System.out.print("(loppu, {SQL-komento})\nkysely> ");
        input = getScanner().nextLine();
        switch (getValintaInt(input)) {
            case 0:
                return;
            default:
                try {
                    System.err.println(input);
                    vastaus = getVarasto().getFull(input);
                } catch (SQLException ex) {
                    Logger.getLogger(Kirjasto.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
                if (vastaus != null) {
                    vastaus.stream().forEach((tempRow) -> {
                        System.out.println(tempRow);
                    });
                }
        }
    }
}
