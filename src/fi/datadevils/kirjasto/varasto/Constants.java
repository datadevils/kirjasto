package fi.datadevils.kirjasto.varasto;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class Constants {

    public static final String DATA_SEPARATOR = ";";
    public static final String DEFAULT_DRIVER = "org.sqlite.JDBC";
    public static final String DEFAULT_PROTOCOL = "jdbc:sqlite";

    public static final SimpleDateFormat DEFAULT_OUT_DATE_FORMATTER
        = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
    public static final SimpleDateFormat DEFAULT_SQLITE_DATE_FORMATTER
        = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    public static final int FIRST_DATA_INDEX = 1;

}
