package fi.datadevils.kirjasto.varasto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SKuuLite implements SQLConnectorInterface {
    /*
     * CONSTANTS
     */

    private static final String DEFAULT_DRIVER = "org.sqlite.JDBC";
    private static final String DEFAULT_PROTOCOL = "jdbc:sqlite";
    /*
     * private variables
     */
    private Connection myConnection = null;
    private ResultSet myResultSet = null;

    /**
     * Constructor
     *
     * @param aDriver
     * @param aProtocol
     * @param aDatabase
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public SKuuLite(String aDriver, String aProtocol, String aDatabase) throws ClassNotFoundException, SQLException {
        Class.forName(aDriver);
        myConnection = DriverManager.getConnection(aProtocol + ":" + aDatabase);
//        System.err.println(myConnection.getClientInfo().toString());
    }

    /**
     * Constructor
     *
     * @param aDatabase
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public SKuuLite(String aDatabase) throws ClassNotFoundException, SQLException {
        this(DEFAULT_DRIVER, DEFAULT_PROTOCOL, aDatabase);
    }

    /**
     * execSql
     *
     * @param aSql
     * @return ResultSet
     * @throws SQLException
     */
    @Override
    public ResultSet execSql(String aSql) throws SQLException {
        Statement tempStatement = myConnection.createStatement();
        myResultSet = tempStatement.executeQuery(aSql);
        
        return myResultSet;
    }

    /**
     * execCreate
     *
     * @param aSql
     * @return ResultSet
     * @throws SQLException
     */
    @Override
    public int execSqlNRS(String aSql) {
        Statement tempStatement = null;
        int temp = -1;
        try {
            tempStatement = myConnection.createStatement();
//          myConnection.setAutoCommit(true);
            if (!tempStatement.execute(aSql)) {
                temp = tempStatement.getUpdateCount();
            }
            
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                tempStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(SKuuLite.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return temp;
    }

    /**
     * closeSql Close connction to the SQLite server.
     *
     * @throws SQLException
     */
    @Override
    public void close() {
        try {
            if (null != myResultSet) {
                myResultSet.close();
            }
            myConnection.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Connection getMyConnection() {
        return myConnection;
    }
   
}
