package fi.datadevils.kirjasto.varasto;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface SQLConnectorInterface {

    public ResultSet execSql(String aSql) throws SQLException;

    public int execSqlNRS(String aSql);

    public void close();
}
