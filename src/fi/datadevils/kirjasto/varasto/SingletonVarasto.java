/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.datadevils.kirjasto.varasto;

import fi.datadevils.kirjasto.data.User;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author mika
 */
public class SingletonVarasto extends Varasto {

	private static final SingletonVarasto INSTANCE = new SingletonVarasto();
	private static final Varasto myVarasto = null;
	private static User myUser = null;

	private SingletonVarasto() {
		super();
		SKuuLite temp = null;
		try {
			temp = new SKuuLite(
					fi.datadevils.kirjasto.Constants.DEFAULT_DATABASE_NAME);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(SingletonVarasto.class.getName()).log(
					Level.SEVERE, null, ex);
		} catch (SQLException ex) {
			Logger.getLogger(SingletonVarasto.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		this.setSQL(temp);
	}

	public static SingletonVarasto getInstance() {
		return INSTANCE;
	}

	public static void setMyUser(User aUser) {
		myUser = aUser;
	}

	public static User getMyUser() {
		return myUser;
	}

	public static SimpleStringProperty getMyUserName() {
		SimpleStringProperty temp = new SimpleStringProperty(
				"et ole kirjautunut");
		if (myUser != null) {
			temp = new SimpleStringProperty(myUser.getEtunimi() + " "
					+ myUser.getSukunimi());
		}
		return temp;
	}

	public static SimpleBooleanProperty getMyUserIsAdmin() {
		SimpleBooleanProperty temp = new SimpleBooleanProperty(
				myUser.getAdmin());
		return temp;
	}

	public static boolean isReady() {
		int tables = 0;
		try {
			tables = INSTANCE
					.getOneInt("SELECT count(*) FROM sqlite_master WHERE type=\"table\";");
		} catch (SQLException ex) {
			Logger.getLogger(SingletonVarasto.class.getName()).log(
					Level.SEVERE, null, ex);
		}
		return (tables != 0);
	}

	public static void initDataBase(String aCreationFileName) {
		ArrayList<String> creation = new ArrayList<>();
		String line = null;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(new File(aCreationFileName)));
			while ((line = br.readLine()) != null) {
				creation.add(line);
			}
			br.close();
			for (String t : creation) {
				INSTANCE.sendSqlNRS(t);
			}
		} catch (FileNotFoundException ex) {
			Logger.getLogger(SingletonVarasto.class.getName()).log(
					Level.SEVERE, null, ex);
		} catch (SQLException ex) {
			Logger.getLogger(SingletonVarasto.class.getName()).log(
					Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(SingletonVarasto.class.getName()).log(
					Level.SEVERE, null, ex);
		}
	}
}
