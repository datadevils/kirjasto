package fi.datadevils.kirjasto.varasto;

import fi.datadevils.kirjasto.data.Teos;
import fi.datadevils.kirjasto.data.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mika Pihlamo
 */
public class Varasto {

	SKuuLite myDatabase = null;

	public Varasto() {
	}

	public Varasto(SKuuLite aSql) {
		myDatabase = aSql;
	}

	public Varasto(String aDatabase) throws ClassNotFoundException,
			SQLException {
		this(new SKuuLite(aDatabase));
	}

	public void setSQL(SKuuLite aSql) {
		myDatabase = aSql;
	}

	public ResultSet sendSql(String aSql) throws SQLException {
		return myDatabase.execSql(aSql);
	}

	public int sendSqlNRS(String aSql) throws SQLException {
		return myDatabase.execSqlNRS(aSql);
	}

	public String getOneString(String aSql) throws SQLException {
		return myDatabase.execSql(aSql).getString(Constants.FIRST_DATA_INDEX);
	}

	public float getOneFloat(String aSql) throws SQLException {
		return myDatabase.execSql(aSql).getFloat(Constants.FIRST_DATA_INDEX);
	}

	public int getOneInt(String aSql) throws SQLException {
		return myDatabase.execSql(aSql).getInt(Constants.FIRST_DATA_INDEX);
	}

	public int getNextId(String aTable) throws SQLException {
		String temp = "select max(id) from " + aTable + ";";
		return getOneInt(temp) + 1;
	}

	public Date getOneDate(String aSql) throws SQLException, ParseException {
		return Constants.DEFAULT_SQLITE_DATE_FORMATTER.parse(this
				.getOneString(aSql));
	}

	public ArrayList<String> getStringList(String aSql) throws SQLException {
		Connection con = myDatabase.getMyConnection();
		con.setAutoCommit(true);
		ArrayList<String> tempSL = new ArrayList<>();
		ResultSet rs = myDatabase.execSql(aSql);
		while (rs.next()) {
			tempSL.add(rs.getString(1));
		}
		return tempSL;
	}

	public void close() throws SQLException {
		myDatabase.close();
	}

	// erikoisesti Kirjasto sovelluksen käyttöön lähinnä testaukseen
	public ArrayList<String> getFull(String aSql) throws SQLException {
		Connection con = myDatabase.getMyConnection();
		con.setAutoCommit(true);
		ArrayList<String> tempSL = new ArrayList<>();
		ResultSet rs = myDatabase.execSql(aSql);
		while (rs.next()) {
			String tempS = "";
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				tempS += rs.getString(i);
				if (i < rs.getMetaData().getColumnCount()) {
					tempS += Constants.DATA_SEPARATOR;
				}
			}
			tempSL.add(tempS);
		}
		return tempSL;
	}

	public User getUser(String email) throws SQLException {
		Connection con = myDatabase.getMyConnection();
		con.setAutoCommit(true);
		User user = null;
		String aSql = "select * from user where email = \"" + email + "\";";
		ResultSet rs = myDatabase.execSql(aSql);
		if (rs.next()) {
			user = new User(rs.getInt("id"), rs.getString("email"),
					rs.getString("etunimi"), rs.getString("sukunimi"),
					rs.getString("salasana"), rs.getDouble("sakkosaldo"),
					rs.getBoolean("admin"));
		}
		return user;
	}

	public void modyfyUser(User aUser) throws SQLException {
		Connection con = myDatabase.getMyConnection();
		con.setAutoCommit(true);
		String tempSql = "update user set";
		tempSql += " etunimi = '" + aUser.getEtunimi();
		tempSql += "', sukunimi = '" + aUser.getSukunimi();
		tempSql += "', email = '" + aUser.getEmail();
		tempSql += "', salasana = '" + aUser.getSalasana();
		tempSql += "', sakkosaldo = " + aUser.getSakkosaldo();
		tempSql += ", admin = " + aUser.getAdminInt();
		tempSql += " where id = " + aUser.getId() + ";";
		System.err.println(tempSql);
		myDatabase.execSqlNRS(tempSql);
	}

	public void addUser(User aUser) throws SQLException {
		Connection con = myDatabase.getMyConnection();
		con.setAutoCommit(true);
		int nextId = getNextId("user");
		String tempSql = "insert into user values(";
		tempSql += nextId + ", \' ";
		tempSql += aUser.getEtunimi() + "\', \'";
		tempSql += aUser.getSukunimi() + "\', \'";
		tempSql += aUser.getEmail() + "\', \'";
		tempSql += aUser.getSalasana() + "\', ";
		tempSql += aUser.getSakkosaldo() + ", \'";
		tempSql += aUser.getAdminInt() + "\');";
		System.err.println(tempSql);
		myDatabase.execSqlNRS(tempSql);
	}

	public void setTyylilaji(String[] genre) throws SQLException {
		Connection con = myDatabase.getMyConnection();
		con.setAutoCommit(true);
		String tempS = "insert into tyylilaji values(";
		tempS += getNextId("tyylilaji") + ", \"";
		tempS += genre[0] + "\", \"";
		tempS += genre[1] + "\");";
		System.err.println(tempS);
		sendSqlNRS(tempS);
	}

	public void setKustantaja(String[] publisher) throws SQLException {
		Connection con = myDatabase.getMyConnection();
		con.setAutoCommit(true);
		String tempS = "insert into kustantaja values(";
		tempS += getNextId("kustantaja") + ", \"";
		tempS += publisher[0] + "\", \"";
		tempS += publisher[1] + "\");";
		System.err.println(tempS);
		sendSqlNRS(tempS);
	}

	public void setLuokka(String[] luokka) throws SQLException {
		Connection con = myDatabase.getMyConnection();
		con.setAutoCommit(true);
		String tempS = "insert into luokka values(";
		tempS += getNextId("luokka") + ", \"";
		tempS += luokka[0] + "\", \"";
		tempS += luokka[1] + "\");";
		System.err.println(tempS);
		sendSqlNRS(tempS);
	}

	public void setUser(User aUser) throws SQLException, ParseException {
		Connection con = myDatabase.getMyConnection();
		con.setAutoCommit(true);
		String tempS = "insert into user values(";
		tempS += getNextId("user") + ", \"";
		tempS += aUser.getEtunimi() + "\", \"";
		tempS += aUser.getSukunimi() + "\", \"";
		tempS += aUser.getEmail() + "\", \"";
		tempS += aUser.getSalasana() + "\", ";
		tempS += String.valueOf(aUser.getSakkosaldo()) + ", ";
		tempS += aUser.getAdminInt() + ");";
		System.err.println(tempS);
		sendSqlNRS(tempS);
	}

	public boolean setTeos(Teos aTeos) {
		boolean tulos = false;
		String tempS = null;
		Connection con = myDatabase.getMyConnection();
		if (aTeos != null) {
			try {
				con.setAutoCommit(false);
				// keräilee tietoja talteen
				int id = getNextId("item");
				int tekijatId = getOneInt("select max(tekijat_id) from item;") + 1;
				int tyyliId = getTyyliLuokkaKustantajaTekijäTyyppiId(
						"tyylilaji", aTeos.getTyylilaji());
				int luokkaId = getTyyliLuokkaKustantajaTekijäTyyppiId("luokka",
						aTeos.getLuokka());
				int kustantajaId = getTyyliLuokkaKustantajaTekijäTyyppiId(
						"kustantaja", aTeos.getKustantaja());
				int tyyppiId = getTyyliLuokkaKustantajaTekijäTyyppiId("tyyppi",
						aTeos.getTyyppi());

				// lisää tekijät tauluihin tekija ja tekijat
				for (String tekija : aTeos.getTekijät()) {
					int tId = getTyyliLuokkaKustantajaTekijäTyyppiId("tekija",
							tekija);
					if (tId < 1)
						tId = setTyyliLuokkaKustantajaTekijäTyyppi(false,
								"tekija", tekija);
					tempS = "insert into tekijat values(";
					tempS += getNextId("tekijat") + ", ";
					tempS += tekijatId + ", " + tId + ");";
					System.err.println(tempS);

					PreparedStatement statement = con.prepareStatement(tempS);
					statement.executeUpdate();
				}
				// lisätään tyylkilaji tauluun
				if (tyyliId < 1) {
					tyyliId = setTyyliLuokkaKustantajaTekijäTyyppi(false,
							"tyylilaji", aTeos.getTyylilaji());
				}
				// lisää luokan tauluun
				if (luokkaId < 1) {
					luokkaId = setTyyliLuokkaKustantajaTekijäTyyppi(false,
							"luokka", aTeos.getLuokka());
				}
				// lisää kustantajan tauluu
				if (kustantajaId < 1) {
					kustantajaId = setTyyliLuokkaKustantajaTekijäTyyppi(false,
							"kustantaja", aTeos.getKustantaja());
				}
				// lisää tyyppi tauluu
				if (tyyppiId < 1) {
					tyyppiId = setTyyliLuokkaKustantajaTekijäTyyppi(false,
							"tyyppi", aTeos.getTyyppi());
				}

				// lisää teoksen tauluun
				tempS = "insert into item values(";
				tempS += id + ", ";
				tempS += tekijatId + ", \"";
				tempS += aTeos.getNimi() + "\", ";
				tempS += tyyliId + ", ";
				tempS += luokkaId + ", ";
				tempS += aTeos.getJulkaisuVuosi() + ", ";
				tempS += kustantajaId + ", ";
				tempS += tyyppiId + ");";
				System.err.println(tempS);

				PreparedStatement statement = con.prepareStatement(tempS);
				statement.executeUpdate();

				// lisää unique_item tauluun
				for (int i = 0; i < aTeos.getKopioidenLkm(); i++) {
					tempS = "insert into unique_item (id, item_id) values(";
					tempS += getNextId("unique_item") + ", " + id + ");";
					System.err.println(tempS);
					statement = con.prepareStatement(tempS);
					statement.executeUpdate();
				}
				con.commit();
				tulos = true;

			} catch (SQLException ex) {
				try {
					con.rollback();
					Logger.getLogger(Varasto.class.getName()).log(Level.SEVERE,
							null, ex);
				} catch (SQLException ex1) {
					Logger.getLogger(Varasto.class.getName()).log(Level.SEVERE,
							null, ex1);
				}
			} finally {
				try {
					con.setAutoCommit(true);
				} catch (SQLException ex) {
					Logger.getLogger(Varasto.class.getName()).log(Level.SEVERE,
							null, ex);
				}
			}
		}
		return tulos;
	}

	// älä hämmenny käytössä on varargs,
	// (käyttö on taulunnimi, nimi/etunimi, selitys/kaupunki/sukunimi)

	private int setTyyliLuokkaKustantajaTekijäTyyppi(boolean autoCommit,
			String... aDatat) throws SQLException {
		String tempS = "insert into " + aDatat[0] + " ";
		Connection con = myDatabase.getMyConnection();
		con.setAutoCommit(autoCommit);
		int id = getNextId(aDatat[0]);
		if (aDatat.length == 2) {
			tempS += "(id,nimi) ";
		}
		tempS += "values(" + id;
		for (int i = 1; i < aDatat.length; i++) {
			if (aDatat[i] != null) {
				tempS += ", \"" + aDatat[i] + "\"";
			}
		}
		tempS += ");";
		System.err.println(tempS);
		sendSqlNRS(tempS);
		return id;
	}

	// älä hämmenny käytössä on varargs,
	// (käyttö on taulunnimi, nimi/sukunimi, etunimi jos tarvitaan)
	private int getTyyliLuokkaKustantajaTekijäTyyppiId(String... aDatat)
			throws SQLException {
		String tempS = "select id from " + aDatat[0] + " where ";
		int id = -1;
		Connection con = myDatabase.getMyConnection();
		tempS += "nimi=\"" + aDatat[1] + "\";";
		try {
			id = getOneInt(tempS);
		} catch (SQLException ex) {
			id = -1;
		}
		return id;
	}
}
